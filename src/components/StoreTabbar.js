import * as React from "react";
import { StyleSheet, Dimensions, Animated, View } from "react-native";
import * as shape from "d3-shape";
import Svg, { Path } from "react-native-svg";
import StaticTabBar from "./StaticTabbar";
import global from "../../resources/global";
import Heart from "../../assets/shopping-heart.svg";
import Bag from "../../assets/shopping.svg";
import Cart from "../../assets/shopping-cart.svg";

const { width } = Dimensions.get("window");
const height = 84;

const AnimatedSvg = Animated.createAnimatedComponent(Svg);

const tabs = [{ icon: <Heart /> }, { icon: <Bag /> }, { icon: <Cart /> }];
const tabWidth = width / tabs.length;

const getPath = () => {
  const left = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)([
    { x: 0, y: 0 },
    { x: width, y: 0 }
  ]);
  const tab = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)
    .curve(shape.curveBasis)([
    { x: width, y: 0 },
    { x: width - 20, y: 0 },
    { x: width + 28, y: 0 },
    { x: width + 28, y: height - 30 },
    { x: width + tabWidth - 28, y: height - 30 },
    { x: width + tabWidth - 28, y: 0 },
    { x: width + tabWidth + 20, y: 0 },
    { x: width + tabWidth, y: 0 }
  ]);
  const right = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)([
    { x: width + tabWidth, y: 0 },
    { x: width * 2.5, y: 0 },
    { x: width * 2.5, y: height },
    { x: 0, y: height },
    { x: 0, y: 0 }
  ]);
  return `${left} ${tab} ${right}`;
};
const d = getPath();

export default class StoreTabbar extends React.Component {
  value = new Animated.Value(-((width / 3) * 2));

  render() {
    const { value: translateX } = this;
    return (
      <>
        <View {...{ width, height }}>
          <AnimatedSvg
            width={width * 2.5}
            {...{ height }}
            style={{ transform: [{ translateX }] }}
          >
            <Path {...{ d }} fill={global.colors.mainColor} />
          </AnimatedSvg>
          <View style={StyleSheet.absoluteFill}>
            <StaticTabBar
              chooseScreen={this.props.chooseScreen}
              navigation={this.props.navigation}
              tabs={tabs}
              value={translateX}
            />
          </View>
        </View>
      </>
    );
  }
}
