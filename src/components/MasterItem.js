import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import global from "../../resources/global";
import { useNavigation } from "@react-navigation/native";
import { Rating } from "react-native-ratings";

const MasterItem = ({ master, method }) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.item}
      onPress={() => navigation.navigate("MasterProfile", { master, method })}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <View style={{ marginRight: 15 }}>
          <View style={styles.icon}>
            <Image
              source={
                master.imageUrl
                  ? { uri: master.imageUrl }
                  : global.images.repair
              }
              style={{
                width: "100%",
                height: "100%",
                borderRadius: 10,
                resizeMode: "cover",
                backgroundColor: global.colors.mainColor,
              }}
            />
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text style={styles.topText}>
              {master.firstName} {master.lastName}
            </Text>
          </View>
          <View
            style={{
              marginTop: 4,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text style={styles.middleText}>{master.providerName}</Text>
          </View>
          <View
            style={{
              marginTop: 4,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <View>
              <Rating
                type="star"
                startingValue={parseFloat(master.rate)}
                ratingCount={5}
                readonly
                imageSize={20}
              />
            </View>
            <View>
              <Text style={styles.littleText}>
                {parseFloat(master.rateCount)} голоса
              </Text>
              <Text style={styles.littleText}>
                {parseFloat(master.rate)} оценка
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          marginTop: 10,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text style={styles.middleText}>{master.categoryName}</Text>
      </View>
      <View
        style={{
          marginTop: 4,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text style={styles.middleText}>
          Начальная ставка:{" "}
          <Text style={{ fontFamily: global.fonts.medium }}>
            от {master.price} UZS
          </Text>
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default MasterItem;
const styles = StyleSheet.create({
  item: {
    width: "100%",
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  button: {
    width: 140,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    backgroundColor: "white",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  icon: {
    width: 80,
    height: 80,
    backgroundColor: global.colors.mainColor,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    shadowColor: "#000",
    backgroundColor: "white",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  topText: {
    fontSize: 18,
    color: "#444444",
    fontFamily: global.fonts.medium,
  },
  littleText: {
    fontSize: 9,
    color: "#999999",
    textAlign: "right",
    fontFamily: global.fonts.regular,
  },
  middleText: {
    fontSize: 14,
    color: "#444444",
    fontFamily: global.fonts.regular,
  },
});
