import React from "react";
import {
  View,
  Text,
  Animated,
  TouchableOpacity,
  Image,
  StyleSheet,
} from "react-native";
import global from "../../resources/global";
import Trash from "../../assets/trash.svg";
import RightItem from "../../assets/right.svg";
import Swipeable from "react-native-gesture-handler/Swipeable";

const CartItem = ({ type }) => {
  const renderLeftActions = (progress, dragX) => {
    const scale = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });
    return (
      <Animated.View
        style={[
          {
            flexDirection: "row",
            opacity: scale,
            // transform: [{scale: scale}],
            marginLeft: 10,
            // transform: [{ translateX: trans }]
          },
        ]}
      >
        <TouchableOpacity
          style={{
            borderRadius: 10,
            height: 70,
            width: 70,
            backgroundColor: global.colors.danger,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Trash />
        </TouchableOpacity>
      </Animated.View>
    );
  };
  return (
    <Swipeable renderLeftActions={renderLeftActions}>
      <TouchableOpacity style={styles.item}>
        <View style={{ flex: 1 }}>
          <View style={styles.itemIcon}>
            <Image
              source={global.images.product}
              style={{ width: "110%", height: "110%", resizeMode: "cover" }}
            />
          </View>
        </View>
        <View style={styles.itemInfo}>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text
              style={{
                fontFamily: global.fonts.medium,
                fontSize: 14,
                color: "#444444",
              }}
            >
              Rosmeft Maksimum{" "}
            </Text>
            <Text
              style={[
                styles.infoText,
                type === "service" ? styles.warnColor : styles.noStyle,
                type === "road" ? styles.dangerColor : styles.noStyle,
                type === "tax" ? styles.taxColor : styles.noStyle,
              ]}
            >
              10W 4L
            </Text>
          </View>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text style={{ color: "#999999", fontSize: 11 }}>Количество:</Text>
            <Text style={{ color: "#999999", fontSize: 11 }}>Цена:</Text>
          </View>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text style={{ color: "#999999", fontSize: 11 }}>1 ШТ</Text>
            <Text
              style={{
                color: global.colors.success,
                fontFamily: global.fonts.medium,
                fontSize: 11,
              }}
            >
              90 000 UZS
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </Swipeable>
  );
};

export default CartItem;
const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    height: 70,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    paddingLeft: 20,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,

    elevation: 8,
  },
  itemInfo: {
    marginRight: 10,
    paddingRight: 10,
    flex: 4,
  },
  itemIcon: {
    height: 46,
    width: 46,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  infoText: {
    fontFamily: global.fonts.medium,
    fontSize: 14,
  },
});
