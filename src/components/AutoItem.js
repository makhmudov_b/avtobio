import React from "react";
import { View, Text, StyleSheet } from "react-native";
import global from "../../resources/global";
import Oil from "../../assets/oil.svg";
import Insurance from "../../assets/insurance.svg";
import Technical from "../../assets/technical.svg";
import Passport from "../../assets/passport.svg";
import Certificate from "../../assets/certificate.svg";

const AutoItem = ({ title, info, status, progress, type }) => {
  const currentColor = () => {
    if (progress < 25) return global.colors.success;
    if (progress < 50) return global.colors.normal;
    if (progress < 60) return global.colors.warn;
    if (progress < 100) return global.colors.danger;
  };
  const CurrentIcon = () => {
    if (type === "oil") return <Oil />;
    if (type === "insurance") return <Insurance />;
    if (type === "technical") return <Technical />;
    if (type === "passport") return <Passport />;
    if (type === "certificate") return <Certificate />;
  };
  return (
    <View style={styles.item}>
      <View style={styles.itemIcon}>
        <CurrentIcon />
      </View>
      <View style={{ flex: 1, marginLeft: 15, justifyContent: "center" }}>
        <View
          style={{
            alignItems: "center",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View>
            <Text
              style={{
                fontFamily: global.fonts.medium,
                color: "#444444",
                fontSize: 13,
              }}
            >
              {title}
            </Text>
            <Text style={{ fontSize: 9, color: "#999999" }}>Состояние:</Text>
          </View>
          <View>
            <Text
              style={{
                fontFamily: global.fonts.medium,
                textAlign: "right",
                color: "#444444",
                fontSize: 13,
              }}
            >
              {info}
            </Text>
            <Text style={{ fontSize: 9, textAlign: "right", color: "#999999" }}>
              {status}
            </Text>
          </View>
        </View>
        <View>
          <View
            style={{
              height: 6,
              marginTop: 8,
              borderRadius: 5,
              flex: 1,
              backgroundColor: "#E9E9E9",
              position: "relative",
            }}
          >
            <View
              style={{
                width: `${progress}%`,
                height: 6,
                borderRadius: 5,
                backgroundColor: currentColor(),
                position: "absolute",
                left: 0,
                top: 0,
              }}
            ></View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default AutoItem;

const styles = StyleSheet.create({
  item: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // marginBottom: 20
  },
  itemIcon: {
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
  },
});
