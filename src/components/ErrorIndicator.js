import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Logo from "../../assets/logo.svg";

const ErrorIndicator = () => {
  return (
    <View>
      <Logo />
      <Text>Ошибка! Что-то пошло не так.</Text>
    </View>
  );
};
export default ErrorIndicator;

const styles = StyleSheet.create({});
