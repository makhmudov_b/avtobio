import React, { useContext, useState, useRef } from "react";
import {
  View,
  Text,
  Animated,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import global from "../../resources/global";
import Service from "../../assets/service.svg";
import Road from "../../assets/road.svg";
import Fuel from "../../assets/fuel.svg";
import Success from "../../assets/success.svg";
import X from "../../assets/trash.svg";
import RightItem from "../../assets/right.svg";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { useNavigation } from "@react-navigation/native";
import Dollar from "../../assets/dollar.svg";
import Context from "./Context";
import {formatDate} from "../utils/date";

const MainItem = ({ event, bottom, top, auto, method }) => {
  const { apiService, token } = useContext(Context);
  const [loading, setLoading] = useState(false);
  const swiper = useRef(null);
  const { type, eventDate } = event;
  const chosenDateTime = formatDate(eventDate);
  const navigation = useNavigation();
  const deleteEvent = () => {
    swiper.current.close();
    setLoading(true);
    apiService
      .deleteData(
        "/events?" + new URLSearchParams({ type: type, eventId: event.id }),
        token,
        auto.id
      )
      .then((value) => {
        if (value.statusCode == 200) {
          method();
        }
        setLoading(false);
      });
  };
  const renderLeftActions = (progress, dragX) => {
    const scale = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });
    return (
      <Animated.View
        style={[
          {
            flexDirection: "row",
            opacity: scale,
            marginLeft: 10,
          },
        ]}
      >
        {/* <TouchableOpacity
          style={{
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
            height: 70,
            backgroundColor: global.colors.success,
            width: 70,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Success />
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => deleteEvent()}
          style={{
            borderRadius: 10,
            height: 70,
            backgroundColor: global.colors.danger,
            width: 70,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <X />
        </TouchableOpacity>
      </Animated.View>
    );
  };
  const getName = () => {
    if (
      event.name &&
      type != "CALL" &&
      type != "VISIT" &&
      type != "VISIT" &&
      type != "ORDER"
    ) {
      return event.name;
    } else {
      if (type == "CALL") {
        return "Вызов мастера";
      } else if (type == "VISIT") return "Вызов мастера";
      else if (type == "ORDER") return "Вызов мастера";
      else return "Визит к мастеру";
    }
  };
  return (
    <View style={{ position: "relative" }}>
      {loading && (
        <View
          style={{
            position: "absolute",
            top: 0,
            borderRadius: 10,
            right: 0,
            left: 0,
            height: 70,
            marginHorizontal: 20,
            backgroundColor: "rgba(255,255,255,.8)",
            zIndex: 20,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ActivityIndicator size={`large`} color={global.colors.mainColor} />
        </View>
      )}
      <Swipeable
        ref={swiper}
        containerStyle={[
          bottom ? { marginBottom: 10 } : { marginBottom: 5 },
          top ? { marginTop: 10 } : {},
          { position: "relative" },
        ]}
        renderLeftActions={
          type === "TRIP" ||
          type === "EXPENSE" ||
          type === "EARN" ||
          type === "REFUEL" ||
          type === "SERVICE"
            ? renderLeftActions
            : null
        }
      >
        <TouchableOpacity
          style={[styles.item]}
          onPress={() =>
            navigation.navigate(
              type !== "ORDER" &&
                type !== "CALL" &&
                type !== "EVACUATOR" &&
                type !== "VISIT" &&
                type !== "SOS"
                ? "Event"
                : "ServiceEvent",
              {
                event: event,
                auto: auto,
                reload: method,
              }
            )
          }
        >
          <View style={{ flex: 1 }}>
            <View
              style={[
                styles.itemIcon,
                type === "SERVICE" ||
                type === "ORDER" ||
                type === "CALL" ||
                type === "VISIT"
                  ? styles.warn
                  : {},
                type === "TRIP" ? styles.danger : {},
                type === "EXPENSE" ? styles.tax : {},
                type === "EARN" ? { backgroundColor: "#6AD97B" } : {},
                type === "REFUEL" ? { backgroundColor: "#B84BFF" } : {},
              ]}
            >
              {type === "SERVICE" ||
              type === "ORDER" ||
              type === "CALL" ||
              type === "VISIT" ? (
                <Service />
              ) : null}
              {type === "TRIP" ? <Road /> : null}
              {type === "EXPENSE" ? <Dollar /> : null}
              {type === "REFUEL" ? <Fuel /> : null}
              {type === "EARN" ? <Dollar /> : null}
            </View>
          </View>
          <View style={styles.itemInfo}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Text
                style={{
                  fontFamily: global.fonts.medium,
                  fontSize: 14,
                  color: "#444444",
                  overflow: "hidden",
                }}
              >
                {getName().slice(0, 14)}
                {getName().length > 14 && "..."}
              </Text>
              <Text
                style={[
                  styles.infoText,
                  type === "SERVICE" ||
                  type === "ORDER" ||
                  type === "CALL" ||
                  type === "VISIT"
                    ? styles.warnColor
                    : {},
                  type === "TRIP" ? styles.dangerColor : {},
                  type === "EXPENSE" ? styles.taxColor : {},
                  type === "REFUEL" ? { color: "#B84BFF" } : {},
                  type === "EARN" ? { color: "#6AD97B" } : {},
                ]}
              >
                {type === "REFUEL" ? "Заправка" : null}
                {type === "SERVICE" ? "Сервис" : null}
                {type === "TRIP" ? "Поездка" : null}
                {type === "EARN" ? "Заработок" : null}
                {type === "EXPENSE" ? "Расход" : null}
                {type === "ORDER" || type === "CALL" || type === "VISIT"
                  ? event.statusName
                  : null}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: 2,
                justifyContent: "space-between",
              }}
            >
              <Text
                style={{
                  color: "#999999",
                  fontSize: 10,
                  fontFamily: global.fonts.regular,
                }}
              >
                {(type === "ORDER" || type === "CALL" || type === "VISIT") &&
                  event.name}
              </Text>
              <Text
                style={{
                  color: "#999999",
                  fontSize: 10,
                  fontFamily: global.fonts.regular,
                }}
              >
                {(type === "ORDER" || type === "CALL" || type === "VISIT") &&
                  event.technicianType}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: 2,
                justifyContent: "space-between",
              }}
            >
              <Text
                style={{
                  color: "#999999",
                  fontSize: 11,
                  fontFamily: global.fonts.regular,
                }}
              >
                {chosenDateTime}
              </Text>
              <Text
                style={{
                  color: "#999999",
                  fontSize: 11,
                  fontFamily: global.fonts.regular,
                }}
              >
                {event.price} UZS
              </Text>
            </View>
          </View>
          <View style={styles.rightBlock}>
            {type === "SERVICE" ||
            type === "ORDER" ||
            type === "CALL" ||
            type === "VISIT" ? (
              <RightItem
                style={{ height: "100%" }}
                color={global.colors.warn}
              />
            ) : null}
            {type === "TRIP" ? (
              <RightItem
                style={{ height: "100%" }}
                color={global.colors.danger}
              />
            ) : null}
            {type === "EXPENSE" ? (
              <RightItem
                style={{ height: "100%" }}
                color={global.colors.preDanger}
              />
            ) : null}
            {type === "REFUEL" ? (
              <RightItem style={{ height: "100%" }} color={"#B84BFF"} />
            ) : null}
            {type === "EARN" ? (
              <RightItem style={{ height: "100%" }} color={"#6AD97B"} />
            ) : null}
          </View>
        </TouchableOpacity>
      </Swipeable>
    </View>
  );
};

export default MainItem;
const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    height: 70,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5,
    // marginBottom: 10,
    paddingLeft: 20,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1.65,
    elevation: 2,
  },
  itemInfo: {
    marginRight: 10,
    paddingRight: 10,
    flex: 4,
  },
  itemIcon: {
    height: 46,
    width: 46,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  infoText: {
    fontFamily: global.fonts.medium,
    fontSize: 14,
  },
  noStyle: {},
  warn: {
    backgroundColor: global.colors.warn,
  },
  danger: {
    backgroundColor: global.colors.danger,
  },
  tax: {
    backgroundColor: global.colors.preDanger,
  },
  warnColor: {
    color: global.colors.warn,
  },
  dangerColor: {
    color: global.colors.danger,
  },
  taxColor: {
    color: global.colors.preDanger,
  },
  rightBlock: {
    position: "absolute",
    right: 0,
    top: 0,
    bottom: 0,
  },
});
