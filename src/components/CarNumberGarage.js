import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Uzbekistan from "../../assets/uzbekistan.svg";
import Number from "../../assets/one-number.svg";
import global from "../../resources/global";

const CarNumberGarage = ({ autoInfo }) => {
  return (
    <>
      <View style={styles.carNumber}>
        <View
          style={{
            flex: 3,
            alignItems: "center",
            height: "100%",
            justifyContent: "center",
            borderRightColor: global.colors.mainColor,
            borderRightWidth: 1,
          }}
        >
          <Text style={styles.carNumberInput}>{autoInfo.slice(0, 2)}</Text>
        </View>
        <View
          style={{
            flex: 8,
            alignItems: "center",
            height: "100%",
            justifyContent: "center",
          }}
        >
          <Text style={styles.carNumberInput}>
            {autoInfo.slice(2, 3) + " "}
            {autoInfo.slice(3, 6) + " "}
            {autoInfo.slice(6, 8)}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            paddingRight: 5,
            height: "100%",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Uzbekistan height={5} />
          <Text
            style={{
              fontSize: 5,
              fontFamily: global.fonts.medium,
              textAlign: "center",
            }}
          >
            UZ
          </Text>
        </View>
      </View>
    </>
  );
};

export default CarNumberGarage;

const styles = StyleSheet.create({
  carNumber: {
    borderWidth: 1,
    borderColor: global.colors.mainColor,
    borderRadius: 5,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 22,
  },
  carNumberInput: {
    fontSize: 12,
    fontFamily: global.fonts.medium,
    color: "#444444",
    textAlign: "center",
    alignItems: "center",
    width: "100%",
    justifyContent: "center",
  },
});
