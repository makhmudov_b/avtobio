import React, { useState } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import global from "../../resources/global";
import Heart from "../../assets/heart-blue.svg";
import HeartActive from "../../assets/heart-red.svg";
import Plus from "../../assets/plus.svg";
import Minus from "../../assets/minus.svg";

const ProductItem = ({ showModal }) => {
  const [like, toggleLike] = useState(0);
  const [counter, changeCounter] = useState(0);
  return (
    <View style={styles.product}>
      <View style={styles.productTop}>
        <TouchableOpacity style={styles.like} onPress={() => toggleLike(!like)}>
          {like ? (
            <HeartActive width={25} height={25} />
          ) : (
            <Heart width={25} height={25} />
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => showModal(1)}>
          <Image
            source={global.images.product}
            style={{ width: "100%", borderRadius: 20 }}
          />
        </TouchableOpacity>
        <View style={[styles.productText]}>
          <Text
            style={{
              fontSize: 12,
              fontFamily: global.fonts.medium,
              color: "#444444",
              maxWidth: 125,
              textAlign: "center"
            }}
          >
            Rosmeft Maksimum 10W 4L
          </Text>
        </View>
      </View>
      <View style={styles.productInfo}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text style={styles.productInfoText}>Объем:</Text>
          <Text style={styles.productInfoText}>4L</Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text style={styles.productInfoText}>Цена:</Text>
          <Text style={styles.productInfoText}>90 000 UZS</Text>
        </View>
        {counter > 0 ? (
          <View
            style={{
              flexDirection: "row",
              marginTop: 10,
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => changeCounter(counter - 1)}
              style={styles.counter}
            >
              <Minus width={16} height={16} />
            </TouchableOpacity>
            <Text style={styles.buttonText}>{counter}</Text>
            <TouchableOpacity
              onPress={() => changeCounter(counter + 1)}
              style={styles.counter}
            >
              <Plus width={16} height={16} />
            </TouchableOpacity>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.button}
            onPress={() => changeCounter(counter + 1)}
          >
            <Text style={styles.buttonText}>В корзину</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default ProductItem;

const styles = StyleSheet.create({
  product: {
    width: 160,
    height: 260,
    backgroundColor: "white",
    borderRadius: 20,
    marginBottom: 10
  },
  productTop: {
    position: "relative",
    height: 160
  },
  productText: {
    alignItems: "center",
    position: "absolute",
    bottom: 18,
    height: 45,
    left: 0,
    right: 0,
    backgroundColor: "rgba(255, 255, 255, 0.5)"
  },
  like: {
    position: "absolute",
    top: 10,
    right: 10,
    zIndex: 5
  },
  productInfo: {
    paddingHorizontal: 20,
    paddingBottom: 20
  },
  productInfoText: {
    fontSize: 10,
    color: "#999"
  },
  counter: {
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 40,
    backgroundColor: global.colors.mainColor
  },
  button: {
    marginTop: 10,
    width: 120,
    height: 40,
    backgroundColor: "white",
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2
  },
  buttonText: {
    color: "#444",
    fontSize: 14
  }
});
