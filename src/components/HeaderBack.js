import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import CustomStatusBar from "./CustomStatusBar";
import Back from "../../assets/arrow-blue.svg";
import global from "../../resources/global";

const HeaderBack = ({ navigation, button, title, type }) => {
  const headerColor = type === "white" ? "white" : global.colors.mainColor;
  const [disabled, setDisabled] = useState(false);
  const tapMethod = () => {
    navigation.goBack();
    setDisabled(true);
    setTimeout(() => setDisabled(true), 800);
  };
  return (
    <>
      <CustomStatusBar type={type} />
      <View style={[styles.header, { backgroundColor: headerColor }]}>
        <View>
          {button ? (
            <TouchableOpacity
              disabled={disabled}
              onPress={() => tapMethod()}
              style={{ padding: 5 }}
            >
              <Back width={30} height={30} />
            </TouchableOpacity>
          ) : (
            <View style={{ opacity: 0 }}>
              <Back width={30} height={30} />
            </View>
          )}
        </View>
        <Text style={styles.title}>{title}</Text>
        <View style={{ opacity: 0 }}>
          <Back width={30} height={30} />
        </View>
      </View>
    </>
  );
};

export default HeaderBack;

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    height: 65,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1.65,
    elevation: 2,
    // marginBottom: 20,
  },
  title: {
    color: "#444444",
    flex: 1,
    textAlign: "center",
    fontFamily: global.fonts.medium,
    fontSize: 24,
  },
});
