import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import global from "../../resources/global";
import Service from "../../assets/service.svg";
import Dollar from "../../assets/dollar.svg";
import Document from "../../assets/documents.svg";
import {formatDate} from "../utils/date";

const ReminderItem = ({ rem, stopMethod, clearMethod }) => {
  const { eventType, createdDate } = rem;
const chosenDateTime = formatDate(createdDate);
  return (
    <View style={styles.item}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View style={{ marginRight: 15 }}>
          <View
            style={[
              styles.icon,
              rem.reminderType == "EVENT" || rem.reminderType == "ORDER"
                ? rem.eventReminderType == "Расход"
                  ? { backgroundColor: global.colors.preDanger }
                  : { backgroundColor: global.colors.warn }
                : { backgroundColor: global.colors.mainColor },
            ]}
          >
            {rem.reminderType == "EVENT" || rem.reminderType == "ORDER" ? (
              rem.eventReminderType != "Расход" ? (
                <Service />
              ) : (
                <Dollar />
              )
            ) : (
              <Document />
            )}
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text style={styles.topText}>
              {eventType ? (
                <>
                  {eventType.slice(0, 15)}
                  {eventType.length > 15 && "..."}
                </>
              ) : (
                rem.documentName
              )}
            </Text>
            <Text style={styles.topText}>
              {rem.reminderType == "EVENT" || rem.reminderType == "ORDER"
                ? rem.eventReminderType
                : "Документ"}
            </Text>
          </View>
          <View
            style={{
              marginTop: 2,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            {rem.reminderOdometer && (
              <>
                <Text style={styles.littleText}>Показатель одометра</Text>
                <Text style={styles.littleText}>{rem.reminderOdometer} КМ</Text>
              </>
            )}
          </View>
          <View
            style={{
              marginTop: 2,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text style={styles.littleText}>
              {rem.reminderType == "EVENT" || rem.reminderType == "ORDER"
                ? "Дата и время"
                : "Срок истечения"}
            </Text>
            <Text style={styles.littleText}>{chosenDateTime}</Text>
          </View>
        </View>
      </View>
      <View style={{ marginTop: 10 }}>
        <Text
          style={{
            fontFamily: global.fonts.medium,
            fontSize: 14,
            color: "#444444",
          }}
        >
          {rem.note ? rem.note : "Нет комментария"}
        </Text>
      </View>
      <View
        style={{
          flexDirection: "row",
          marginTop: 15,
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <TouchableOpacity
          style={styles.button}
          onPress={() => clearMethod(rem.id)}
        >
          <Text style={{ fontFamily: global.fonts.regular }}>Скрыть</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button]}
          onPress={() => stopMethod(rem.id)}
        >
          <Text style={{ fontFamily: global.fonts.regular }}>Остановить</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ReminderItem;
const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  button: {
    width: global.strings.width / 2.7,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    backgroundColor: "white",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  icon: {
    width: 46,
    height: 46,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
  },
  topText: {
    fontSize: 14,
    color: "#444444",
    fontFamily: global.fonts.medium,
  },
  littleText: {
    fontSize: 9,
    color: "#999999",
    fontFamily: global.fonts.regular,
  },
});
