import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import global from "../../resources/global";
import CarNumberGarage from "../components/CarNumberGarage";

const GarageItem = ({ navigation, auto }) => {
  const {
    id,
    makeName,
    modelName,
    manufacturedYear,
    currentMileage,
    plateNumber,
  } = auto;
  return (
    <TouchableOpacity
      style={styles.item}
      onPress={() => navigation.navigate("Auto", { vehicleId: id })}
    >
      <Image source={{ uri: auto.imageUrl }} style={styles.image} />
      <View
        style={[
          styles.image,
          { zIndex: 10, backgroundColor: "rgba(0,0,0,.5)" },
        ]}
      />
      <View
        style={{
          justifyContent: "space-between",
          flex: 1,
          padding: 20,
          zIndex: 20,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View>
            <Text style={[styles.topText]}>{makeName}</Text>
            <Text style={styles.littleText}>{modelName}</Text>
          </View>
          <View>
            <Text style={[styles.topText, { textAlign: "right" }]}>
              {manufacturedYear}
            </Text>
            <Text style={styles.littleText}>{currentMileage} KM</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View>
            <View style={{ width: 115 }}>
              <CarNumberGarage autoInfo={plateNumber} />
            </View>
          </View>
          <View>
            <Text style={styles.topText}>Подробнее</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default GarageItem;
const styles = StyleSheet.create({
  item: {
    width: "100%",
    height: 200,
    borderRadius: 20,
    marginBottom: 20,
  },
  image: {
    borderRadius: 20,
    position: "absolute",
    resizeMode: "cover",
    width: "100%",
    height: "100%",
  },
  topText: {
    fontSize: 18,
    color: "white",
    fontFamily: global.fonts.medium,
  },
  littleText: {
    fontSize: 12,
    fontFamily: global.fonts.regular,
    color: "white",
  },
});
