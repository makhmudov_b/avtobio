import React from "react";
import { View, StatusBar, Platform } from "react-native";
import Constants from "expo-constants";
import global from "../../resources/global";

const CustomStatusBar = ({ type }) => {
  const barColor = type === "white" ? "white" : global.colors.mainColor;
  return (
    <View
      style={{
        backgroundColor: barColor,
        height: Platform.OS === "ios" ? Constants.statusBarHeight : "auto",
      }}
    >
      <StatusBar
        backgroundColor={barColor}
        barStyle={type === "white" ? "dark-content" : "light-content"}
      />
    </View>
  );
};

export default CustomStatusBar;
