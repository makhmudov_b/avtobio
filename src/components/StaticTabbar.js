import React, { Component } from "react";
import {
  TouchableWithoutFeedback,
  TouchableOpacity,
  View,
  Animated,
  StyleSheet,
  Text,
} from "react-native";
import global from "../../resources/global";
import Main from "../../assets/main.svg";
import Context from "../components/Context";

export default class StaticTabbar extends Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    const { tabs } = props;
    this.values = tabs.map(
      (tab, index) => new Animated.Value(index === 1 ? 1 : 0)
    );
    this.state = {
      active: 1,
      disable: false,
    };
  }
  moveToPage = (index) => {
    if (parseInt(index) === 1) {
      if (!this.state.disable) {
        this.props.navigation.navigate("AddEvent");
      }
    }
  };
  onPress = (index) => {
    if (!this.state.disable) {
      const { value, tabs } = this.props;
      const width = global.strings.width;
      const tabWidth = width / tabs.length;
      Animated.sequence([
        ...this.values.map((value) =>
          Animated.timing(value, {
            toValue: 0,
            duration: 30,
            useNativeDriver: true,
          })
        ),
        Animated.parallel([
          Animated.spring(this.values[index], {
            toValue: 1,
            useNativeDriver: true,
          }),
          Animated.spring(value, {
            toValue: width * -1 + tabWidth * index,
            useNativeDriver: true,
          }),
        ]),
      ]).start();
      this.setState(
        {
          active: index,
        },
        this.props.chooseScreen(index)
      );
    }
  };
  render() {
    const { tabs, value } = this.props;
    const width = global.strings.width;
    const tabWidth = width / tabs.length;
    const tabHeight = 84;
    const circleSize = tabWidth / 2.2 < 60 ? tabWidth / 2.2 : 60;
    return (
      <View style={styles.container}>
        {tabs.map((tab, key) => {
          const activeValue = this.values[key];
          const opacity = value.interpolate({
            inputRange: [
              -width + tabWidth * (key - 1),
              -width + tabWidth * key,
              -width + tabWidth * (key + 1),
            ],
            outputRange: [1, 0, 1],
          });
          const translateY = activeValue.interpolate({
            inputRange: [0, 1],
            outputRange: [tabHeight + 10, 0],
          });
          return (
            <React.Fragment key={key}>
              <TouchableWithoutFeedback onPress={() => this.onPress(key)}>
                <Animated.View style={[styles.tab, { opacity }]}>
                  {key != 1 ? (
                    key == 2 ? (
                      <View style={{ position: "relative" }}>
                        {tab.icon}
                        <View
                          style={{
                            position: "absolute",
                            top: -7,
                            right: 0,
                            backgroundColor:
                              this.context.reminderCount > 0
                                ? global.colors.danger
                                : "#ffffff",
                            borderRadius: 20,
                            width: 18,
                            height: 18,
                            alignItems: "center",
                            justifyContent: "center",
                            zIndex: 20,
                          }}
                        >
                          <Text
                            style={{
                              fontFamily: global.fonts.bold,
                              fontSize: 10,
                              color:
                                this.context.reminderCount > 0
                                  ? "white"
                                  : global.colors.mainColor,
                              textAlign: "center",
                              height: 18,
                              lineHeight: 18,
                            }}
                          >
                            {this.context.reminderCount}
                          </Text>
                        </View>
                      </View>
                    ) : (
                      tab.icon
                    )
                  ) : (
                    <Main />
                  )}
                </Animated.View>
              </TouchableWithoutFeedback>
              <Animated.View
                style={{
                  position: "absolute",
                  width: tabWidth,
                  left: tabWidth * key,
                  height: tabHeight,
                  justifyContent: "center",
                  alignItems: "center",
                  top: -27,
                  transform: [{ translateY }],
                }}
              >
                <View
                  style={[
                    styles.circle,
                    { height: circleSize, width: circleSize },
                  ]}
                >
                  {key == 2 && (
                    <View
                      style={{
                        position: "absolute",
                        top: 10,
                        right: 15,
                        backgroundColor:
                          this.context.reminderCount > 0
                            ? global.colors.danger
                            : "#ffffff",
                        borderRadius: 20,
                        width: 18,
                        height: 18,
                        alignItems: "center",
                        justifyContent: "center",
                        zIndex: 20,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: global.fonts.bold,
                          fontSize: 10,
                          color:
                            this.context.reminderCount > 0
                              ? "white"
                              : global.colors.mainColor,
                          height: 18,
                          lineHeight: 18,
                        }}
                      >
                        {this.context.reminderCount}
                      </Text>
                    </View>
                  )}
                  <TouchableOpacity onPress={() => this.moveToPage(key)}>
                    {tab.icon}
                  </TouchableOpacity>
                </View>
              </Animated.View>
            </React.Fragment>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
  circle: {
    borderRadius: 100,
    backgroundColor: global.colors.mainColor,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
});
