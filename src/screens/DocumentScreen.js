import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import ArrowWhite from "../../assets/arrow-right-white.svg";
import Carousel from "react-native-snap-carousel";
import ArrowBack from "../../assets/arrow-blue.svg";
import Context from "../components/Context";
import CarNumber from "../components/CarNumber";
import CustomStatusBar from "../components/CustomStatusBar";

function DocumentScreen({ navigation, route }) {
  const { apiService, token, autos, phone } = useContext(Context);
  const { params } = route;
  const [index, setIndex] = useState(0);
  const [loading, setLoading] = useState(false);
  const setCurrentCar = (key) => {
    getDocuments(key);
    setIndex(key);
  };
  // const [documents, setDocuments] = useState([]);
  const [types, setTypes] = useState([]);
  useEffect(() => {
    getDocuments(0);
  }, []);
  const getDocuments = (key) => {
    setLoading(true);
    apiService
      .getResources("/document", token, autos[key].id)
      .then((value) => {
        setLoading(false);
        if (value.data) {
          setTypes(value.data);
        } else {
          setTypes([]);
        }
      })
      .catch((e) => Alert.alert("Что-то пошло не так"));
  };
  const reloadData = () => {
    getDocuments(index);
  };
  const DocItem = ({ type }) => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate("AddDocument", {
            updateMethod: () => reloadData(),
            id: type,
            vehicleId: autos[index].id,
          })
        }
        style={{
          marginBottom: 10,
          backgroundColor: "white",
          borderRadius: 10,
          alignItems: "center",
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 20,
          height: 50,
          borderRadius: 50,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.05,
          shadowRadius: 4.65,
          elevation: 2,
        }}
      >
        <Text
          style={{
            fontSize: 18,
            fontFamily: global.fonts.regular,
            color: "#444444",
          }}
        >
          {type.name}
        </Text>
        <Arrow />
      </TouchableOpacity>
    );
  };
  const width = global.strings.width;
  const SliderItem = ({ item, key }) => {
    return (
      <View
        style={[
          {
            alignItems: "center",
            width: "100%",
            // maxWidth: 20,
            justifyContent: "center",
            paddingBottom: 20,
            alignSelf: "center",
            zIndex: 20,
          },
        ]}
      >
        <CarNumber autoInfo={item.plateNumber} />
      </View>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <CustomStatusBar type={"white"} />
        <View
          style={{
            paddingHorizontal: 20,
            backgroundColor: "white",
            width: "100%",
            paddingTop: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <ArrowBack />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 24,
                color: "#444444",
                textAlign: "center",
                fontFamily: global.fonts.medium,
              }}
            >
              Мои документы
            </Text>
            <View style={{ opacity: 0 }}>
              <ArrowBack />
            </View>
          </View>
          <View
            style={{
              paddingTop: 20,
              alignItems: "center",
              justifyContent: "center",
              minHeight: 60,
            }}
          >
            {loading ? (
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 0,
                  backgroundColor: "rgba(246, 246, 246,0.6)",
                  zIndex: 999,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20,
                }}
              ></View>
            ) : null}
            {autos.length ? (
              <Carousel
                layout={"stack"}
                // layout={"default"}
                data={autos}
                loop={true}
                loopClonesPerSide={5}
                initialNumToRender={autos.length + 1}
                renderItem={SliderItem}
                sliderWidth={width}
                itemWidth={width - 100}
                // slideInterpolatedStyle={animatedStyles}
                // scrollInterpolator={scrollInterpolator}
                useScrollView={false}
                onSnapToItem={(index) => setCurrentCar(index)}
              />
            ) : (
              <View
                style={{
                  alignItems: "center",
                  maxWidth: 280,
                  justifyContent: "center",
                  paddingBottom: 20,
                  alignSelf: "center",
                }}
              >
                <CarNumber autoInfo={"--------"} />
              </View>
            )}
          </View>
          {!loading && (
            <>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddDocument", {
                    updateMethod: () => reloadData(),
                    code: "DRIVER_LICENSE",
                    name: "Водительские права",
                    id:
                      types.find((type) => type.type == "DRIVER_LICENSE") ||
                      null,
                    vehicleId: autos[index].id,
                  })
                }
                style={[
                  {
                    width: "100%",
                    backgroundColor: global.colors.mainColor,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "space-between",
                    flexDirection: "row",
                    borderRadius: 50,
                    paddingHorizontal: 20,
                    marginBottom: 20,
                  },
                ]}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Водительские права
                </Text>
                <ArrowWhite />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddDocument", {
                    updateMethod: () => reloadData(),
                    code: "TECH_PASSPORT",
                    name: "Тех. паспорт",
                    id:
                      types.find((type) => type.type == "TECH_PASSPORT") ||
                      null,
                    vehicleId: autos[index].id,
                  })
                }
                style={[
                  {
                    width: "100%",
                    backgroundColor: global.colors.mainColor,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "space-between",
                    flexDirection: "row",
                    borderRadius: 50,
                    paddingHorizontal: 20,
                    marginBottom: 20,
                  },
                ]}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Тех. паспорт
                </Text>
                <ArrowWhite />
              </TouchableOpacity>
            </>
          )}
        </View>

        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              paddingHorizontal: 20,
              backgroundColor: "#F6F6F6",
            }}
          >
            <View>
              <View style={{ paddingTop: 20 }}>
                {!loading ? (
                  types.length > 0 &&
                  types.map(
                    (type, key) =>
                      type.type !== "DRIVER_LICENSE" &&
                      type.type !== "TECH_PASSPORT" && (
                        <DocItem type={type} key={key} />
                      )
                  )
                ) : (
                  <Image
                    source={global.images.loader}
                    style={{ height: 200, width: 200, alignSelf: "center" }}
                  />
                )}
                {!loading && (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("AddDocument", {
                        updateMethod: () => reloadData(),
                        vehicleId: autos[index].id,
                      })
                    }
                    style={[
                      {
                        width: "100%",
                        backgroundColor: global.colors.mainColor,
                        height: 50,
                        alignItems: "center",
                        justifyContent: "center",
                        marginBottom: 15,
                        marginTop: 10,
                        borderRadius: 50,
                        alignItems: "center",
                      },
                    ]}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontSize: 18,
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      Добавить документ
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default DocumentScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  carNumber: {
    borderWidth: 2,
    borderColor: global.colors.mainColor,
    borderRadius: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
    marginTop: 10,
    height: 60,
  },
  carNumberInput: {
    fontSize: 36,
    fontFamily: global.fonts.medium,
    color: "#444444",
    textAlign: "center",
    alignItems: "center",
    width: "100%",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
});
