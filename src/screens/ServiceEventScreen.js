import React, { useContext, useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Modal,
  TouchableWithoutFeedback,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Service from "../../assets/service.svg";
import Road from "../../assets/road.svg";
import Fuel from "../../assets/fuel.svg";
import Dollar from "../../assets/dollar.svg";
import Calendar from "../../assets/calendar-time.svg";
// import MasterItem from "../components/MasterItem";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import Evakuator from "../../assets/evakuator.svg";
import DateTimePicker from "@react-native-community/datetimepicker";
import SuccessModal from "../components/SuccessModal";
import {formatDate, parseDate} from "../utils/date";

function ServiceEventScreen({ navigation, route }) {
  const { apiService, token, notification } = useContext(Context);
  const [order, setOrder] = useState(null);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [modal, setModal] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (notification) {
      if (typeof notification == "object") {
        getOrder();
      }
    }
  }, [notification]);
  useEffect(() => {
    getOrder();
  }, []);
  const onChangeDate = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setShowTime(true);
    }
  };
  const getOrder = () => {
    if (!loading) setLoading(true);
    apiService
      .getResources("/booking/order/" + event.id, token, auto.id)
      .then((value) => {
        setOrder(value.data);
        setLoading(false);
        setDate(new Date(value.data.orderDate));
      });
  };
  const acceptTime = () => {
    apiService
      .updateForm("/booking/order/book/" + event.id, token, {}, auto.id)
      .then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          reload();
          getOrder();
        } else {
          Alert.alert("Что-то пошло не так!");
        }
      });
  };
  const changeNewTime = () => {
    const body = {
      orderDate: parseDate(date),
      id: event.id,
      vehicleId: auto.id,
    };
    setModal(false);
    setLoading(true);
    apiService
      .updateForm("/booking/order/date-change", token, body, auto.id)
      .then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          setShowModal(true);
          reload();
          setTimeout(() => {
            setShowModal(false);
            navigation.goBack();
          }, 1500);
        } else {
          Alert.alert("Что-то пошло не так!");
        }
      });
  };
  const deleteEvent = () => {
    setLoading(true);
    apiService
      .updateForm("/booking/order/cancel/" + event.id, token, {}, auto.id)
      .then((value) => {
        if (value.statusCode == 200) {
          reload();
          navigation.goBack();
        } else {
          Alert.alert(value.message);
        }
        setLoading(false);
      });
  };
  const [loading, setLoading] = useState(true);
  const { params } = route;
  const { event, auto, reload } = params;
  const { type } = event;
const chosenDateTime = formatDate(date);
  const onChangeTime = (event, selectedTime) => {
    setShowTime(false);
    if (selectedTime) {
      setDateChosen(true);
      setDate(selectedTime);
    }
  };
  const TextBlock = ({ textLeft, textRight, colorLeft, colorRight }) => {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingTop: 5,
        }}
      >
        <Text
          style={{
            fontFamily: global.fonts.regular,
            fontSize: 9,
            color: "#999999",
          }}
        >
          {textLeft}
        </Text>
        <Text
          style={{
            fontFamily: global.fonts.regular,
            fontSize: 9,
            color: "#999999",
          }}
        >
          {textRight}
        </Text>
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  const getName = () => {
    if (order) return order.typeName;
    return "";
  };
  return (
    <>
      <HeaderBack
        navigation={navigation}
        title={getName()}
        button={true}
        type="white"
      />
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          display="default"
          onChange={onChangeDate}
          minimumDate={new Date()}
        />
      )}
      {showTime && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`time`}
          is24Hour={true}
          display="default"
          onChange={onChangeTime}
        />
      )}
      <SuccessModal showModal={showModal} setShowModal={setShowModal} />
      <Modal animationType="fade" transparent={true} visible={modal}>
        <TouchableWithoutFeedback
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
          onPress={() => setModal(false)}
        >
          <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
        </TouchableWithoutFeedback>
        <View
          style={{
            flexDirection: "row",
            position: "absolute",
            alignSelf: "center",
            justifyContent: "center",
            top: 0,
            left: 0,
            right: 0,
            height: global.strings.height,
            alignItems: "center",
          }}
        >
          <View style={styles.authorization}>
            <View
              style={[
                styles.authorizationBlock,
                { backgroundColor: "white", borderRadius: 20 },
              ]}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 24,
                  fontFamily: global.fonts.bold,
                  marginBottom: 20,
                }}
              >
                Дата и время
              </Text>
              <TouchableOpacity
                onPress={() => {
                  setShow(true);
                }}
              >
                <View style={styles.authorizationInput}>
                  <Text
                    style={[
                      {
                        lineHeight: 50,
                        color: "#444",
                        fontFamily: global.fonts.regular,
                      },
                    ]}
                  >
                    {chosenDateTime}
                  </Text>
                  <Calendar />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: "100%",
                  height: 50,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: dateChosen
                    ? global.colors.mainColor
                    : "#efefef",
                  borderRadius: 50,
                  marginTop: 10,
                }}
                onPress={() => changeNewTime()}
                disabled={!dateChosen}
              >
                <Text
                  style={{
                    fontFamily: global.fonts.regular,
                    fontSize: 18,
                    color: "white",
                  }}
                >
                  Отправить
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={{ height: 20 }} />
          <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
            <View style={[styles.authorization]}>
              <View
                style={{
                  backgroundColor: "white",
                  padding: 20,
                  borderRadius: 20,
                  shadowColor: "rgba(0,0,0,.15)",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.9,
                  shadowRadius: 3.84,
                  elevation: 5,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 20,
                  }}
                >
                  <View
                    style={[
                      styles.icon,
                      type === "SERVICE" || type === "ORDER" || type === "CALL"
                        ? styles.warn
                        : styles.noStyle,
                      type === "TRIP" ? styles.danger : styles.noStyle,
                      type === "EXPENSE" ? styles.tax : styles.noStyle,
                      type === "EARN"
                        ? { backgroundColor: "#6AD97B" }
                        : styles.noStyle,
                      type === "REFUEL"
                        ? { backgroundColor: "#B84BFF" }
                        : styles.noStyle,
                      type === "EVACUATOR" ? styles.danger : styles.noStyle,
                      type === "SOS" ? styles.danger : styles.noStyle,
                    ]}
                  >
                    {type === "SERVICE" ||
                    type === "ORDER" ||
                    type === "SOS" ||
                    type === "CALL" ? (
                      <Service />
                    ) : null}
                    {type === "TRIP" ? <Road /> : null}
                    {type === "EXPENSE" ? <Dollar /> : null}
                    {type === "REFUEL" ? <Fuel /> : null}
                    {type === "EARN" ? <Dollar /> : null}
                    {type === "EVACUATOR" ? <Evakuator width={28} /> : null}
                  </View>
                  <View
                    style={{
                      flex: 1,
                    }}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "flex-start",
                          justifyContent: "space-between",
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 14,
                            fontFamily: global.fonts.medium,
                            color: "#444444",
                            maxWidth: 150,
                          }}
                        >
                          {getName()}
                        </Text>
                        <Text
                          style={[
                            styles.infoText,
                            type === "SERVICE" ||
                            type === "ORDER" ||
                            type === "CALL"
                              ? styles.warnColor
                              : styles.noStyle,
                            type === "TRIP"
                              ? styles.dangerColor
                              : styles.noStyle,
                            type === "EXPENSE"
                              ? styles.taxColor
                              : styles.noStyle,
                            type === "REFUEL"
                              ? { color: "#B84BFF" }
                              : styles.noStyle,
                            type === "EARN"
                              ? { color: "#6AD97B" }
                              : styles.noStyle,
                            type === "EVACUATOR"
                              ? styles.dangerColor
                              : styles.noStyle,
                            type === "SOS"
                              ? styles.dangerColor
                              : styles.noStyle,
                          ]}
                        >
                          {order ? order.statusName : null}
                        </Text>
                      </View>

                      {order && (
                        <TextBlock
                          textLeft={"Номер авто:"}
                          textRight={order.vehicle.plateNumber}
                        />
                      )}
                      <TextBlock
                        textLeft="Дата и время:"
                        textRight={chosenDateTime}
                      />
                      {order && (
                        <TextBlock
                          textLeft={"Покозание одометра:"}
                          textRight={order.vehicle.currentMileage}
                        />
                      )}
                      {order && (
                        <TextBlock
                          textLeft={"Номер телефона:"}
                          textRight={order.phone}
                        />
                      )}
                      {order && (
                        <TextBlock
                          textLeft="Имя мастера:"
                          textRight={order.technicianName}
                        />
                      )}
                      {order && (
                        <TextBlock
                          textLeft="Сумма расхода:"
                          textRight={order.total + " UZS"}
                        />
                      )}
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 12,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: global.fonts.medium,
                      fontSize: 14,
                      color: "#444444",
                    }}
                  >
                    {order
                      ? order.note
                        ? order.note
                        : "Нет комментария"
                      : null}
                  </Text>
                </View>
                {order ? (
                  order.status == "PENDING" ||
                  order.status == "CHANGING_DATE" ? (
                    <TouchableOpacity
                      style={{
                        width: "100%",
                        height: 50,
                        alignItems: "center",
                        justifyContent: "center",
                        backgroundColor: global.colors.danger,
                        borderRadius: 50,
                        marginTop: 10,
                      }}
                      onPress={() => deleteEvent()}
                    >
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 18,
                          color: "white",
                        }}
                      >
                        Отменить вызов
                      </Text>
                    </TouchableOpacity>
                  ) : null
                ) : null}
              </View>
            </View>
            {order
              ? order.status == "CHANGING_DATE" && (
                  <View style={[styles.authorization, { marginTop: 20 }]}>
                    <View
                      style={{
                        backgroundColor: "white",
                        padding: 20,
                        borderRadius: 20,
                        shadowColor: "rgba(0,0,0,.15)",
                        shadowOffset: {
                          width: 0,
                          height: 2,
                        },
                        shadowOpacity: 0.9,
                        shadowRadius: 3.84,
                        elevation: 5,
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          marginBottom: 20,
                        }}
                      >
                        <View style={[styles.icon, { overflow: "hidden" }]}>
                          <Image
                            source={global.images.repair}
                            style={{
                              width: "100%",
                              height: "100%",
                              borderRadius: 10,
                              resizeMode: "cover",
                              backgroundColor: global.colors.mainColor,
                            }}
                          />
                        </View>
                        <View
                          style={{
                            flex: 1,
                          }}
                        >
                          <View>
                            <View
                              style={{
                                flexDirection: "row",
                                alignItems: "flex-start",
                                justifyContent: "space-between",
                              }}
                            >
                              <Text
                                style={{
                                  fontSize: 14,
                                  fontFamily: global.fonts.medium,
                                  color: "#444444",
                                  maxWidth: 160,
                                }}
                              >
                                {order ? order.technicianName : ""}
                              </Text>
                              <Text style={[styles.infoText]}>Правки</Text>
                            </View>
                            <TextBlock
                              textLeft="Дата и время:"
                              textRight={chosenDateTime}
                            />
                          </View>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            // width: global.strings.width / 2.7,
                            flex: 1,
                            height: 50,
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: global.colors.mainColor,
                            borderRadius: 50,
                            marginTop: 10,
                          }}
                          onPress={() => setModal(true)}
                        >
                          <Text
                            style={{
                              fontFamily: global.fonts.regular,
                              fontSize: 18,
                              color: "white",
                            }}
                          >
                            Предложить
                          </Text>
                        </TouchableOpacity>
                        <View width={10} />
                        <TouchableOpacity
                          style={{
                            // width: global.strings.width / 2.7,
                            flex: 1,
                            height: 50,
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: global.colors.success,
                            borderRadius: 50,
                            marginTop: 10,
                          }}
                          onPress={() => acceptTime()}
                        >
                          <Text
                            style={{
                              fontFamily: global.fonts.regular,
                              fontSize: 18,
                              color: "white",
                            }}
                          >
                            Применить
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                )
              : null}
          </View>
        </View>
      </View>
    </>
  );
}

export default ServiceEventScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
    width: "100%",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.warn,
    borderRadius: 50,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 3,
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: global.colors.mainColor,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  infoText: {
    fontFamily: global.fonts.medium,
    fontSize: 14,
  },
  noStyle: {},
  warn: {
    backgroundColor: global.colors.warn,
  },
  danger: {
    backgroundColor: global.colors.danger,
  },
  tax: {
    backgroundColor: global.colors.preDanger,
  },
  warnColor: {
    color: global.colors.warn,
  },
  dangerColor: {
    color: global.colors.danger,
  },
  taxColor: {
    color: global.colors.preDanger,
  },
  itemIcon: {
    height: 46,
    width: 46,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});
