import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
} from "react-native";
import global from "../../resources/global";
import Service from "../../assets/service.svg";
import HeaderBack from "../components/HeaderBack";
import Oil from "../../assets/oil.svg";
import Fuel from "../../assets/fuel.svg";
import CarWash from "../../assets/car-wash.svg";
import Evakuator from "../../assets/evakuator.svg";
import Heart from "../../assets/heart.svg";

function ShowLocatorScreen({ navigation, route }) {
  const { params } = route;
  const { service, type } = params;

  const now = new Date();
  const weekDay = now.getDay();
  const findServiceType = (code) => {
    if (code == "CAR_WASH")
      return <CarWash width={24} style={{ resizeMode: "contain" }} />;
    if (code == "AUTO_SERVICE")
      return <Service width={24} style={{ resizeMode: "contain" }} />;
    if (code == "GAS_STATION")
      return <Fuel width={24} style={{ resizeMode: "contain" }} />;
    if (code == "OIL_STATION")
      return <Oil width={24} style={{ resizeMode: "contain" }} />;
    if (code == "EVAСUATOR")
      return <Evakuator width={24} style={{ resizeMode: "contain" }} />;
    if (
      code != "CAR_WASH" &&
      code != "AUTO_SERVICE" &&
      code != "GAS_STATION" &&
      code != "OIL_STATION" &&
      code != "EVAСUATOR"
    )
      return <Heart width={24} style={{ resizeMode: "contain" }} />;
  };
  const openGps = (lat, lng) => {
    const scheme = Platform.select({
      ios: "maps:0,0?q=",
      android: "geo:0,0?q=",
    });
    const latLng = `${lat},${lng}`;
    const label = service.name;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`,
    });
    Linking.openURL(url);
  };
  const isOpen = (item = null) => {
    const getCurrent = item.timeslots[weekDay];
    if (!getCurrent) return "Закрыто";
    if (
      now.getHours() > parseInt(getCurrent.startTime.slice(0, 2)) &&
      now.getHours() < parseInt(getCurrent.endTime.slice(0, 2))
    ) {
      return "Открыто";
    } else {
      return "Закрыто";
    }
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          navigation={navigation}
          title={service.name}
          button={true}
          type="white"
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
            <View style={{ height: 20 }} />
            <View style={[styles.authorization]}>
              <View
                style={{
                  backgroundColor: "white",
                  padding: 20,
                  borderRadius: 20,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginBottom: 20,
                  }}
                >
                  <View style={styles.icon}>{findServiceType(type)}</View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <View>
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: global.fonts.medium,
                          color: "#444444",
                          marginBottom: 5,
                        }}
                      >
                        {service.name}
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: "#999999",
                          marginBottom: 5,
                        }}
                      >
                        {service.categoryName}
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: "#999999",
                        }}
                      >
                        {typeof service.timeslots[weekDay] !== "undefined"
                          ? service.timeslots[weekDay].startTime
                          : "Не задан"}{" "}
                        -{" "}
                        {typeof service.timeslots[weekDay] !== "undefined"
                          ? service.timeslots[weekDay].endTime
                          : "Не задан"}
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: global.fonts.medium,
                          color: "#444444",
                          textAlign: "right",
                          marginBottom: 5,
                        }}
                      >
                        {Math.ceil(service.distance / 100) / 10} KM
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: global.colors.success,
                          textAlign: "right",
                          marginBottom: 5,
                        }}
                      >
                        {isOpen(service)}
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: "#999999",
                          textAlign: "right",
                        }}
                      >
                        {service.coordinate ? service.coordinate.address : ""}
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  {service.categoryCode != "OIL_STATION" && (
                    <TouchableOpacity
                      style={[
                        styles.bigButton,
                        {
                          shadowColor: "rgba(0,0,0,.15)",
                          shadowOffset: {
                            width: 0,
                            height: 2,
                          },
                          shadowOpacity: 0.8,
                          shadowRadius: 3.84,
                          elevation: 5,
                        },
                      ]}
                      onPress={() =>
                        openGps(service.coordinate.lat, service.coordinate.lng)
                      }
                    >
                      <Text
                        style={{
                          color: "white",
                          fontSize: 14,
                          fontFamily: global.fonts.regular,
                        }}
                      >
                        Маршрут
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default ShowLocatorScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
    width: "100%",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.mainColor,
    borderRadius: 50,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 3,
  },
  bigButton: {
    // width: 140,
    flex: 1,
    height: 50,
    backgroundColor: global.colors.mainColor,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
