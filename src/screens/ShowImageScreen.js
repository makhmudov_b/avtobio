import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
  TextInput,
} from "react-native";
import global from "../../resources/global";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";

const ShowImageScreen = ({ route, navigation }) => {
  const { file } = route.params;
  return (
    <View style={styles.container}>
      <HeaderBack
        type="white"
        button={true}
        title={"Изображение"}
        navigation={navigation}
      />
      <View
        style={{
          flex: 1,
          width: "100%",
          padding: 10,
        }}
      >
        <Image
          source={{ uri: file }}
          style={{
            width: "100%",
            height: "100%",
            borderRadius: 20,
            overflow: "hidden",
          }}
          resizeMode="contain"
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  carNumber: {
    borderWidth: 2,
    borderColor: global.colors.mainColor,
    borderRadius: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
    marginTop: 10,
    height: 60,
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 20,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
  },
  shadow: {
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    width: "100%",
  },
  icon: {
    position: "absolute",
    right: 15,
    top: 13,
  },
});

export default ShowImageScreen;
