import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Image,
} from "react-native";
import global from "../../resources/global";
// import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import ArrowBack from "../../assets/arrow-blue.svg";
import ArrowWhite from "../../assets/arrow.svg";
import CustomStatusBar from "../components/CustomStatusBar";

function TypeChoiceScreen({ navigation, route }) {
  const { apiService, token } = useContext(Context);
  const { params } = route;
  const { title, setMethod, requestId } = params;
  const [choices, setChoices] = useState([]);
  const [loading, setLoading] = useState(true);
  const [chosen, setChosen] = useState(null);
  const reasonCheck = title === "Причина вызова" || title === "Причина визита";
  const getData = (url) => {
    if (reasonCheck) return apiService.getReferences(url, token);
    return apiService.getResources(url, token);
  };
  const getUrl = () => {
    if (reasonCheck)
      return "/backend/v1/reference/choose?parentCode=AUTO_SERVICE";
  };
  const submit = (choice) => {
    setMethod(choice);
    navigation.goBack();
  };
  const chooseParent = (choice) => {
    if (!chosen) {
      setChosen(choice);
      setLoading(true);
      getData("/backend/v1/reference/choose?parentCode=" + choice.code).then(
        (value) => {
          if (value.data) setChoices(value.data);
          setLoading(false);
        }
      );
    } else {
      submit(choice);
    }
  };
  useEffect(() => {
    getData(getUrl()).then((value) => {
      if (value.data) setChoices(value.data);
      setLoading(false);
    });
  }, []);
  const HeaderBacker = ({ button, title, type }) => {
    const headerColor = type === "white" ? "white" : global.colors.mainColor;
    const [disabled, setDisabled] = useState(false);
    const tapMethod = () => {
      navigation.goBack();
      setDisabled(true);
      setTimeout(() => setDisabled(true), 800);
    };
    return (
      <>
        <CustomStatusBar type={type} />
        <View style={[styles.header, { backgroundColor: headerColor }]}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingBottom: 20,
              paddingTop: 10,
            }}
          >
            <View>
              {button ? (
                <TouchableOpacity
                  disabled={disabled}
                  onPress={() => tapMethod()}
                  style={{ padding: 5 }}
                >
                  <ArrowBack width={30} height={30} />
                </TouchableOpacity>
              ) : (
                <View style={{ opacity: 0 }}>
                  <ArrowBack width={30} height={30} />
                </View>
              )}
            </View>
            <Text style={styles.title}>{title}</Text>
            <View style={{ opacity: 0 }}>
              <ArrowBack width={30} height={30} />
            </View>
          </View>
          {chosen && (
            <View>
              <TouchableOpacity
                style={{ marginBottom: 20, marginHorizontal: 10 }}
                onPress={() => submit(chosen)}
              >
                <View
                  style={{
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: "100%",
                    flexDirection: "row",
                    backgroundColor: global.colors.mainColor,
                    borderColor: global.colors.mainColor,
                    paddingHorizontal: 15,
                    height: 50,
                    borderWidth: 1,
                    borderRadius: 25,
                  }}
                >
                  <Text style={[styles.authorizationInput, { color: "white" }]}>
                    Я не знаю причину
                  </Text>
                  <ArrowWhite
                    height={20}
                    style={{ transform: [{ rotate: "180deg" }] }}
                  />
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </>
    );
  };
  return (
    <>
      <HeaderBacker type="white" button={true} title={title} />
      <ScrollView style={styles.container}>
        <View style={{ height: 20 }} />
        <View style={[styles.whiteBox, { marginBottom: 20 }]}>
          {!loading ? (
            choices.length ? (
              choices.map((choice, key) => {
                return (
                  <TouchableOpacity
                    style={{ marginBottom: 10 }}
                    key={key}
                    onPress={() => chooseParent(choice)}
                  >
                    <View
                      style={{
                        justifyContent: "space-between",
                        alignItems: "center",
                        width: "100%",
                        flexDirection: "row",
                        backgroundColor: "white",
                        borderColor: "#E9E9E9",
                        paddingHorizontal: 15,
                        height: 50,
                        borderWidth: 1,
                        borderRadius: 25,
                      }}
                    >
                      <Text style={styles.authorizationInput}>
                        {choice.name.slice(0, 24)}
                        {choice.name.length > 24 && "..."}
                      </Text>
                      <ArrowBack
                        height={20}
                        style={{ transform: [{ rotate: "180deg" }] }}
                      />
                    </View>
                  </TouchableOpacity>
                );
              })
            ) : null
          ) : (
            <Image
              source={global.images.loader}
              style={{ height: 200, width: 200, alignSelf: "center" }}
            />
          )}
        </View>
      </ScrollView>
    </>
  );
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#F6F6F6" },
  left: {
    marginRight: 10,
  },
  right: {
    marginLeft: 10,
  },
  header: {
    paddingHorizontal: 10,
    minHeight: 65,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1.65,
    elevation: 2,
    // marginBottom: 20,
  },
  title: {
    color: "#444444",
    flex: 1,
    textAlign: "center",
    fontFamily: global.fonts.medium,
    fontSize: 24,
  },
  whiteBox: {
    backgroundColor: "white",
    marginRight: 20,
    marginLeft: 20,
    padding: 20,
    shadowColor: "#000",
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  box: {
    height: 60,
    width: 60,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  authorizationInput: {
    fontSize: 16,
    fontFamily: global.fonts.regular,
    // textAlign: "left",
  },
});
export default TypeChoiceScreen;
