import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import ArrowRight from "../../assets/arrow-right.svg";
import Metr from "../../assets/kilometr.svg";
import Calendar from "../../assets/calendar-time.svg";
import Editable from "../../assets/edit.svg";
import Location from "../../assets/location.svg";
import { SafeAreaProvider } from "react-native-safe-area-context";
import SafeAreaView from "react-native-safe-area-view";

function CheckoutScreen({ navigation }) {
  const [confirmation, onChangeConfirmation] = useState("");
  const [language, onChangeLang] = useState(0);
  const RightBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Дата и время"
            placeholderTextColor="#999999"
            value={confirmation}
          />
          <Calendar style={styles.icon} />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Тип сервиса"
            placeholderTextColor="#999999"
            value={confirmation}
          />
          <View style={styles.icon}>
            <ArrowRight />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Имя мастера"
            placeholderTextColor="#999999"
            value={confirmation}
          />
          <View style={styles.icon}>
            <ArrowRight />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={[styles.authorizationInput]}
            placeholder="Сумма расхода"
            placeholderTextColor="#999999"
            value={[confirmation]}
          />
          <View style={[styles.icon, { width: 35 }]}>
            <Text style={{ fontSize: 18, color: "#999999" }}>UZS</Text>
          </View>
        </View>
        <Text
          style={{
            textAlign: "center",
            paddingHorizontal: 20,
            paddingBottom: 10,
            fontSize: 11,
            color: global.colors.danger,
          }}
        >
          Указанная сумма расхода НЕ включает в себя расходники такие как
          запчасти, масла и т.д.
        </Text>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Пробег"
            placeholderTextColor="#999999"
            value={confirmation}
          />
          <View style={styles.icon}>
            <Metr />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Номер телефона"
            placeholderTextColor="#999999"
            value={confirmation}
          />
          <View style={styles.icon}>
            <Editable />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={confirmation}
          />
        </View>
      </View>
    );
  };
  const LeftBlock = () => {
    return (
      <View>
        <Text
          style={{
            textAlign: "center",
            fontFamily: global.fonts.medium,
            fontSize: 18,
            paddingBottom: 15,
          }}
        >
          Детали заказа
        </Text>
        <View style={{ position: "relative" }}>
          <TouchableOpacity onPress={() => navigation.navigate("AddressMap")}>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#999" },
              ]}
            >
              Адрес доставки
            </Text>
          </TouchableOpacity>
          <View style={styles.icon}>
            <Location />
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            paddingBottom: 10,
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => onChangeLang(0)}
            style={[
              styles.leftBlock,
              styles.block,
              !language ? styles.activeBlock : styles.inactiveBlock,
            ]}
          >
            <Text style={!language ? styles.activeText : styles.inactiveText}>
              Наличные
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onChangeLang(1)}
            style={[
              styles.rightBlock,
              styles.block,
              language ? styles.activeBlock : styles.inactiveBlock,
            ]}
          >
            <Text style={language ? styles.activeText : styles.inactiveText}>
              Онлайн
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#999" },
              ]}
            >
              Дата и время
            </Text>
          </TouchableOpacity>
          <View style={styles.icon}>
            <Calendar />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Номер телефона"
            keyboardType={"phone-pad"}
            placeholderTextColor="#999999"
            value={confirmation}
          />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={confirmation}
          />
        </View>
      </View>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }}>
          <StatusBar backgroundColor="#F6F6F6" barStyle="dark-content" />
          <View
            style={{
              justifyContent: "center",
              paddingBottom: 50,
              position: "relative",
              zIndex: 20,
              marginTop: 40,
            }}
          >
            <View
              style={{ zIndex: 10, position: "absolute", top: 2, left: 20 }}
            >
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Arrow />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                textAlign: "center",
                color: "#444444",
                fontSize: 24,
                fontFamily: global.fonts.medium,
              }}
            >
              Оформление заказа
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <LeftBlock />
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <Text
                  style={{
                    textAlign: "center",
                    fontFamily: global.fonts.medium,
                    color: "#444444",
                    fontSize: 18,
                    paddingBottom: 10,
                  }}
                >
                  Сводка по счету
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5,
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={{ fontSize: 14, color: "#999999" }}>
                    Сумма за товары:
                  </Text>
                  <Text style={{ fontSize: 14, color: "#999999" }}>
                    1 200 000 UZS
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5,
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={{ fontSize: 14, color: "#999999" }}>
                    Сумма за доставку:
                  </Text>
                  <Text style={{ fontSize: 14, color: "#999999" }}>
                    15 000 UZS
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 15,
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      color: "#444444",
                      fontFamily: global.fonts.medium,
                    }}
                  >
                    Общая сумма:
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: "#444444",
                      fontFamily: global.fonts.medium,
                    }}
                  >
                    1 215 000 UZS
                  </Text>
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.success },
              ]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Оформить заказ
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default CheckoutScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    flex: 1,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderRadius: 60,
    marginRight: 10,
  },
  rightBlock: {
    borderRadius: 60,
    marginLeft: 10,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 18,
  },
});
