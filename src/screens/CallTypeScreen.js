import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-red.svg";
import ArrowRight from "../../assets/arrow-right-red.svg";
import Edit from "../../assets/edit-red.svg";
import Calendar from "../../assets/calendar-time-red.svg";
import { SafeAreaProvider } from "react-native-safe-area-context";
import SafeAreaView from "react-native-safe-area-view";
import Constants from "expo-constants";

function CallTypeScreen({ navigation }) {
  const [confirmation, onChangeConfirmation] = useState("");
  const LeftBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              Срочно
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              В течении дня
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              В течении недели
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              Дата и время
            </Text>
          </TouchableOpacity>
          <View style={styles.icon}>
            <Calendar />
          </View>
        </View>
      </View>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              backgroundColor: "#F6F6F6",
              height: Constants.statusBarHeight,
            }}
          ></View>
          <StatusBar barStyle={"dark-content"} />
          <View
            style={{
              justifyContent: "center",
              paddingBottom: 50,
              position: "relative",
              zIndex: 20,
              marginTop: 20,
            }}
          >
            <View
              style={{ zIndex: 10, position: "absolute", top: 2, left: 20 }}
            >
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Arrow />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                textAlign: "center",
                color: "#444444",
                fontSize: 24,
                fontWeight: "bold",
              }}
            >
              Тип вызова
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <LeftBlock />
              </View>
            </View>
            <TouchableOpacity
              style={[styles.button, { backgroundColor: global.colors.danger }]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Вызвать эвакуатор
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default CallTypeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    textAlign: "center",
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 18,
  },
});
