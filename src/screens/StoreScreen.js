import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  Modal,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Search from "../../assets/search.svg";
import ArrowRight from "../../assets/arrow-right.svg";
import Tormoz from "../../assets/tormoz.svg";
import Heart from "../../assets/heart-blue.svg";
import HeartActive from "../../assets/heart-red.svg";
import Constants from "expo-constants";
import StoreTabbar from "../components/StoreTabbar";
import ProductItem from "../components/ProductItem";
import CartItem from "../components/CartItem";
import HeaderBack from "../components/HeaderBack";

function StoreScreen({ navigation }) {
  const [currentScreen, changeCurrentScreen] = useState(1);
  const [category, changeCategory] = useState(0);
  const [like, toggleLike] = useState(0);
  const [scrollItem] = useState([
    { name: 1 },
    { name: 1 },
    { name: 1 },
    { name: 1 },
    { name: 1 },
  ]);
  const changeScreen = function (index) {
    changeCurrentScreen(index);
    changeCategory(0);
  };
  const [modalVisible, setModalVisible] = useState(false);
  const ScrollItem = () => {
    return (
      <TouchableOpacity style={styles.block} onPress={() => changeCategory(1)}>
        <View style={styles.blockIcon}>
          <Tormoz />
        </View>
        <Text
          style={{
            maxWidth: 75,
            marginTop: 8,
            color: "#444",
            fontSize: 14,
            fontFamily: global.fonts.medium,
            textAlign: "center",
          }}
        >
          Тормозная система
        </Text>
      </TouchableOpacity>
    );
  };
  const MainBlock = () => {
    return (
      <>
        <Modal animationType="fade" transparent={true} visible={modalVisible}>
          <TouchableWithoutFeedback
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
            onPress={() => setModalVisible(false)}
          >
            <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
          </TouchableWithoutFeedback>
          <View
            style={{
              flexDirection: "row",
              position: "absolute",
              marginTop: 60,
              alignSelf: "center",
            }}
          >
            <View
              style={[
                styles.authorizationBlock,
                { borderRadius: 20, backgroundColor: "white" },
              ]}
            >
              <View style={{ position: "relative" }}>
                <TouchableOpacity
                  style={[styles.authorizationInput]}
                  onPress={() => setModalVisible(true)}
                >
                  <Text
                    style={{ color: "#999999", fontSize: 18, paddingRight: 40 }}
                  >
                    Моторные масла
                  </Text>
                </TouchableOpacity>
                <ArrowRight style={styles.icon} />
              </View>
              <View style={{ position: "relative" }}>
                <TouchableOpacity style={[styles.authorizationInput]}>
                  <Text
                    style={{ color: "#999999", fontSize: 18, paddingRight: 40 }}
                  >
                    Масла трансмиссионные
                  </Text>
                </TouchableOpacity>
                <ArrowRight style={styles.icon} />
              </View>
            </View>
          </View>
        </Modal>
        <HeaderBack
          type="white"
          button={true}
          title="Магазин"
          navigation={navigation}
        />
        <View style={{ height: 20 }} />
        <View
          style={{
            flex: 1,
            width: "100%",
            backgroundColor: "#F6F6F6",
            alignItems: "center",
          }}
        >
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <View style={{ position: "relative" }}>
                <TouchableOpacity
                  style={styles.authorizationInput}
                  onPress={() => setModalVisible(true)}
                >
                  <Text style={{ color: "#999999", fontSize: 18 }}>
                    Марка автомобиля
                  </Text>
                </TouchableOpacity>
                <ArrowRight style={styles.icon} />
              </View>
              <View style={{ position: "relative" }}>
                <TouchableOpacity style={styles.authorizationInput}>
                  <Text style={{ color: "#999999", fontSize: 18 }}>Модель</Text>
                </TouchableOpacity>
                <ArrowRight style={styles.icon} />
              </View>
            </View>
          </View>
          <ScrollView
            horizontal={true}
            style={{ paddingLeft: 20, maxHeight: 150 }}
            showsHorizontalScrollIndicator={false}
          >
            {scrollItem.map((item, key) => (
              <ScrollItem key={key} />
            ))}
          </ScrollView>
          <ScrollView
            horizontal={true}
            style={{ paddingLeft: 20, maxHeight: 150 }}
            showsHorizontalScrollIndicator={false}
          >
            {scrollItem.map((item, key) => (
              <ScrollItem key={key} />
            ))}
          </ScrollView>
        </View>
      </>
    );
  };
  const FavouriteBlock = () => {
    return (
      <>
        <Modal animationType="fade" transparent={true} visible={modalVisible}>
          <TouchableWithoutFeedback
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
            onPress={() => setModalVisible(false)}
          >
            <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
          </TouchableWithoutFeedback>
          <View
            style={{
              borderRadius: 20,
              paddingBottom: 20,
              backgroundColor: "white",
              position: "relative",
              alignItems: "center",
              width: "85%",
              position: "absolute",
              top: "15%",
              alignSelf: "center",
            }}
          >
            <TouchableOpacity
              style={{ zIndex: 10, position: "absolute", top: 20, right: 20 }}
            >
              {like ? (
                <HeartActive width={25} height={25} />
              ) : (
                <Heart width={25} height={25} />
              )}
            </TouchableOpacity>
            <Image
              source={global.images.product}
              style={{ height: 280, resizeMode: "cover" }}
            />
            <Text
              style={{
                textAlign: "center",
                fontSize: 24,
                maxWidth: 230,
                color: "#444444",
                lineHeight: 24,
                fontFamily: global.fonts.medium,
              }}
            >
              Rosmeft Maksimum 10W 4L
            </Text>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Производитель:</Text>
              <Text style={styles.wrapText}>Lotos</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Марка:</Text>
              <Text style={styles.wrapText}>Rosmeft</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Объем:</Text>
              <Text style={styles.wrapText}>4L</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Тип качества:</Text>
              <Text style={styles.wrapText}>Maksimum 10W 4L</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Maksimum 10W 4L</Text>
              <Text style={styles.wrapText}>90 000 UZS</Text>
            </View>
          </View>
        </Modal>
        <HeaderBack
          type="white"
          button={true}
          title="Избранные"
          navigation={navigation}
        />
        <ScrollView style={{ width: "100%", backgroundColor: "#F6F6F6" }}>
          <View style={{ height: 20 }} />
          <View
            style={{
              flex: 1,
              paddingHorizontal: 20,
              flexDirection: "row",
              flexWrap: "wrap",
              width: "100%",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
          </View>
        </ScrollView>
      </>
    );
  };
  const CartBlock = () => {
    return (
      <>
        <HeaderBack
          type="white"
          button={true}
          title="Корзина"
          navigation={navigation}
        />
        <ScrollView
          style={{ width: "100%", flex: 1, backgroundColor: "#F6F6F6" }}
        >
          <View style={{ height: 20 }} />
          <CartItem />
          <CartItem />
          <CartItem />
          <CartItem />
          <CartItem />
          <CartItem />
          <CartItem />
          <CartItem />
          <CartItem />
        </ScrollView>
        <TouchableOpacity
          style={{
            width: "80%",
            height: 50,
            marginVertical: 15,
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 50,
            backgroundColor: global.colors.success,
          }}
          onPress={() => navigation.navigate("Checkout")}
        >
          <Text style={{ fontSize: 18, color: "white" }}>
            Перейти к оформлению
          </Text>
        </TouchableOpacity>
      </>
    );
  };
  const ShowScreen = () => {
    return (
      <>
        <Modal animationType="fade" transparent={true} visible={modalVisible}>
          <TouchableWithoutFeedback
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
            onPress={() => setModalVisible(false)}
          >
            <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
          </TouchableWithoutFeedback>
          <View
            style={{
              borderRadius: 20,
              paddingBottom: 20,
              backgroundColor: "white",
              position: "relative",
              alignItems: "center",
              width: "85%",
              position: "absolute",
              top: "15%",
              alignSelf: "center",
            }}
          >
            <TouchableOpacity
              style={{ zIndex: 10, position: "absolute", top: 20, right: 20 }}
            >
              {like ? (
                <HeartActive width={25} height={25} />
              ) : (
                <Heart width={25} height={25} />
              )}
            </TouchableOpacity>
            <Image
              source={global.images.product}
              style={{ height: 280, resizeMode: "cover" }}
            />
            <Text
              style={{
                textAlign: "center",
                fontSize: 24,
                maxWidth: 230,
                color: "#444444",
                lineHeight: 24,
                fontFamily: global.fonts.medium,
              }}
            >
              Rosmeft Maksimum 10W 4L
            </Text>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Производитель:</Text>
              <Text style={styles.wrapText}>Lotos</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Марка:</Text>
              <Text style={styles.wrapText}>Rosmeft</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Объем:</Text>
              <Text style={styles.wrapText}>4L</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Тип качества:</Text>
              <Text style={styles.wrapText}>Maksimum 10W 4L</Text>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.wrapText}>Maksimum 10W 4L</Text>
              <Text style={styles.wrapText}>90 000 UZS</Text>
            </View>
          </View>
        </Modal>
        <HeaderBack
          type="white"
          button={true}
          title="Моторные масла"
          navigation={navigation}
        />
        <ScrollView style={{ width: "100%", backgroundColor: "#F6F6F6" }}>
          <View style={{ height: 20 }} />
          <View
            style={{
              flex: 1,
              paddingHorizontal: 20,
              flexDirection: "row",
              flexWrap: "wrap",
              width: "100%",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
            <ProductItem showModal={() => setModalVisible()} />
          </View>
        </ScrollView>
      </>
    );
  };
  return (
    <>
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "flex-end",
          backgroundColor: "#F6F6F6",
        }}
      >
        {currentScreen === 0 ? <FavouriteBlock /> : null}
        {currentScreen === 1 && category === 0 ? <MainBlock /> : null}
        {currentScreen === 2 ? <CartBlock /> : null}
        {currentScreen === 1 && category > 0 ? <ShowScreen /> : null}
        <StoreTabbar navigation={navigation} chooseScreen={changeScreen} />
      </View>
    </>
  );
}

export default StoreScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  wrap: {
    width: "100%",
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  wrapText: {
    fontSize: 14,
    color: "#999999",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginBottom: 15,
  },
  gazShadow: {
    backgroundColor: "white",
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.05)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    width: 135,
    height: 135,
    marginRight: 10,
    alignSelf: "center",
    backgroundColor: "white",
    shadowColor: "rgba(0,0,0,.05)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 3.84,
    elevation: 2,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  blockIcon: {
    width: 60,
    height: 60,
    borderRadius: 10,
    backgroundColor: global.colors.mainColor,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  inactiveGaz: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "white",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    padding: 15,
    borderWidth: 1,
    height: 60,
    justifyContent: "center",
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 18,
  },
});
