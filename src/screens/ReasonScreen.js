import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  Image,
} from "react-native";
import global from "../../resources/global";
import Chosen from "../../assets/chosen.svg";
import NotChosen from "../../assets/not-chosen.svg";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";

function ReasonScreen({ navigation, route }) {
  const { apiService, token } = useContext(Context);
  const { params } = route;
  const { method } = params;
  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([]);
  const getData = (url) => {
    return apiService.getReferences(url, token);
  };
  const getUrl = () => {
    return "/backend/v1/reference/choose?parentCode=AUTO_SERVICE";
    if (title === "Причина вызова")
      return "backend/v1/reference/choose?parentCode=AUTO_SERVICE";
    if (title === "Модель") return "/vehicle/models/" + requestId;
    if (title === "Модификация") return "/vehicle/modifications/" + requestId;
  };
  useEffect(() => {
    getData(getUrl())
      .then((value) => {
        if (value.data) setList(value.data);
        setLoading(false);
      })
      .catch((e) => Alert.alert("Что-то пошло не так"));
  }, []);
  const submit = () => {
    const getCheckedList = list.filter((item) => item.checked === true);
    if (!getCheckedList.length) {
      Alert.alert("Выберите из списка");
    } else {
      method(getCheckedList);
      navigation.goBack();
    }
  };
  function handleCheckBox(itemId) {
    let getList = list.map((item) => {
      if (item.id === itemId) item.checked = !item.checked;
      item.price = null;
      item.serviceId = item.id;
      item.serviceName = item.name;
      return item;
    });
    setList(getList);
  }
  const ChoiceBlock = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => handleCheckBox(item.id)}>
        <View style={{ position: "relative" }}>
          <View style={styles.icon}>
            {item.checked ? <Chosen /> : <NotChosen />}
          </View>
          <Text
            style={[
              styles.authorizationInput,
              {
                lineHeight: 50,
                color: "#444444",
              },
            ]}
          >
            <Text>{item.name}</Text>
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title={params.title}
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                {!loading ? (
                  list.map((item, key) => {
                    return <ChoiceBlock key={key} item={item} />;
                  })
                ) : (
                  <Image
                    source={global.images.loader}
                    style={{ height: 200, width: 200, alignSelf: "center" }}
                  />
                )}
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.button,
                {
                  backgroundColor: global.colors.mainColor,
                  marginBottom: 30,
                },
              ]}
              onPress={() => submit()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Выбрать
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default ReasonScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
    marginBottom: 10,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingLeft: 60,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    left: 20,
    top: 13,
  },
});
