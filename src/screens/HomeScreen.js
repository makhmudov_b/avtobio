import React, { useState, useEffect, useContext, memo, useRef } from "react";
import {
  ScrollView,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  Image,
  ActivityIndicator,
  Alert,
  Platform,
} from "react-native";
import Tabbar from "../components/Tabbar";
import MainItem from "../components/MainItem";
import GarageItem from "../components/GarageItem";
import ReminderItem from "../components/ReminderItem";
import global from "../../resources/global";
import Hamburger from "../../assets/hamburger.svg";
import NoCar from "../../assets/no-car.svg";
import NoEvent from "../../assets/no-event.svg";
import NoReminder from "../../assets/no-reminder.svg";
import Carousel from "react-native-snap-carousel";
import Context from "../components/Context";
import CarNumber from "../components/CarNumber";
import CustomStatusBar from "../components/CustomStatusBar";
// import { scrollInterpolator, animatedStyles } from "../utils/animated";
const width = global.strings.width;

function HomeScreen({ navigation }) {
  const {
    apiService,
    token,
    autos,
    setReminderCount,
    reminderCount,
  } = useContext(Context);
  const [currentScreen, changeCurrentScreen] = useState(1);
  const AddAutoButton = () => {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          style={[
            styles.button,
            { backgroundColor: global.colors.mainColor, marginBottom: 50 },
          ]}
          onPress={() => navigation.navigate("AddAuto")}
        >
          <Text
            style={{
              color: "white",
              fontSize: 18,
              fontFamily: global.fonts.regular,
            }}
          >
            Добавить авто
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  const SliderItem = ({ item, key }) => {
    return (
      <View
        style={[
          {
            alignItems: "center",
            width: "100%",
            // maxWidth: 20,
            justifyContent: "center",
            paddingBottom: 20,
            alignSelf: "center",
            zIndex: 20,
          },
        ]}
      >
        <CarNumber autoInfo={item.plateNumber} />
      </View>
    );
  };
  const MainScreen = () => {
    const [index, setIndex] = useState(0);
    const [loading, setLoading] = useState(true);
    const [events, setEvents] = useState([]);
    // const isMounted = useRef(false);
    const setCurrentCar = (key) => {
      setIndex(key);
      getEvents(key);
    };
    // useEffect(() => {
    //   if (autos.length > 0) {
    //     const unsubscribe = navigation.addListener("focus", () => {
    //       if (!loading) {
    //         getEvents(index);
    //         // console.warn(`key : ${key} inde: ${index}`);
    //       }
    //     });
    //     return unsubscribe;
    //   }
    // }, []);
    const getEvents = (key) => {
      if (autos.length > 0) {
        if (!loading) setLoading(true);
        apiService
          .getResources("/events/upcoming", token, autos[key].id)
          .then((value) => {
            const data = value.data;
            setLoading(false);
            if (typeof data !== "undefined") {
              setEvents(data);
            } else {
              setEvents([]);
            }
          })
          .catch((e) => {
            setLoading(false);
            setEvents([]);
            Alert.alert("Что-то пошло не так");
          });
      } else {
        setLoading(false);
      }
    };
    useEffect(() => {
      getEvents(0);
      // isMounted.current = true;
      // getEvents(index);
    }, []);
    const EventEmpty = () => {
      return (
        <View
          style={{
            alignSelf: "center",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <NoEvent />
          <Text
            style={{
              color: "#444444",
              textAlign: "left",
              paddingTop: 20,
              fontSize: 18,
              fontFamily: global.fonts.medium,
            }}
          >
            Событий нет
          </Text>
        </View>
      );
    };
    return (
      <>
        <View
          style={{
            paddingHorizontal: 20,
            backgroundColor: "white",
            width: "100%",
            paddingTop: 10,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingVertical: 10,
            }}
          >
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Hamburger />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 24,
                color: "#444444",
                textAlign: "center",
                fontFamily: global.fonts.medium,
              }}
            >
              AvtoBio
              {/* {autos[index]
                ? autos[index].makeName + " " + autos[index].modelName
                : "Нет авто"} */}
            </Text>
            <View style={{ opacity: 0 }}>
              <Hamburger />
            </View>
          </View>
          <View
            style={{
              paddingTop: 20,
              alignItems: "center",
              justifyContent: "center",
              minHeight: 60,
            }}
          >
            {loading ? (
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 0,
                  backgroundColor: "rgba(246, 246, 246,0.6)",
                  zIndex: 999,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20,
                }}
              ></View>
            ) : null}
            {autos.length ? (
              <Carousel
                layout={"stack"}
                // layout={"default"}
                data={autos}
                loop={true}
                loopClonesPerSide={5}
                initialNumToRender={autos.length + 1}
                renderItem={SliderItem}
                sliderWidth={width}
                itemWidth={width - 100}
                // slideInterpolatedStyle={animatedStyles}
                // scrollInterpolator={scrollInterpolator}
                useScrollView={false}
                scrollEnabled={!loading}
                onSnapToItem={(index) => setCurrentCar(index)}
              />
            ) : (
              <View
                style={{
                  alignItems: "center",
                  maxWidth: 280,
                  justifyContent: "center",
                  paddingBottom: 20,
                  alignSelf: "center",
                }}
              >
                <CarNumber autoInfo={"--------"} />
              </View>
            )}
          </View>
        </View>
        <ScrollView style={{ width: "100%", paddingTop: 20 }}>
          <View
            style={{
              paddingHorizontal: 20,
              alignItems: "flex-start",
              justifyContent: "flex-start",
            }}
          >
            {/* <Text
              style={{
                color: "#444444",
                textAlign: "left",
                paddingBottom: 10,
                fontSize: 18,
                fontFamily: global.fonts.medium,
              }}
            >
              Выполняется
            </Text> */}
          </View>
          {!loading ? (
            autos.length > 0 ? (
              events.length > 0 ? (
                events.map((event, key) => {
                  return (
                    <MainItem
                      method={() => getEvents(index)}
                      auto={autos[index]}
                      key={`${event.id} ${key}`}
                      event={event}
                    />
                  );
                })
              ) : (
                <EventEmpty />
              )
            ) : (
              <View>
                <AutoEmpty />
                <View style={{ marginHorizontal: 20 }}>
                  <AddAutoButton />
                </View>
              </View>
            )
          ) : (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                minHeight: 200,
              }}
            >
              <Image
                source={global.images.loader}
                style={{ height: 200, width: 200, alignSelf: "center" }}
              />
            </View>
          )}

          {/* <MainItem type="road" />
          <MainItem type="tax" />
          <MainItem type="service" />
          <MainItem type="road" />
          <MainItem type="tax" />
          <MainItem type="service" />
          <MainItem type="road" />
          <MainItem type="tax" /> */}
          <View style={{ height: 30 }} />
        </ScrollView>
      </>
    );
  };
  const AutoEmpty = () => {
    return (
      <View
        style={{
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <NoCar />
      </View>
    );
  };
  const GarageScreen = () => {
    return (
      <>
        <View
          style={{
            paddingHorizontal: 20,
            backgroundColor: "white",
            width: "100%",
            paddingVertical: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Hamburger />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 24,
                color: "#444444",
                textAlign: "center",
                fontFamily: global.fonts.medium,
              }}
            >
              Мой гараж
            </Text>
            <View style={{ opacity: 0 }}>
              <Hamburger />
            </View>
          </View>
        </View>
        <ScrollView
          style={{ width: "100%", paddingTop: 20, paddingHorizontal: 20 }}
        >
          {autos.length ? (
            autos.map((auto, key) => (
              <GarageItem auto={auto} key={key} navigation={navigation} />
            ))
          ) : (
            <AutoEmpty />
          )}
          <AddAutoButton />
        </ScrollView>
      </>
    );
  };

  const ReminderScreen = () => {
    const [loading, setLoading] = useState(true);
    const [reminder, setReminder] = useState([]);
    const getReminder = () => {
      if (!loading) setLoading(true);
      apiService
        .getResources("/reminder/list", token)
        .then((value) => {
          const data = value.data;
          if (typeof data !== "undefined") {
            if (data.length != reminderCount) {
              setReminderCount(data.length);
            }
            setReminder(data);
          } else {
            setReminderCount(0);
          }
          setLoading(false);
        })
        .catch((e) => {
          setReminderCount(0);
          setLoading(false);
        });
    };
    useEffect(() => {
      getReminder();
    }, []);
    const stopReminder = (id) => {
      if (!loading) setLoading(true);
      apiService.deleteMethod("/reminder/stop/" + id, token).then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          getReminder();
        }
      });
    };
    const clearReminder = (id) => {
      if (!loading) setLoading(true);
      apiService.deleteMethod("/reminder/item/" + id, token).then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          getReminder();
        }
      });
    };

    return (
      <>
        <View
          style={{
            paddingHorizontal: 20,
            backgroundColor: "white",
            width: "100%",
            paddingVertical: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Hamburger />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 24,
                color: "#444444",
                textAlign: "center",
                fontFamily: global.fonts.medium,
              }}
            >
              Напоминания
            </Text>
            <View style={{ opacity: 0 }}>
              <Hamburger />
            </View>
          </View>
        </View>
        <ScrollView style={{ width: "100%", paddingTop: 10 }}>
          {!loading ? (
            reminder.length ? (
              reminder.map((rem, key) => {
                return (
                  <ReminderItem
                    stopMethod={stopReminder}
                    clearMethod={clearReminder}
                    key={key + `${rem.id}`}
                    rem={rem}
                  />
                );
              })
            ) : (
              <View
                style={{
                  alignSelf: "center",
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1,
                  minHeight: global.strings.height / 1.5,
                }}
              >
                <NoReminder />
                <Text
                  style={{
                    color: "#444444",
                    textAlign: "left",
                    paddingTop: 20,
                    fontSize: 18,
                    fontFamily: global.fonts.medium,
                  }}
                >
                  Напоминаний нет
                </Text>
              </View>
            )
          ) : (
            <Image
              source={global.images.loader}
              style={{ height: 200, width: 200, alignSelf: "center" }}
            />
          )}
          <View height={50} />
        </ScrollView>
      </>
    );
  };
  return (
    <>
      {Platform.OS == "ios" && <CustomStatusBar type={`blue`} />}
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "flex-end",
          backgroundColor: "#F6F6F6",
        }}
      >
        <View
          style={{
            backgroundColor: "white",
          }}
        ></View>
        {currentScreen === 0 ? <GarageScreen /> : null}
        {currentScreen === 1 ? <MainScreen /> : null}
        {currentScreen === 2 ? <ReminderScreen /> : null}
        <Tabbar navigation={navigation} chooseScreen={changeCurrentScreen} />
      </View>
    </>
  );
}
export default HomeScreen;
const styles = StyleSheet.create({
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 10,
    marginBottom: 10,
    width: "100%",
  },
});
