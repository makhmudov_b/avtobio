import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  StatusBar,
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import Constants from "expo-constants";
import MyLocation from "../../assets/extreme-location.svg";
import Target from "../../assets/target-red.svg";
import Service from "../../assets/service.svg";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-red.svg";
import Plus from "../../assets/plus-red.svg";
import Minus from "../../assets/minus-red.svg";
import Search from "../../assets/search-red.svg";
import Evakuator from "../../assets/evakuator.svg";
import Overlay from "../../assets/overlay.svg";
import Location from "../../assets/location-white.svg";
import CustomStatusBar from "../components/CustomStatusBar";

export default class ExtremeMapScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      region: {
        latitude: 41.26465,
        longitude: 69.21627,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05,
      },
      openModal: false,
      type: props.route.params ? props.route.params.type : 0,
    };
  }
  onRegionChange(region) {
    // this.map.animateToRegion(region,1000);
    // this.setState({
    //     region:{
    //         latitude: region.latitude,
    //         longitude: region.longitude,
    //         latitudeDelta: region.latitudeDelta,
    //         longitudeDelta: region.longitudeDelta
    //     }
    //     });
    return 0;
  }
  setType(type) {
    /// 1 - Evakutor 2 - Master
    this.setState({ type });
  }
  changeZoom(type) {
    if (type === "zoomIn") {
      // this.map.animateCamera({
      //     zoom: this.state.zoom + 1,
      // });
    } else {
      // this.setState({
      //     region:{
      //         latitude: region.latitude,
      //         longitude: region.longitude,
      //         latitudeDelta: region.latitudeDelta / 5,
      //         longitudeDelta: region.longitudeDelta / 5
      //     }
      //  });
    }
    return 0;
  }
  render() {
    const { navigation } = this.props;
    return (
      <>
        <CustomStatusBar type={"white"} />
        <View style={styles.container}>
          <View
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              zIndex: 10,
            }}
          >
            <View
              style={{
                paddingHorizontal: 20,
                paddingTop: 20,
                paddingBottom: 20,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Arrow />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 24,
                    fontFamily: global.fonts.medium,
                    color: "#444444",
                  }}
                >
                  Вызвать помощь
                </Text>
                <View style={{ opacity: 0 }}>
                  <Search />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.whiteBlock}>
            {this.state.type === 0 ? (
              <View
                style={[
                  { backgroundColor: "white", borderRadius: 20, padding: 20 },
                  styles.shadow,
                ]}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontFamily: global.fonts.medium,
                    fontSize: 24,
                    color: "#444444",
                    marginBottom: 15,
                  }}
                >
                  Выберите тип помощи
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("Evacuator", { type: 1 })
                    }
                    style={[styles.choose, { marginRight: 10 }]}
                  >
                    <View style={styles.chooseIcon}>
                      <Evakuator />
                    </View>
                    <Text style={styles.chooseText}>Эвакуатор</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("ExtremeMaster", { type: 2 })
                    }
                    style={[styles.choose, { marginLeft: 10 }]}
                  >
                    <View style={styles.chooseIcon}>
                      <Service width={40} height={40} />
                    </View>
                    <Text style={styles.chooseText}>Мастер</Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <TouchableOpacity
                  style={[
                    styles.button,
                    { backgroundColor: global.colors.danger },
                  ]}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Location
                      width={17}
                      height={24}
                      style={{ marginRight: 5 }}
                    />
                    <Text
                      style={{ color: "white", marginLeft: 5, fontSize: 18 }}
                    >
                      Подтвердить
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
          <MapView
            initialRegion={this.state.region}
            ref={(map) => {
              this.map = map;
            }}
            onRegionChangeComplete={(region) => this.onRegionChange(region)}
            provider={MapView.PROVIDER_GOOGLE}
            style={styles.mapStyle}
          ></MapView>
          <View pointerEvents={"none"} style={styles.overlay}>
            <Overlay
              width={"100%"}
              height={"100%"}
              style={{ position: "absolute", transform: [{ scale: 1.3 }] }}
            />
            <MyLocation />
          </View>
          {/* <View style={{ position: "absolute", right: 20 }}>
            <TouchableOpacity
              style={[styles.mapButton, { borderRadius: 50, marginTop: 20 }]}
            >
              <Target width={24} />
            </TouchableOpacity>
          </View> */}
        </View>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  choose: {
    backgroundColor: "white",
    width: 140,
    height: 140,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
    borderRadius: 20,
  },
  chooseIcon: {
    width: 60,
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: global.colors.danger,
    borderRadius: 10,
  },
  chooseText: {
    fontSize: 14,
    color: "#444444",
    fontFamily: global.fonts.medium,
    marginTop: 10,
  },
  authorizationBlock: {
    padding: 20,
  },
  input: {
    fontSize: 18,
    color: "#444444",
    backgroundColor: "white",
    borderRadius: 50,
    width: 225,
    height: 40,
    paddingLeft: 10,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  shadow: {
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  whiteBlock: {
    zIndex: 20,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 45,
    width: "100%",
    paddingHorizontal: 20,
  },
  mapStyle: {
    width: "100%",
    height: "100%",
    position: "relative",
    // position:'absolute',
    // top:0,
    // left:0,
    // right:0,
    // bottom:0
  },
  overlay: {
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  //   circle: {
  //     backgroundColor: 'black',
  //     borderRadius: 100,
  //     alignSelf: 'stretch',
  //     width:400,
  //     height:300,
  //   },
  none: {
    display: "none",
    overflow: "hidden",
    width: 0,
    height: 0,
  },
  show: {
    width: 90,
    height: 90,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.warn,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    borderRadius: 50,
    marginTop: 20,
    width: "85%",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  mapButton: {
    backgroundColor: "white",
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  borderBottom: {
    borderBottomColor: "#E9E9E9",
    borderBottomWidth: 1,
  },
});
