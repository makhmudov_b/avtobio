import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Alert,
  Image,
} from "react-native";
import global from "../../resources/global";
import ArrowBack from "../../assets/arrow-blue.svg";
import Carousel from "react-native-snap-carousel";
import Context from "../components/Context";
import CarNumber from "../components/CarNumber";
import { LineChart, XAxis, YAxis, Grid } from "react-native-svg-charts";
import * as shape from "d3-shape";
import CustomStatusBar from "../components/CustomStatusBar";
import moment from "moment";
import "moment/locale/ru";
moment.locale("ru");
function StatisticScreen({ navigation }) {
  const [data, setData] = useState([]);
  const [current, setCurrent] = useState(0);
  const [index, setIndex] = useState(0);
  const [loading, setLoading] = useState(false);
  const { apiService, token, phone, autos } = useContext(Context);
  const [gasoline] = useState(["Неделя", "Месяц", "Год"]);
  const [stats, setStats] = useState([]);
  const [type, setType] = useState(0);
  const [dateType, setDateType] = useState(0);
  const granuality = () => {
    if (current == 0) return "week";
    if (current == 1) return "month";
    if (current == 2) return "year";
  };
  const getStats = async (key) => {
    const body = {
      granularity: granuality(),
      type: dateType == 0 ? "average" : "all",
    };
    return apiService.getResources(
      "/statistics/stats?" + new URLSearchParams(body),
      token,
      autos[key].id
    );
  };
  const getInfo = async (key) => {
    const body = {
      granularity: granuality(),
    };
    const handleType = () => {
      if (type == 0) return "mileage";
      if (type == 1) return "expense";
    };
    return apiService.getResources(
      "/statistics/" + handleType() + "?" + new URLSearchParams(body),
      token,
      autos[key].id
    );
  };
  const choosePeriod = (key) => {
    if (key !== current) {
      setCurrent(key);
      setLoading(true);
    }
    // setTimeout(() => getRequest(index), 1000);
  };
  const getRequest = (key) => {
    if (!loading) setLoading(true);
    Promise.all([
      getInfo(key).then((value) => {
        if (value.statusCode === 200) {
          const mileage = value.data;
          if (mileage) {
            if (type == 0) setData([...mileage].map((mil) => mil.odometer));
            if (type == 1) setData([...mileage].map((mil) => mil.expense));
          } else setData([]);
        } else {
          setData([]);
        }
      }),
      getStats(key).then((value) => {
        if (value.statusCode === 200) {
          if (value.data) setStats(value.data);
          else setStats([]);
        } else {
          setStats([]);
        }
      }),
    ])
      .then((value) => {
        setLoading(false);
      })
      .catch((err) => {
        setData([]);
        setStats([]);
        setLoading(false);
        Alert.alert("Что-то пошло не так");
      });
  };
  const setCurrentCar = (key) => {
    getRequest(key);
    setIndex(key);
  };
  const chooseDataType = (value) => {
    setDateType(value);
    setLoading(true);
    // setTimeout(() => getRequest(index), 1000);
  };
  const chooseType = (value) => {
    setType(value);
    setLoading(true);
    // setTimeout(() => getRequest(index), 1000);
  };
  useEffect(() => {
    getRequest(index);
  }, [type, dateType, current]);
  const width = global.strings.width;
  const SliderItem = ({ item, index }) => {
    return (
      <View
        style={{
          alignItems: "center",
          maxWidth: 280,
          justifyContent: "center",
          paddingBottom: 20,
          alignSelf: "center",
        }}
      >
        <CarNumber autoInfo={item.plateNumber} />
      </View>
    );
  };
  const getDateType = () => {
    if (dateType == 0) return "Средний";
    else return "Общий";
  };
  const getGraphType = () => {
    if (type == 0) return "График пробега";
    else return "График расхода";
  };
  const getCurrentType = () => {
    if (current == 0) return "За неделю";
    if (current == 1) return "За месяц";
    if (current == 2) return "За день";
  };
  const axesSvg = {
    fontSize: 12,
    fill: "rgba(0,0,0,.4)",
  };
  const verticalContentInset = { top: 30, bottom: 10 };
  const xAxisHeight = 20;
  const firstdayOfMonth = moment().startOf("month").format("D MMM ");
  const lastdayOfMonth = moment().endOf("month").format("D MMM");
  const firstWeekDay = "Понедельник";
  const lastWeekDay = "Воскресение";
  const lastDayOfYear = "31 Дек.";
  const firstDayOfYear = "1 Янв.";
  const ChartComponent = () => (
    <>
      <View style={{ flex: 1 }}>
        <LineChart
          style={{ flex: 1 }}
          data={data}
          contentInset={verticalContentInset}
          curve={shape.curveNatural}
          showGrid={true}
          svg={{
            stroke: global.colors.mainColor,
            strokeWidth: 2,
          }}
        />
        <Grid />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: "100%",
            justifyContent: "space-between",
            borderTopColor: "#efefef",
            borderTopWidth: 1,
            paddingBottom: 20,
            paddingTop: 20,
          }}
        >
          <Text
            style={{
              fontFamily: global.fonts.regular,
              fontSize: 12,
              color: "#999",
            }}
          >
            {current == 0
              ? firstWeekDay
              : current == 1
              ? firstdayOfMonth
              : firstDayOfYear}
          </Text>
          <Text
            style={{
              fontFamily: global.fonts.regular,
              fontSize: 12,
              color: "#999",
            }}
          >
            {current == 0
              ? lastWeekDay
              : current == 1
              ? lastdayOfMonth
              : lastDayOfYear}
          </Text>
        </View>
        {/* <XAxis
          style={{
            paddingTop: 5,
            borderTopWidth: 1,
            borderTopColor: "rgba(0,0,0,.2)",
            height: xAxisHeight,
          }}
          data={data}
          contentInset={{ left: 10, right: 10 }}
          svg={axesSvg}
        /> */}
      </View>
      <View style={{ height: 200, paddingLeft: 10 }}>
        <YAxis
          data={data}
          numberOfTicks={4}
          style={{
            height: "100%",
            position: "relative",
            top: -20,
          }}
          contentInset={verticalContentInset}
          svg={axesSvg}
        />
      </View>
    </>
  );
  return (
    <>
      <CustomStatusBar type={"white"} />
      <View style={styles.container}>
        <View
          style={{
            paddingHorizontal: 20,
            backgroundColor: "white",
            width: "100%",
            paddingTop: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <ArrowBack />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 24,
                color: "#444444",
                textAlign: "center",
                fontFamily: global.fonts.medium,
              }}
            >
              Статистика
            </Text>
            <View style={{ opacity: 0 }}>
              <ArrowBack />
            </View>
          </View>
          <View style={{ paddingTop: 20, alignItems: "center" }}>
            {loading ? (
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 0,
                  backgroundColor: "rgba(246, 246, 246,0.6)",
                  zIndex: 999,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 20,
                }}
              ></View>
            ) : null}
            {autos.length ? (
              <Carousel
                layout={"stack"}
                // layout={"default"}
                data={autos}
                loop={true}
                loopClonesPerSide={5}
                initialNumToRender={autos.length + 1}
                renderItem={SliderItem}
                sliderWidth={width}
                itemWidth={width - 100}
                // slideInterpolatedStyle={animatedStyles}
                // scrollInterpolator={scrollInterpolator}
                useScrollView={false}
                scrollEnabled={!loading}
                onSnapToItem={(index) => setCurrentCar(index)}
              />
            ) : (
              <View
                style={{
                  alignItems: "center",
                  maxWidth: 280,
                  justifyContent: "center",
                  paddingBottom: 20,
                  alignSelf: "center",
                }}
              >
                <CarNumber autoInfo={"--------"} />
              </View>
            )}
          </View>
        </View>
        <ScrollView style={{ flex: 1 }}>
          {!loading ? (
            <React.Fragment>
              <View style={{ marginTop: 20, flexDirection: "row" }}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: "white",
                    borderRadius: 20,
                    overflow: "hidden",
                    justifyContent: "center",
                    alignSelf: "center",
                    marginHorizontal: 20,
                    padding: 20,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => chooseDataType(0)}
                      style={[
                        styles.leftBlock,
                        styles.block,
                        !dateType ? styles.activeBlock : styles.inactiveBlock,
                      ]}
                    >
                      <Text
                        style={
                          !dateType ? styles.activeText : styles.inactiveText
                        }
                      >
                        Средний
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => chooseDataType(1)}
                      style={[
                        styles.rightBlock,
                        styles.block,
                        dateType ? styles.activeBlock : styles.inactiveBlock,
                      ]}
                    >
                      <Text
                        style={
                          dateType ? styles.activeText : styles.inactiveText
                        }
                      >
                        Общий
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={[
                      {
                        position: "relative",
                        flexDirection: "row",
                        marginTop: 10,
                      },
                      styles.gazShadow,
                    ]}
                  >
                    {gasoline.length > 0 &&
                      gasoline.map((item, key) => {
                        return (
                          <TouchableOpacity
                            style={[
                              key === 0 ? styles.leftBlock : {},
                              key === gasoline.length - 1
                                ? styles.rightBlock
                                : {},
                              styles.block,
                              current === key
                                ? styles.activeBlock
                                : styles.inactiveGaz,
                            ]}
                            key={key}
                            onPress={() => choosePeriod(key)}
                          >
                            <Text
                              style={[
                                current === key
                                  ? styles.activeText
                                  : styles.inactiveText,
                                { fontFamily: global.fonts.regular },
                              ]}
                            >
                              {item}
                            </Text>
                          </TouchableOpacity>
                        );
                      })}
                  </View>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  backgroundColor: "#F6F6F6",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    marginTop: 20,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <View
                    style={[
                      styles.infoBlock,
                      {
                        backgroundColor: global.colors.preDanger,
                        marginRight: 15,
                      },
                    ]}
                  >
                    <Text style={styles.topBottomText}>{getDateType()}</Text>
                    <Text style={styles.middleText}>
                      {stats.length ?? stats.odometer} км
                    </Text>
                    <Text style={styles.topBottomText}>{getCurrentType()}</Text>
                  </View>
                  <View
                    style={[
                      styles.infoBlock,
                      {
                        backgroundColor: global.colors.violet,
                        marginRight: 15,
                      },
                    ]}
                  >
                    <Text style={styles.topBottomText}>{getDateType()}</Text>
                    <Text style={styles.middleText}>
                      {stats.length ?? stats.petrol} L
                    </Text>
                    <Text style={styles.topBottomText}>{getCurrentType()}</Text>
                  </View>
                  <View
                    style={[
                      styles.infoBlock,
                      {
                        backgroundColor: "#34BE5B",
                        marginRight: 15,
                      },
                    ]}
                  >
                    <Text style={styles.topBottomText}>{getDateType()}</Text>
                    <Text style={styles.middleText}>
                      {stats.length ?? stats.expense} m³
                    </Text>
                    <Text style={styles.topBottomText}>{getCurrentType()}</Text>
                  </View>
                  <View
                    style={[
                      styles.infoBlock,
                      { backgroundColor: global.colors.danger },
                    ]}
                  >
                    <Text style={styles.topBottomText}>{getDateType()}</Text>
                    <Text style={styles.middleText}>
                      {stats.length ?? stats.expense}
                    </Text>
                    <Text style={styles.topBottomText}>Расход</Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  marginTop: 20,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <View style={{ minHeight: 280, flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: "white",
                      borderRadius: 20,
                      overflow: "hidden",
                      justifyContent: "center",
                      alignSelf: "center",
                      marginHorizontal: 20,
                      padding: 20,
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        fontFamily: global.fonts.medium,
                        color: "#444444",
                        fontSize: 18,
                      }}
                    >
                      {getGraphType()}
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        width: "100%",
                        alignItems: "center",
                        height: 200,
                      }}
                    >
                      <ChartComponent />
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        paddingBottom: 10,
                        justifyContent: "center",
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => chooseType(0)}
                        style={[
                          styles.leftBlock,
                          styles.block,
                          !type ? styles.activeBlock : styles.inactiveBlock,
                        ]}
                      >
                        <Text
                          style={
                            !type ? styles.activeText : styles.inactiveText
                          }
                        >
                          Пробег
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => chooseType(1)}
                        style={[
                          styles.rightBlock,
                          styles.block,
                          type ? styles.activeBlock : styles.inactiveBlock,
                        ]}
                      >
                        <Text
                          style={type ? styles.activeText : styles.inactiveText}
                        >
                          Расход
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </React.Fragment>
          ) : (
            <Image
              source={global.images.loader}
              style={{ height: 200, width: 200, alignSelf: "center" }}
            />
          )}
        </ScrollView>
      </View>
    </>
  );
}

export default StatisticScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
  },
  infoBlock: {
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
    alignItems: "center",
    height: global.strings.width / 5.2,
    width: global.strings.width / 5.2,
  },
  gazShadow: {
    backgroundColor: "white",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  topBottomText: {
    fontSize: 9,
    color: "#E9E9E9",
  },
  middleText: {
    color: "white",
    fontFamily: global.fonts.medium,
    fontSize: 10,
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  inactiveGaz: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "white",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
});
