import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  Image,
} from "react-native";
import global from "../../resources/global";
import Work from "../../assets/work.svg";
import House from "../../assets/house.svg";
import Service from "../../assets/service.svg";
import Fuel from "../../assets/fuel.svg";
import LocationWhite from "../../assets/location-white.svg";
import PlusWhite from "../../assets/plus.svg";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";

function MyAddressScreen({ navigation }) {
  const { token, apiService } = useContext(Context);
  const [address, setAddress] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getList();
  }, []);
  const getList = () => {
    apiService
      .getResources("/address/list", token)
      .then((value) => {
        const data = value.data;
        if (typeof data !== "undefined") {
          setAddress(data);
        }
        setLoading(false);
      })
      .catch((e) => Alert.alert("Что-то пошло не так"));
  };
  const showIcon = (value) => {
    if (value == 0) return <Work />;
    if (value == 1) return <House />;
    if (value == 2) return <Service width={30} height={30} />;
    if (value == 3) return <Fuel />;
    if (value == 4) return <LocationWhite />;
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Мои адреса"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ alignItems: "center", paddingHorizontal: 20 }}>
            <View style={{ height: 20 }} />
            <View style={[styles.authorization]}>
              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                {!loading ? (
                  <React.Fragment>
                    {address.map((add, key) => {
                      return (
                        <TouchableOpacity
                          onPress={() =>
                            navigation.navigate("AddAddress", {
                              data: add,
                              update: getList,
                            })
                          }
                          key={key}
                          style={styles.block}
                        >
                          <View
                            style={[styles.blockInner, { marginBottom: 10 }]}
                          >
                            {showIcon(add.icon)}
                          </View>
                          <Text
                            style={{
                              fontSize: 14,
                              fontFamily: global.fonts.medium,
                              textAlign: "center",
                              color: "#444444",
                            }}
                          >
                            {add.name}
                          </Text>
                          <Text
                            style={{
                              fontSize: 9,
                              textAlign: "center",
                              color: "#999999",
                              fontFamily: global.fonts.regular,
                              maxWidth: 120,
                            }}
                          >
                            {add.address}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate("AddAddress", { update: getList })
                      }
                      style={styles.block}
                    >
                      <View style={[styles.blockInner, { marginBottom: 10 }]}>
                        <PlusWhite />
                      </View>
                    </TouchableOpacity>
                  </React.Fragment>
                ) : (
                  <View
                    style={{
                      width: "100%",
                      height: global.strings.width * 0.37,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Image
                      source={global.images.loader}
                      style={{ height: 200, width: 200, alignSelf: "center" }}
                    />
                  </View>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default MyAddressScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-around",
  },
  authorization: {
    backgroundColor: "white",
    width: `100%`,
    padding: 20,
    marginBottom: 20,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
  },
  block: {
    width: global.strings.width * 0.37,
    height: global.strings.width * 0.37,
    marginBottom: 20,
    justifyContent: "center",
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.9,
    shadowRadius: 3.84,
    elevation: 3,
  },
  blockInner: {
    padding: 10,
    alignItems: "center",
    justifyContent: "center",
    width: 60,
    height: 60,
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
  },
});
