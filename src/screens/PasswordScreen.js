import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import CustomIcon from "../../assets/background.svg";
import Arrow from "../../assets/arrow.svg";

function PasswordScreen({ navigation }) {
  const [password, onChangePassword] = useState("");
  const [confirmation, onChangeConfirmation] = useState("");
  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={global.colors.mainColor}
          barStyle="light-content"
        />
        <View style={{ height: 200, position: "relative" }}>
          <View style={{ zIndex: 20, paddingTop: 50, paddingLeft: 20 }}>
            <TouchableOpacity onPress={() => alert("back")}>
              <Arrow />
            </TouchableOpacity>
          </View>
          <View style={[styles.absoluteBackground]}>
            <CustomIcon width={350} height={610} />
          </View>
        </View>
        <View
          style={{ flex: 1, backgroundColor: "#F6F6F6", alignItems: "center" }}
        >
          <View style={styles.authorization}>
            <View style={styles.authorizationTop}>
              <Text
                style={{
                  fontFamily: global.fonts.medium,
                  fontSize: 18,
                  color: "#444444",
                }}
              >
                Создайте пароль
              </Text>
            </View>
            <View style={styles.authorizationBlock}>
              <TextInput
                returnKeyType={"done"}
                style={styles.authorizationInput}
                secureTextEntry={true}
                placeholder="Придумайте пароль"
                placeholderTextColor="#999999"
                value={password}
                onChangeText={(text) => onChangePassword(text)}
              />
              <TextInput
                returnKeyType={"done"}
                style={styles.authorizationInput}
                secureTextEntry={true}
                placeholder="Подтвердите пароль"
                placeholderTextColor="#999999"
                value={confirmation}
                onChangeText={(text) => onChangeConfirmation(text)}
              />
              <TouchableOpacity
                style={[
                  styles.button,
                  { backgroundColor: global.colors.mainColor },
                ]}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Создать
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </>
  );
}

export default PasswordScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.mainColor,
    position: "relative",
  },
  absoluteBackground: {
    paddingTop: 10,
    alignItems: "center",
    position: "absolute",
    bottom: `-10%`,
    width: `100%`,
    height: `100%`,
    left: 0,
    right: 0,
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    transform: [{ translateY: -76 }],
  },
  authorizationTop: {
    alignItems: "center",
    justifyContent: "center",
    height: 76,
    borderBottomWidth: 1,
    borderBottomColor: "#E9E9E9",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 20,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 10,
    borderColor: global.colors.mainColor,
    borderWidth: 2,
  },
  input: {
    textAlign: "center",
    fontSize: 36,
    fontFamily: global.fonts.medium,
    letterSpacing: 20,
  },
  bottomLine: {
    height: 2,
    width: 160,
    marginTop: 8,
    marginBottom: 20,
    backgroundColor: "#E9E9E9",
  },
});
