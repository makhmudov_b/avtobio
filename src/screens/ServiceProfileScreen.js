import React, { useState, useEffect, useContext } from "react";
import { Text, View, StyleSheet, ScrollView, Alert } from "react-native";
import global from "../../resources/global";
import Service from "../../assets/service.svg";
import MasterItem from "../components/MasterItem";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function ServiceProfileScreen({ navigation, route }) {
  const { token, apiService, currentCar } = useContext(Context);
  const { params } = route;
  const { service } = params;
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    apiService
      .getResources("/locator/item/" + service.id, token, currentCar.id)
      .then((value) => {
        if (value.statusCode !== 200) {
          Alert.alert("Что-то пошло не так!");
          return;
        }
        const { data } = value;
        if (typeof data != "undefined") {
          setData(data);
        }

        setLoading(false);
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
      });
  }, []);
  const now = new Date();
  const weekDay = now.getDay();
  if (loading) return <RequestLoadingScreen />;
  const isOpen = (item = null) => {
    const getCurrent = item.timeslots[weekDay];
    if (!getCurrent) return "Закрыто";
    if (
      now.getHours() > parseInt(getCurrent.startTime.slice(0, 2)) &&
      now.getHours() < parseInt(getCurrent.endTime.slice(0, 2))
    ) {
      return "Открыто";
    } else {
      return "Закрыто";
    }
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          navigation={navigation}
          title={service.name}
          button={true}
          type="white"
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
            <View style={{ height: 20 }} />
            <View style={[styles.authorization, styles.shadow]}>
              <View
                style={{
                  backgroundColor: "white",
                  padding: 20,
                  borderRadius: 20,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginBottom: 20,
                  }}
                >
                  <View style={styles.icon}>
                    <Service />
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <View>
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: global.fonts.medium,
                          color: "#444444",
                        }}
                      >
                        {service.name}
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: "#999999",
                        }}
                      >
                        {service.address ? service.address.address : ""}
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: "#999999",
                        }}
                      >
                        {typeof service.timeslots[weekDay] !== "undefined"
                          ? service.timeslots[weekDay].startTime
                          : "Не задан"}{" "}
                        -{" "}
                        {typeof service.timeslots[weekDay] !== "undefined"
                          ? service.timeslots[weekDay].endTime
                          : "Не задан"}
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: global.fonts.medium,
                          color: "#444444",
                          textAlign: "right",
                        }}
                      >
                        {Math.ceil(service.distance / 100) / 10} KM
                      </Text>
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: global.colors.success,
                          textAlign: "right",
                        }}
                      >
                        {isOpen(service)}
                      </Text>
                      {/* <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 9,
                          color: "#999999",
                          textAlign: "right",
                        }}
                      >
                        Без выходных
                      </Text> */}
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontFamily: global.fonts.medium,
                        fontSize: 12,
                        color: "#444444",
                      }}
                    >
                      Услуги:
                    </Text>
                  </View>
                  <View style={{ flex: 1, marginLeft: 15 }}>
                    {data.services &&
                      data.services.map((serv, key) => {
                        return (
                          <View
                            key={key}
                            style={{
                              flex: 1,
                              marginTop: 4,
                              flexDirection: "row",
                              justifyContent: "space-between",
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: global.fonts.regular,
                                fontSize: 10,
                                color: "#444444",
                              }}
                            >
                              {serv.name}
                            </Text>
                            <View
                              style={{
                                flex: 1,
                                borderBottomWidth: 1,
                                borderStyle: "dotted",
                                borderBottomColor: "#e9e9e9",
                              }}
                            ></View>
                            <Text
                              style={{
                                fontFamily: global.fonts.regular,
                                fontSize: 10,
                                color: "#999999",
                              }}
                            >
                              {serv.price} UZS
                            </Text>
                          </View>
                        );
                      })}
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 12,
                    marginBottom: 10,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: global.fonts.medium,
                      fontSize: 12,
                      color: "#444444",
                    }}
                  >
                    Количество:
                  </Text>
                  <Text
                    style={{
                      fontFamily: global.fonts.medium,
                      fontSize: 12,
                      color: "#444444",
                    }}
                  >
                    {data.technicianCount} мастеров
                  </Text>
                </View>
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              {data.technicians &&
                data.technicians.map((master, key) => (
                  <MasterItem master={master} key={key} />
                ))}
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default ServiceProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
    width: "100%",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.warn,
    borderRadius: 50,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 3,
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
