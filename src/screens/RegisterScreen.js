import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal,
  TouchableWithoutFeedback,
  ScrollView,
  Alert,
} from "react-native";
import global from "../../resources/global";
import CalendarPhoto from "../../assets/calendar.svg";
import ArrowRightBlue from "../../assets/arrow-right.svg";
import HeaderBack from "../components/HeaderBack";
// import { Calendar } from "react-native-calendars";
import DateTimePicker from "@react-native-community/datetimepicker";
import Context from "../components/Context";

function RegisterScreen({ navigation }) {
  const { apiService, token, phone, getUserData } = useContext(Context);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [modal, setModal] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);

  const [gender, setGender] = useState(null);
  const [name, setName] = useState(null);
  const [email, setMail] = useState(null);
  const [address, setAddress] = useState(null);
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDateChosen(true);
      setDate(selectedDate);
    }
  };
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const chosenDate = `${year}-${month < 10 ? "0" + month : month}-${
    day < 10 ? "0" + day : day
  }`;
  const dateFormatted = `${day < 10 ? "0" + day : day}-${
    month < 10 ? "0" + month : month
  }-${year}`;

  const getGender = gender === "MALE" ? "Мужчина" : "Женщина";

  const RegisterForm = () => {
    if (name && email && gender && chosenDate) {
      apiService
        .register(token, {
          firstName: name,
          birthDate: chosenDate,
          email: email,
          sex: gender,
        })
        .then((value) => {
          if (value.statusCode === 200) {
            getUserData();
            navigation.replace("Home");
          } else {
            Alert.alert(value.message);
          }
        })
        .catch((e) => {
          // Alert.alert(e);
          // navigation.replace(e);
        });
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  return (
    <>
      <HeaderBack
        type="white"
        button={false}
        title="Регистрация"
        navigation={navigation}
      />
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          display="default"
          onChange={onChange}
        />
      )}
      <Modal animationType="fade" transparent={true} visible={modal}>
        <TouchableWithoutFeedback
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
          onPress={() => setModal(false)}
        >
          <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
        </TouchableWithoutFeedback>
        <View
          style={{
            flexDirection: "row",
            position: "absolute",
            marginTop: 60,
            alignSelf: "center",
            justifyContent: "center",
            width: "100%",
          }}
        >
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <TouchableOpacity
                onPress={() => {
                  setGender("MALE");
                  setModal(false);
                }}
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color: "#444",
                      lineHeight: 50,
                      textAlign: "center",
                    },
                  ]}
                >
                  Мужчина
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setGender("FEMALE");
                  setModal(false);
                }}
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color: "#444",
                      lineHeight: 50,
                      textAlign: "center",
                    },
                  ]}
                >
                  Женщина
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <ScrollView style={styles.container}>
        <View
          style={{ flex: 1, backgroundColor: "#F6F6F6", alignItems: "center" }}
        >
          <View style={{ height: 20 }} />
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <TextInput
                returnKeyType={"done"}
                style={styles.authorizationInput}
                placeholder="Полное имя"
                placeholderTextColor="#999999"
                value={name}
                onChangeText={(text) => setName(text)}
              />
              <TextInput
                returnKeyType={"done"}
                style={styles.authorizationInput}
                placeholder="Номер телефона"
                placeholderTextColor="#444"
                value={"+" + phone}
                editable={false}
              />
              <TextInput
                returnKeyType={"done"}
                keyboardType={`email-address`}
                style={styles.authorizationInput}
                placeholder="Почтовый адрес"
                placeholderTextColor="#999999"
                value={email}
                onChangeText={(text) => setMail(text)}
              />
              <View style={{ position: "relative" }}>
                <TouchableOpacity onPress={() => setShow(true)}>
                  <Text
                    style={[
                      styles.authorizationInput,
                      {
                        color: !dateChosen ? "#999" : "#444",
                        lineHeight: 50,
                      },
                    ]}
                  >
                    {dateChosen ? dateFormatted : "Дата рождения"}
                  </Text>
                  <CalendarPhoto style={styles.icon} />
                </TouchableOpacity>
              </View>
              <View style={{ position: "relative" }}>
                <TouchableOpacity onPress={() => setModal(true)}>
                  <Text
                    style={[
                      styles.authorizationInput,
                      {
                        color: !gender ? "#999" : "#444",
                        lineHeight: 50,
                      },
                    ]}
                  >
                    {!gender ? "Пол" : getGender}
                  </Text>
                  <ArrowRightBlue style={styles.icon} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <TouchableOpacity
            style={[
              styles.button,
              { backgroundColor: global.colors.mainColor },
            ]}
            onPress={() => RegisterForm()}
          >
            <Text
              style={{
                color: "white",
                fontSize: 18,
                fontFamily: global.colors.regular,
              }}
            >
              Далее
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
}

export default RegisterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    right: 15,
    top: 13,
  },
});
