import React, { Component } from "react";
import { Text, View, ActivityIndicator } from "react-native";
import Logo from "../../assets/logo.svg";
import global from "../../resources/global";
export default class LoadingScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          backgroundColor: global.colors.mainColor,
          justifyContent: "center",
        }}
      >
        <View>
          <Logo />
        </View>
      </View>
    );
  }
}
