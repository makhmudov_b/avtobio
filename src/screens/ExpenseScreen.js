import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Metr from "../../assets/kilometr.svg";
import Calendar from "../../assets/calendar-time.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import SuccessModal from "../components/SuccessModal";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import {formatDate, parseDate} from "../utils/date";
function ExpenseScreen({ navigation, route }) {
  const { apiService, token } = useContext(Context);
  const { data, auto } = route.params
    ? route.params
    : { data: null, auto: null };
  const [number, setNumber] = useState("");
  const [vehicleId, setVehicle] = useState(0);
  const [name, setName] = useState("");
  const [amount, setAmount] = useState("");
  const [note, setNote] = useState("");
  const [odometer, setOdometr] = useState("");
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (data) {
      setLoading(true);
      apiService
        .getResources("/events/expense/" + data.id, token)
        .then((value) => {
          ChooseAuto(auto.id, auto.plateNumber);
          setAmount(value.data.amount.toString());
          setOdometr(value.data.odometer.toString());
          setName(value.data.name);
          setNote(value.data.note);
          setDate(new Date(value.data.transactionDate));
          setDateChosen(true);
          setLoading(false);
        })
        .catch((e) => navigation.goBack());
    }
  }, []);
  const onChangeDate = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setShowTime(true);
    }
  };
  const onChangeTime = (event, selectedTime) => {
    setShowTime(false);
    if (selectedTime) {
      setDateChosen(true);
      setDate(selectedTime);
    }
  };
  const chosenDateTime = formatDate(date);
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const saveEvent = () => {
    if (dateChosen && vehicleId && amount && name.length && odometer.length) {
      const body = {
        amount: parseInt(amount),
        name: name,
        note: note,
        odometer: parseInt(odometer),
        transactionDate: parseDate(date),
        vehicleId: parseInt(vehicleId),
      };
      setLoading(true);
      if (data) {
        body.id = data.id;
        apiService
          .updateForm("/events/expense", token, body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
              setLoading(false);
            }
          })
          .catch((e) => setLoading(false));
      } else {
        apiService
          .addEvent(token, "expense", body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
            }
          })
          .catch((e) => setLoading(false));
      }
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const LeftBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("ChooseAuto", {
                method: ChooseAuto,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !vehicleId ? "#999999" : "#444444",
                },
              ]}
            >
              {!vehicleId ? "Выберите авто" : number}
            </Text>
            <ArrowRight style={styles.icon} />
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Название расхода"
            placeholderTextColor="#999999"
            defaultValue={name}
            onChangeText={(text) => {
              setName(text);
            }}
          />
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity onPress={() => setShow(true)}>
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !dateChosen ? "#999" : "#444",
                },
              ]}
            >
              {dateChosen ? chosenDateTime : "Дата и время"}
            </Text>
            <Calendar style={styles.icon} />
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Показание адометра"
            keyboardType={"numeric"}
            placeholderTextColor="#999999"
            value={odometer}
            onChangeText={(text) => setOdometr(text)}
          />
          <View style={styles.icon}>
            <Metr />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Сумма расхода"
            keyboardType={"numeric"}
            placeholderTextColor="#999999"
            value={amount}
            onChangeText={(text) => setAmount(text)}
          />
          <View style={[styles.icon, { width: "auto" }]}>
            <Text style={{ fontSize: 18, color: "#999999" }}>UZS</Text>
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={note}
            onChangeText={(text) => setNote(text)}
          />
        </View>
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          is24Hour={true}
          display="default"
          onChange={onChangeDate}
        />
      )}
      {showTime && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`time`}
          is24Hour={true}
          display="default"
          onChange={onChangeTime}
        />
      )}
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Расход"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>{LeftBlock()}</View>
            </View>
            <TouchableOpacity
              onPress={() => saveEvent()}
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor },
              ]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Сохранить
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <SuccessModal showModal={showModal} setShowModal={setShowModal} />
      </View>
    </>
  );
}

export default ExpenseScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    paddingRight: 50,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});
