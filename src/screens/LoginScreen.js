import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import CustomIcon from "../../assets/background.svg";
import Logo from "../../assets/logo.svg";
import Google from "../../assets/google.svg";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function LoginScreen({ navigation }) {
  const { apiService, setPhone } = useContext(Context);
  const [phone, onChangePhone] = useState("");
  const [loading, setLoading] = useState(false);
  const Login = () => {
    if (phone.length !== 9) {
      Alert.alert("Введите номер телефона!");
      return 0;
    }
    setLoading(true);
    apiService
      .sendCode("998" + phone)
      .then((value) => {
        if (value.statusCode === 200) {
          setLoading(false);
          setPhone("998" + phone);
          navigation.navigate("Confirm");
        } else {
          setLoading(false);
          Alert.alert(value.message);
          // Alert.alert("Введены неправильные данные. Повторите заново!");
        }
      })
      .catch((e) => {
        setLoading(false);
        Alert.alert(e.message);
      });
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={global.colors.mainColor}
          barStyle="light-content"
        />
        <View
          style={{
            flex: 3,
            alignItems: "center",
            justifyContent: "center",
            position: "relative",
          }}
        >
          <View style={[styles.absoluteBackground]}>
            <CustomIcon width={`200%`} height={`200%`} />
          </View>
          <Logo />
        </View>
        <View
          style={{
            flex: 4,
            backgroundColor: "#F6F6F6",
            alignItems: "center",
          }}
        >
          <View style={styles.authorization}>
            <View style={styles.authorizationTop}>
              <Text
                style={{
                  fontFamily: global.fonts.medium,
                  fontSize: 18,
                  color: "#444444",
                }}
              >
                Вход
              </Text>
            </View>
            <View style={styles.authorizationBlock}>
              <View style={styles.authorizationInput}>
                <Text
                  style={[styles.authorizationInputInner, { paddingRight: 5 }]}
                >
                  +998
                </Text>
                <TextInput
                  returnKeyType={"done"}
                  style={[styles.authorizationInputInner, { flex: 1 }]}
                  keyboardType={"phone-pad"}
                  placeholder="Номер телефона"
                  placeholderTextColor="#999999"
                  value={phone}
                  onChangeText={(phone) => onChangePhone(phone)}
                />
              </View>
              <TouchableOpacity
                onPress={() => Login()}
                style={[
                  styles.button,
                  { backgroundColor: global.colors.mainColor },
                ]}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Войти
                </Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={[styles.button, { backgroundColor: "white" }]}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Google style={{ marginRight: 15 }} />
                  <Text
                    style={{
                      color: global.colors.mainColor,
                      fontFamily: global.fonts.regular,
                      fontSize: 18,
                    }}
                  >
                    Вход с помощью Google
                  </Text>
                </View>
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </View>
    </>
  );
}

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.mainColor,
    position: "relative",
  },
  absoluteBackground: {
    paddingTop: 10,
    alignItems: "center",
    position: "absolute",
    bottom: `-20%`,
    width: `100%`,
    height: `100%`,
    left: 0,
    right: 0,
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    transform: [{ translateY: -76 }],
  },
  authorizationTop: {
    alignItems: "center",
    justifyContent: "center",
    height: 76,
    borderBottomWidth: 1,
    borderBottomColor: "#E9E9E9",
  },
  authorizationBlock: {
    padding: 20,
  },
  authorizationInput: {
    paddingHorizontal: 20,
    height: 50,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    flexDirection: "row",
    alignItems: "center",
  },
  authorizationInputInner: {
    fontFamily: global.fonts.regular,
    fontSize: 18,
    color: "#444444",
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 10,
    borderColor: global.colors.mainColor,
    borderWidth: 2,
  },
});
