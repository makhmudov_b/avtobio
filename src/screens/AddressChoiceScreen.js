import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  KeyboardAvoidingView,
  StatusBar,
  ScrollView,
  Alert,
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import ServiceMarker from "../../assets/service-marker.svg";
import ServiceChosenMarker from "../../assets/service-chosen.svg";
import FuelMarker from "../../assets/fuel-marker.svg";
import FuelChosenMarker from "../../assets/fuel-chosen.svg";
import Service from "../../assets/service.svg";
import Fuel from "../../assets/fuel.svg";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Search from "../../assets/search.svg";
import Heart from "../../assets/like-blue.svg";
import Close from "../../assets/close-blue.svg";
import Share from "../../assets/share-blue.svg";
import Overlay from "../../assets/overlay.svg";
import Constants from "expo-constants";
import Target from "../../assets/target.svg";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import CustomStatusBar from "../components/CustomStatusBar";
import XBlue from "../../assets/x-blue.svg";

export default class AddressChoiceScreen extends Component {
  static contextType = Context;
  constructor() {
    super();
  }
  componentDidMount() {
    this.getData();
  }
  state = {
    data: [],
    chosen: -1,
    show: false,
    service: 1,
    loading: false,
    region: {
      latitude: 41.26465,
      longitude: 69.21627,
      latitudeDelta: 0.04,
      longitudeDelta: 0.05,
    },
    search: "",
    openSearch: false,
  };
  getData() {
    if (this.props.route.params.type) this.getRequest();
  }
  getRequest(keyword) {
    this.setState({ loading: true });
    const { apiService, token, currentCar } = this.context;
    apiService
      .getResources(
        `/locator?categoryCode=${
          this.props.route.params.type == "fuel"
            ? "GAS_STATION"
            : "AUTO_SERVICE"
        }&sortType=DESC${keyword ? "&searchKey=" + keyword : ""}`,
        token,
        currentCar.id
      )
      .then((value) => {
        const data = typeof value.data != "undefined" ? value.data : [];
        this.setState({
          data: data,
          loading: false,
          chosen: -1,
          show: false,
        });
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
      });
  }
  onRegionChange(region) {
    this.setState({
      region: {
        latitude: region.latitude,
        longitude: region.longitude,
      },
    });
  }
  setCamerView(markerPosition, key) {
    this.setState({
      region: {
        latitude: markerPosition.latitude,
        longitude: markerPosition.longitude,
        latitudeDelta: this.state.region.latitudeDelta,
        longitudeDelta: this.state.region.longitudeDelta,
      },
      chosen: key,
      show: true,
      openSearch: false,
    });
  }
  handleSearch(search) {
    this.setState({ search: search });
  }
  onEditEnd() {
    if (this.state.search.length > 3) this.getRequest(this.state.search);
  }
  setSearch(boolValue) {
    this.setState({ openSearch: boolValue });
  }
  goToUserLocation() {
    const { coords } = this.context.location;
    const { latitude, longitude, altitude, heading } = coords;
    const temp_cordinate = { latitude: latitude, longitude: longitude };
    this.map.animateCamera({
      center: temp_cordinate,
      pitch: 2,
      heading: heading,
      altitude: altitude,
      zoom: 14,
    });
  }
  render() {
    const { navigation, route } = this.props;
    const { params } = route;
    const { type, title, method } = params;
    const now = new Date();
    const weekDay = now.getDay();
    const CustomChosen = () => {
      if (type === "fuel") return <FuelChosenMarker />;
      if (type === "service") return <ServiceChosenMarker />;
    };
    const CustomMarker = () => {
      if (type === "fuel") return <FuelMarker />;
      if (type === "service") return <ServiceMarker />;
    };
    const TypeIcon = () => {
      if (type === "fuel") return <Fuel />;
      if (type === "service") return <Service />;
    };
    const submit = () => {
      const { data, chosen } = this.state;
      method(data[chosen].name, data[chosen].id);
      navigation.goBack();
    };
    if (this.state.loading) {
      return <RequestLoadingScreen />;
    }
    return (
      <>
        <CustomStatusBar type={`white`} />
        <KeyboardAvoidingView behavior={"padding"} style={styles.container}>
          <View
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              zIndex: 10,
            }}
          >
            <View
              style={{
                paddingHorizontal: 20,
                paddingTop: 20,
                // paddingBottom: 40,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Arrow />
                </TouchableOpacity>
                <View style={{ flex: 1, position: "relative" }}>
                  {this.state.openSearch && (
                    <TextInput
                      returnKeyType={"search"}
                      style={{
                        alignSelf: "center",
                        width: "80%",
                        backgroundColor: "white",
                        borderRadius: 50,
                        paddingHorizontal: 20,
                        height: 40,
                        shadowColor: "rgba(0,0,0,.15)",
                        shadowOffset: {
                          width: 0,
                          height: 2,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 3.84,
                        elevation: 5,
                        fontSize: 18,
                        fontFamily: global.fonts.regular,
                        color: "#444",
                        position: "absolute",
                        top: -5,
                        zIndex: 20,
                      }}
                      value={this.state.search}
                      onChangeText={(text) => this.handleSearch(text)}
                      onSubmitEditing={() => this.onEditEnd()}
                    />
                  )}
                  <Text
                    style={{
                      fontSize: 24,
                      textAlign: "center",
                      fontFamily: global.fonts.medium,
                      color: "#444444",
                    }}
                  >
                    {title}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.setSearch(!this.state.openSearch)}
                >
                  {this.state.openSearch ? (
                    <XBlue height={30} style={{ resizeMode: "contain" }} />
                  ) : (
                    <Search height={30} style={{ resizeMode: "contain" }} />
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {this.state.openSearch == 1 &&
            this.state.search.length > 0 &&
            this.state.show == 0 &&
            this.state.data.length > 0 && (
              <View style={styles.searchBlock}>
                <View
                  style={[
                    {
                      backgroundColor: "white",
                      paddingVertical: 10,
                      borderRadius: 20,
                      flex: 1,
                    },
                    styles.shadow,
                  ]}
                >
                  <ScrollView
                    style={{ backgroundColor: "white", borderRadius: 20 }}
                  >
                    {this.state.data.map((item, key) => {
                      return (
                        <TouchableOpacity
                          key={`${key} ${item.id}`}
                          onPress={() =>
                            this.setCamerView(item.coordinate, key)
                          }
                          style={{
                            width: "94%",
                            alignSelf: "center",
                            backgroundColor: "white",
                            height: 60,
                            borderRadius: 25,
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            paddingHorizontal: 20,
                            shadowColor: "rgba(0,0,0,.15)",
                            shadowOffset: {
                              width: 0,
                              height: 0,
                            },
                            shadowOpacity: 0.6,
                            shadowRadius: 3.84,
                            elevation: 2,
                            marginLeft: 5,
                            marginRight: 5,
                            marginTop: 10,
                          }}
                        >
                          <View
                            style={[
                              styles.icon,
                              type !== "fuel"
                                ? { backgroundColor: global.colors.warn }
                                : { backgroundColor: "#B84BFF" },
                            ]}
                          >
                            <TypeIcon />
                          </View>
                          <View style={{ flex: 1, paddingLeft: 10 }}>
                            <Text
                              style={{
                                color: "#444444",
                                marginBottom: 5,
                                fontSize: 12,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              {item.name}
                            </Text>
                            <Text
                              style={{
                                color: "#999999",
                                fontSize: 9,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              address
                            </Text>
                          </View>
                          <View>
                            <Text
                              style={{
                                color: global.colors.success,
                                marginBottom: 5,
                                fontSize: 12,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              {now.getHours() >
                                parseInt(
                                  item.timeslots[weekDay].startTime.slice(0, 2)
                                ) &&
                              now.getHours() <
                                parseInt(
                                  item.timeslots[weekDay].endTime.slice(0, 2)
                                )
                                ? "Открыто"
                                : "Закрыто"}
                            </Text>
                            <Text
                              style={{
                                color: "#999999",
                                fontSize: 9,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              4.5 KM
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                    <View height={20} />
                  </ScrollView>
                </View>
              </View>
            )}
          <MapView
            initialRegion={{
              latitude: 41.26465,
              longitude: 69.21627,
              latitudeDelta: 0.7,
              longitudeDelta: 0.7,
            }}
            ref={(map) => {
              this.map = map;
            }}
            // onRegionChangeComplete={(region) => this.onRegionChange(region)}
            provider={MapView.PROVIDER_GOOGLE}
            style={styles.mapStyle}
            showsUserLocation={true}
          >
            {this.state.service > 0
              ? this.state.data.map((item, key) => {
                  const coordinate = {
                    longitude: item.coordinate.lng,
                    latitude: item.coordinate.lat,
                  };
                  return (
                    <Marker
                      coordinate={coordinate}
                      key={`${key} ${item.id}`}
                      onPress={() => this.setCamerView(coordinate, key)}
                    >
                      <View
                        style={
                          key === this.state.chosen ? styles.none : styles.show
                        }
                      >
                        {CustomChosen()}
                      </View>
                      <View
                        style={
                          key !== this.state.chosen ? styles.none : styles.show
                        }
                      >
                        {CustomMarker()}
                      </View>
                    </Marker>
                  );
                })
              : null}
          </MapView>
          <View pointerEvents={"none"} style={styles.overlay}>
            <Overlay
              width={global.strings.width}
              height={global.strings.height}
              style={{ position: "absolute", transform: [{ scale: 1.3 }] }}
            />
          </View>
          <View style={{ position: "absolute", right: 20 }}>
            <TouchableOpacity
              onPress={() => this.goToUserLocation()}
              style={[styles.mapButton, { borderRadius: 50 }]}
            >
              <Target width={24} />
            </TouchableOpacity>
          </View>
          {this.state.show ? (
            <View style={styles.whiteBlock}>
              <TouchableOpacity
                style={[
                  styles.button,
                  { position: "absolute", zIndex: 999, right: 20, top: -20 },
                ]}
                onPress={() =>
                  this.setState({ show: false, chosen: -1, service: 1 })
                }
              >
                <Close />
              </TouchableOpacity>
              <View
                style={[
                  {
                    backgroundColor: "white",
                    padding: 20,
                    borderRadius: 20,
                    flex: 1,
                  },
                  styles.shadow,
                ]}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <View
                    style={[
                      styles.icon,
                      type !== "fuel"
                        ? { backgroundColor: global.colors.warn }
                        : { backgroundColor: "#B84BFF" },
                    ]}
                  >
                    <TypeIcon />
                  </View>
                  <View style={{ flex: 1, marginLeft: 15 }}>
                    <Text
                      style={{
                        color: "#444444",
                        marginBottom: 5,
                        fontSize: 14,
                        fontFamily: global.fonts.medium,
                      }}
                    >
                      {typeof this.state.data[this.state.chosen].name !==
                      "undefined"
                        ? this.state.data[this.state.chosen].name
                        : null}
                    </Text>
                    <Text
                      style={{
                        color: "#999999",
                        marginBottom: 5,
                        fontSize: 9,
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      {typeof this.state.data[this.state.chosen].coordinate !==
                      "undefined"
                        ? this.state.data[this.state.chosen].coordinate.address
                        : null}
                    </Text>
                    <Text
                      style={{
                        color: "#999999",
                        fontSize: 9,
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      {typeof this.state.data[this.state.chosen].timeslots !==
                      "undefined"
                        ? this.state.data[this.state.chosen].timeslots[weekDay]
                            .startTime
                        : "Не задан"}{" "}
                      -{" "}
                      {typeof this.state.data[this.state.chosen].timeslots !==
                      "undefined"
                        ? this.state.data[this.state.chosen].timeslots[weekDay]
                            .endTime
                        : "Не задан"}
                    </Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={{
                        textAlign: "right",
                        color: "#444444",
                        marginBottom: 5,
                        fontSize: 14,
                        fontFamily: global.fonts.medium,
                      }}
                    >
                      4.3 КМ
                    </Text>
                    <Text
                      style={{
                        textAlign: "right",
                        color: global.colors.success,
                        marginBottom: 5,
                        fontSize: 9,
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      Открыто
                    </Text>
                    <Text
                      style={{
                        textAlign: "right",
                        color: "#999999",
                        fontSize: 9,
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      Без выходных
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 15,
                    alignItems: "center",
                    justifyContent: "space-evenly",
                    marginBottom: 10,
                  }}
                >
                  <TouchableOpacity
                    style={[
                      styles.bigButton,
                      {
                        backgroundColor: "white",
                        shadowColor: "rgba(0,0,0,.15)",
                        shadowOffset: {
                          width: 0,
                          height: 2,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 3.84,
                        elevation: 5,
                      },
                    ]}
                    onPress={() =>
                      type !== "fuel"
                        ? navigation.navigate("ServiceProfile", {
                            service: this.state.data[this.state.chosen],
                            type: type,
                          })
                        : navigation.navigate("ShowLocator", {
                            service: this.state.data[this.state.chosen],
                          })
                    }
                  >
                    <Text
                      style={{
                        color: "#444",
                        fontSize: 14,
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      Подробнее
                    </Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity
                  style={styles.bigButton}
                  onPress={() => submit()}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 14,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    Выбрать
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </KeyboardAvoidingView>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // width: global.strings.width,
    // height: global.strings.height,
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  input: {
    fontSize: 18,
    fontFamily: global.fonts.regular,
    color: "#444444",
    backgroundColor: "white",
    borderRadius: 50,
    width: 225,
    height: 40,
    paddingLeft: 10,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  whiteBlock: {
    zIndex: 999,
    position: "absolute",
    bottom: 20,
    width: global.strings.width,
    paddingHorizontal: 20,
  },
  serviceButton: {
    alignItems: "center",
    justifyContent: "center",
    width: 90,
    height: 90,
    backgroundColor: "white",
    borderRadius: 20,
    marginBottom: 10,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  mapStyle: {
    width: global.strings.width,
    height: global.strings.height,
    // position: "relative",
    position: "absolute",
    // top:0,
    // left:0,
    // right:0,
    // bottom:0
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 8,
  },
  overlay: {
    position: "absolute",
    width: global.strings.width,
    height: global.strings.height,
    top: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  mapButton: {
    backgroundColor: "white",
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  borderBottom: {
    borderBottomColor: "#E9E9E9",
    borderBottomWidth: 1,
  },
  //   circle: {
  //     backgroundColor: 'black',
  //     borderRadius: 100,
  //     alignSelf: 'stretch',
  //     width:400,
  //     height:300,
  //   },
  none: {
    display: "none",
    overflow: "hidden",
    width: 0,
    height: 0,
  },
  show: {
    width: 90,
    height: 90,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.warn,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  searchBlock: {
    zIndex: 90,
    position: "absolute",
    top: Constants.statusBarHeight + 50,
    width: global.strings.width,
    bottom: 25,
    paddingHorizontal: 20,
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: "white",
    borderRadius: 40,
    shadowColor: "rgba(0,0,0,.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.65,
    shadowRadius: 0.1,
    elevation: 9,
    alignItems: "center",
    justifyContent: "center",
  },
  bigButton: {
    // width: 140,
    flex: 1,
    height: 50,
    backgroundColor: global.colors.mainColor,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
  },
});
