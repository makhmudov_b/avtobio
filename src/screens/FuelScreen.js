import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Metr from "../../assets/kilometr.svg";
import Calendar from "../../assets/calendar-time.svg";
import Location from "../../assets/location.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import SuccessModal from "../components/SuccessModal";
import {formatDate, parseDate} from "../utils/date";

function FuelScreen({ navigation, route }) {
  const { apiService, token } = useContext(Context);
  const { data, auto } = route.params
    ? route.params
    : { data: null, auto: null };
  const [container, setContainer] = useState(0);
  const [litr, setLitr] = useState("");
  const [price, setPrice] = useState("");
  const [number, setNumber] = useState("");
  const [vehicleId, setVehicle] = useState(0);
  const [amount, setAmount] = useState("");
  const [note, setNote] = useState("");
  const [odometer, setOdometr] = useState("");
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [stationName, setStationName] = useState(null);
  const [stationId, setStationId] = useState(null);
  const [petrol, setPetrol] = useState([]);
  const [gasoline, setGasoline] = useState([]);
  const [current, setCurrent] = useState(0);
  const onChangeDate = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setShowTime(true);
    }
  };
  const setStation = (name, id) => {
    setStationName(name);
    setStationId(id);
  };
  const onChangeTime = (event, selectedTime) => {
    setShowTime(false);
    if (selectedTime) {
      setDateChosen(true);
      setDate(selectedTime);
    }
  };
  const chosenDateTime = formatDate(date);
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const saveEvent = () => {
    const getTotal = calculatePrice();
    if (
      dateChosen &&
      parseInt(vehicleId) &&
      parseInt(getTotal) &&
      parseInt(odometer) &&
      parseInt(current) !== 0 &&
      parseInt(stationId) !== 0 &&
      parseInt(price) &&
      parseInt(litr)
    ) {
      const body = {
        note: note,
        odometer: parseInt(odometer),
        vehicleId: parseInt(vehicleId),
        refuelDate: parseDate(date),
        total: parseInt(getTotal),
        fuelTypeId: parseInt(current),
        gasStationId: parseInt(stationId),
        price: parseInt(price),
        qty: parseInt(litr),
      };
      setLoading(true);
      if (data) {
        body.id = data.id;
        apiService
          .updateForm("/events/refuel", token, body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
            }
          });
      } else {
        apiService
          .addEvent(token, "refuel", body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
            }
          });
      }
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const getGasPetroliumData = () => {
    const refereUrl = "/backend/v1/reference/choose?parentCode=";
    apiService
      .getReferences(refereUrl + "PETROL", token)
      .then((value) => {
        if (value.statusCode === 200) {
          setPetrol(value.data);
        }
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
      });
    apiService
      .getReferences(refereUrl + "GAS", token)
      .then((value) => {
        if (value.statusCode === 200) {
          setGasoline(value.data);
        }
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
      });
  };

  useEffect(() => {
    getGasPetroliumData();
    if (data) {
      setLoading(true);
      apiService
        .getResources("/events/refuel/" + data.id, token)
        .then((value) => {
          ChooseAuto(auto.id, auto.plateNumber);
          setCurrent(parseInt(value.data.fuelTypeId));
          setDate(new Date(value.data.refuelDate));
          setStation(value.data.gasStationName, value.data.gasStationId);
          setDateChosen(true);
          setOdometr(value.data.odometer.toString());
          setNote(value.data.note);
          setLitr(value.data.qty.toString());
          setPrice((value.data.total / value.data.qty).toString());
          setContainer(value.data.serviceCode == "PETROL" ? 0 : 1);
          setLoading(false);
        });
    }
  }, []);
  const changeLang = (containerId) => {
    if (containerId === 0) setCurrent(petrol[0].id);
    else setCurrent(gasoline[0].id);
    setPrice("");
    setLitr("");
    setContainer(containerId);
  };
  const calculatePrice = () => {
    if (litr && price) {
      const calcPrice = parseInt(litr) * parseInt(price);
      return calcPrice;
    }
    return "";
  };
  const RightBlock = () => {
    return (
      <View>
        <View
          style={[
            { position: "relative", flexDirection: "row", marginBottom: 10 },
            styles.gazShadow,
          ]}
        >
          {gasoline.length > 0 &&
            gasoline.map((item, key) => {
              return (
                <TouchableOpacity
                  style={[
                    key === 0 ? styles.leftBlock : {},
                    key === gasoline.length - 1 ? styles.rightBlock : {},
                    styles.block,
                    current === item.id
                      ? styles.activeBlock
                      : styles.inactiveGaz,
                  ]}
                  key={key}
                  onPress={() => setCurrent(item.id)}
                >
                  <Text
                    style={[
                      current === item.id
                        ? styles.activeText
                        : styles.inactiveText,
                      { fontFamily: global.fonts.regular },
                    ]}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              );
            })}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ position: "relative", width: 95, marginRight: 10 }}>
            <TextInput
              returnKeyType={"done"}
              keyboardType={"numeric"}
              style={[styles.authorizationInput, { paddingRight: 30 }]}
              value={litr}
              onChangeText={(text) => setLitr(text)}
            />
            <View
              style={[
                styles.icon,
                {
                  right: 15,
                  width: "auto",
                  flexDirection: "row",
                  alignItems: "flex-start",
                  justifyContent: "center",
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 18,
                  color: "#999999",
                  fontFamily: global.fonts.regular,
                }}
              >
                M
              </Text>
              <Text
                style={{
                  fontSize: 8,
                  color: "#999999",
                  fontFamily: global.fonts.regular,
                }}
              >
                3
              </Text>
            </View>
          </View>
          <View style={{ position: "relative", flex: 1 }}>
            <TextInput
              returnKeyType={"done"}
              style={[styles.authorizationInput, { paddingRight: 60 }]}
              placeholder="За метр куб"
              keyboardType={"numeric"}
              placeholderTextColor="#999999"
              value={price}
              onChangeText={(text) => setPrice(text)}
            />
            <View style={[styles.icon, { width: "auto", textAling: "right" }]}>
              <Text
                style={{
                  fontSize: 18,
                  color: "#999999",
                  fontFamily: global.fonts.regular,
                }}
              >
                UZS
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  const LeftBlock = () => {
    return (
      <View>
        <View
          style={[
            { position: "relative", flexDirection: "row", marginBottom: 10 },
            styles.gazShadow,
          ]}
        >
          {petrol.length > 0 &&
            petrol.map((item, key) => {
              return (
                <TouchableOpacity
                  style={[
                    key === 0 ? styles.leftBlock : {},
                    key === petrol.length - 1 ? styles.rightBlock : {},
                    styles.block,
                    current === item.id
                      ? styles.activeBlock
                      : styles.inactiveGaz,
                  ]}
                  key={key}
                  onPress={() => setCurrent(item.id)}
                >
                  <Text
                    style={[
                      current === item.id
                        ? styles.activeText
                        : styles.inactiveText,
                      { fontFamily: global.fonts.regular },
                    ]}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              );
            })}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ position: "relative", width: 95, marginRight: 10 }}>
            <TextInput
              returnKeyType={"done"}
              style={[styles.authorizationInput, { paddingRight: 35 }]}
              keyboardType={"numeric"}
              value={litr}
              onChangeText={(text) => setLitr(text)}
            />
            <View
              style={[
                styles.icon,
                {
                  width: "auto",
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 18,
                  color: "#999999",
                  fontFamily: global.fonts.regular,
                }}
              >
                L
              </Text>
            </View>
          </View>
          <View style={{ position: "relative", flex: 1 }}>
            <TextInput
              returnKeyType={"done"}
              keyboardType={"numeric"}
              style={[styles.authorizationInput, { paddingRight: 60 }]}
              placeholder="За литр"
              placeholderTextColor="#999999"
              value={price}
              onChangeText={(text) => setPrice(text)}
            />
            <View style={[styles.icon, { width: "auto" }]}>
              <Text
                style={{
                  fontSize: 18,
                  color: "#999999",
                  fontFamily: global.fonts.regular,
                }}
              >
                UZS
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      <HeaderBack
        type="white"
        button={true}
        title="Заправка"
        navigation={navigation}
      />
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          is24Hour={true}
          display="default"
          onChange={onChangeDate}
        />
      )}
      {showTime && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`time`}
          is24Hour={true}
          display="default"
          onChange={onChangeTime}
        />
      )}
      <View style={styles.container}>
        <ScrollView>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <View
                  style={{
                    flexDirection: "row",
                    paddingBottom: 10,
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => changeLang(0)}
                    style={[
                      styles.leftBlock,
                      styles.block,
                      !container ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={
                        !container ? styles.activeText : styles.inactiveText
                      }
                    >
                      Бензин
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => changeLang(1)}
                    style={[
                      styles.rightBlock,
                      styles.block,
                      container ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={
                        container ? styles.activeText : styles.inactiveText
                      }
                    >
                      Газ
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("ChooseAuto", {
                        method: ChooseAuto,
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !vehicleId ? "#999999" : "#444444",
                        },
                      ]}
                    >
                      {!vehicleId ? "Выберите авто" : number}
                    </Text>
                    <ArrowRight style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity onPress={() => setShow(true)}>
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !dateChosen ? "#999" : "#444",
                        },
                      ]}
                    >
                      {dateChosen ? chosenDateTime : "Дата и время"}
                    </Text>
                    <Calendar style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("AddressChoice", {
                        title: "Адрес заправки",
                        method: setStation,
                        type: "fuel",
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !stationName ? "#999" : "#444",
                        },
                      ]}
                    >
                      {!stationName ? "Укажите заправку" : stationName}
                    </Text>
                    <Location style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Показание адометра"
                    keyboardType={"numeric"}
                    placeholderTextColor="#999999"
                    value={odometer}
                    onChangeText={(text) => setOdometr(text)}
                  />
                  <View style={styles.icon}>
                    <Metr />
                  </View>
                </View>
                {!container ? LeftBlock() : RightBlock()}
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Примечание ..."
                    placeholderTextColor="#999999"
                    value={note}
                    onChangeText={(text) => setNote(text)}
                  />
                </View>
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <View style={{ position: "relative" }}>
                  <Text
                    style={[
                      styles.authorizationInput,
                      {
                        lineHeight: 50,
                        color: !calculatePrice() ? "#999" : "#444",
                      },
                    ]}
                  >
                    {!calculatePrice() ? "Итого" : calculatePrice()}
                  </Text>
                  <View style={[styles.icon, { width: "auto" }]}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#999999",
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      UZS
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor },
              ]}
              onPress={() => saveEvent()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Сохранить
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <SuccessModal showModal={showModal} setShowModal={setShowModal} />
      </View>
    </>
  );
}

export default FuelScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  gazShadow: {
    backgroundColor: "white",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  inactiveGaz: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "white",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderColor: "#E9E9E9",
    borderRadius: 25,
    backgroundColor: "white",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});
