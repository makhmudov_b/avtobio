import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Photo from "../../assets/photo.svg";
import Calendar from "../../assets/calendar.svg";
import SafeAreaView from "react-native-safe-area-view";
import Constants from "expo-constants";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import SuccessModal from "../components/SuccessModal";
import RequestLoadingScreen from "./RequestLoadingScreen";

function FeedbackScreen({ navigation }) {
  const { apiService, token } = useContext(Context);
  const [title, setTitle] = useState("");
  const [email, setEmail] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [feedback, onChangeFeedback] = useState("");
  const [emotions, onChangeEmotion] = useState([
    { text: "😫" },
    { text: "😒" },
    { text: "😏" },
    { text: "😊" },
    { text: "😁" },
  ]);
  const [mark, onChangeMark] = useState(4);
  const saveFeedback = () => {
    if (!email.length || !feedback.length) {
      Alert.alert("Нужно заполнить все");
      return;
    }
    const body = {
      email,
      feedback,
      rate: mark + 1,
    };
    setLoading(true);
    apiService.postData(token, "/feedback", body).then((value) => {
      setLoading(false);
      if (value.statusCode === 200) {
        setShowModal(true);
        setTimeout(() => navigation.replace("Home"), 1000);
      } else {
        Alert.alert(value.message);
      }
    });
  };
  const EmojiButton = ({ text, index }) => {
    return (
      <TouchableOpacity
        onPress={() => onChangeMark(index)}
        style={[styles.emojiButton]}
      >
        <Text
          style={[
            { fontSize: 24 },
            mark === index ? { opacity: 1 } : { opacity: 0.3 },
          ]}
        >
          {text}
        </Text>
      </TouchableOpacity>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Отзыв"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1, justifyContent: "space-between" }}>
            <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
              <View style={{ height: 20 }} />
              <View style={styles.authorization}>
                <View style={styles.authorizationBlock}>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Почтовый адрес"
                    placeholderTextColor="#999999"
                    value={email}
                    keyboardType={`email-address`}
                    onChangeText={(text) => setEmail(text)}
                  />
                  <TextInput
                    returnKeyType={"done"}
                    style={[
                      styles.authorizationInput,
                      {
                        height: 250,
                        borderRadius: 25,
                        paddingTop: 10,
                        textAlignVertical: "top",
                      },
                    ]}
                    placeholder="Напишите отзыв"
                    placeholderTextColor="#999999"
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={(text) => onChangeFeedback(text)}
                    value={feedback}
                  />
                  <Text
                    style={{
                      fontSize: 18,
                      padding: 20,
                      fontFamily: global.fonts.medium,
                      textAlign: "center",
                    }}
                  >
                    Оцените нас
                  </Text>
                  <View
                    style={{
                      paddingBottom: 20,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                      width: "100%",
                      flexWrap: "wrap",
                    }}
                  >
                    {emotions.map((emotion, key) => {
                      return (
                        <EmojiButton
                          key={key}
                          index={key}
                          text={emotion.text}
                        />
                      );
                    })}
                  </View>
                </View>
              </View>
              <View style={{ width: "85%" }}>
                <TouchableOpacity
                  style={[
                    styles.button,
                    { backgroundColor: global.colors.mainColor },
                  ]}
                  onPress={() => saveFeedback()}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 18,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    Сохранить
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
      <SuccessModal showModal={showModal} setShowModal={setShowModal} />
    </>
  );
}

export default FeedbackScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-around",
  },
  emojiButton: {
    height: 40,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  noPhoto: {
    height: 150,
    borderWidth: 1,
    borderColor: "rgba(0,0,0,.15)",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
  },
  icon: {
    position: "absolute",
    right: 15,
    top: 20,
  },
});
