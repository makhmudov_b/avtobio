import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Send from "../../assets/send.svg";
import HeaderBack from "../components/HeaderBack";

function AddFeedbackScreen({ navigation, route }) {
  const { master } = route.params;
  const [confirmation, onChangeConfirmation] = useState("");
  const screenHeight = Math.round(Dimensions.get("window").height * 0.8);

  const Feeback = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          marginBottom: 10,
          alignItems: "flex-start",
          justifyContent: "space-between",
        }}
      >
        <Image
          source={global.images.comment}
          style={{
            width: 50,
            height: 50,
            resizeMode: "cover",
            borderRadius: 50,
          }}
        />
        <View
          style={[
            {
              flex: 1,
              marginLeft: 15,
              backgroundColor: "white",
              padding: 15,
              borderRadius: 20,
            },
            styles.shadow,
          ]}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                fontSize: 12,
                fontFamily: global.fonts.medium,
                color: "#444444",
                textAlign: "left",
              }}
            >
              Владимер Петров
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontFamily: global.fonts.medium,
                color: "#444444",
                textAlign: "left",
              }}
            >
              04.02.2020
            </Text>
          </View>
          <Text
            style={{
              fontSize: 9,
              color: "#444444",
              marginTop: 5,
              fontFamily: global.fonts.regular,
            }}
          >
            Василий Петрович очень крутой мастер. Сделает все качественно и
            быстро. Мне он установил меджикар за 20 мин.
          </Text>
        </View>
      </View>
    );
  };
  const MyFeeback = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          marginBottom: 10,
          alignItems: "flex-start",
          justifyContent: "space-between",
        }}
      >
        <View
          style={[
            {
              flex: 1,
              marginRight: 15,
              backgroundColor: global.colors.mainColor,
              padding: 15,
              borderRadius: 20,
            },
            styles.shadow,
          ]}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                fontSize: 12,
                fontFamily: global.fonts.medium,
                color: "white",
                textAlign: "left",
              }}
            >
              Владимер Петров
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontFamily: global.fonts.medium,
                color: "white",
                textAlign: "left",
              }}
            >
              04.02.2020
            </Text>
          </View>

          <Text
            style={{
              marginTop: 5,
              fontSize: 9,
              color: "white",
              fontFamily: global.fonts.regular,
            }}
          >
            Василий Петрович очень крутой мастер. Сделает все качественно и
            быстро. Мне он установил меджикар за 20 мин.
          </Text>
        </View>
        <Image
          source={global.images.comment}
          style={{
            width: 50,
            height: 50,
            resizeMode: "cover",
            borderRadius: 50,
          }}
        />
      </View>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          navigation={navigation}
          title={`${master.firstName} ${master.lastName}`}
          button={true}
          type="white"
        />
        <View style={{ height: 20 }} />
        <ScrollView
          style={{ flex: 1 }}
          contentContainerStyle={[
            styles.authorization,
            { backgroundColor: "#F6F6F6" },
          ]}
        >
          <Feeback />
          <Feeback />
          <Feeback />
        </ScrollView>
      </View>
    </>
  );
}

export default AddFeedbackScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
    width: "100%",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 3,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
});
