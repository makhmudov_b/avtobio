import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Metr from "../../assets/kilometr.svg";
import Calendar from "../../assets/calendar-time.svg";
import Editable from "../../assets/edit.svg";
import Checked from "../../assets/checked.svg";
import NotChecked from "../../assets/not-checked.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import SuccessModal from "../components/SuccessModal";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import { parseDate } from "../utils/date";

function ReminderScreen({ navigation }) {
  const { apiService, token } = useContext(Context);
  const [reason, setReason] = useState(0);
  const [reasonId, setReasonId] = useState("");
  const [number, setNumber] = useState("");
  const [vehicleId, setVehicle] = useState(0);
  const [note, setNote] = useState("");
  const [odometer, setOdometr] = useState("");
  const [top, setTop] = useState(0);
  const [type, setType] = useState(0);
  const [regular, onChangeRegular] = useState(0);
  const [showModal, setShowModal] = useState(0);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [cycleName, setCycleName] = useState(null);
  const [cycle, setCycle] = useState(null);
  const [dateChosen, setDateChosen] = useState(false);
  const [loading, setLoading] = useState(false);
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDateChosen(true);
      setDate(selectedDate);
    }
  };
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const chosenDate = `${year}-${month < 10 ? "0" + month : month}-${
    day < 10 ? "0" + day : day
  }`;
  const dateFormatted = `${day < 10 ? "0" + day : day}-${
    month < 10 ? "0" + month : month
  }-${year}`;
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const chooseType = ({ name, id }) => {
    setReason(name);
    setReasonId(id);
  };
  const setTopScreen = (value) => {
    setTop(value);
    setReasonId(0);
  };
  const setCycleMehtod = (value) => {
    setCycleName(value.name);
    setCycle(value.code);
  };
  const saveEvent = () => {
    // type once / regular    if(once) odometer || dateChosen else{ odometr || cycle }
    // regular checked odometr | date
    const bottomChecker = !type
      ? regular
        ? odometer
        : dateChosen
      : regular
      ? odometer
      : cycle;
    if (bottomChecker && parseInt(vehicleId) > 0 && reasonId) {
      const reminderType = top ? "EXPENSE" : "SERVICE";
      const regularData = {
        ...(regular && { recurringOdometer: odometer }),
        ...(!regular && { recurringInterval: cycle }),
        recurringType: "REGULAR_REMINDER",
        typeId: reasonId,
        note,
        vehicleId,
        reminderType,
      };
      const onceData = {
        ...(regular && { exactOdometer: odometer }),
        ...(!regular && { exactTime: parseDate(date) }),
        recurringType: "ONCE_REMINDER",
        typeId: reasonId,
        vehicleId,
        note,
        reminderType,
      };
      const data = type ? regularData : onceData;
      setLoading(true);
      apiService.addEvent(token, "reminder", data, vehicleId).then((value) => {
        setLoading(false);
        if (value.statusCode === 200) {
          setShowModal(true);
          setTimeout(() => navigation.replace("Home"), 1000);
        } else {
          Alert.alert(value.message);
        }
      });
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const BottomLeft = () => {
    return (
      <React.Fragment>
        <View
          style={{
            position: "relative",
          }}
        >
          <View
            style={{
              position: "absolute",
              top: 13,
              left: 0,
            }}
          >
            <TouchableOpacity onPress={() => onChangeRegular(1)}>
              {regular ? <Checked /> : <NotChecked />}
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginLeft: 35 }}>
            <TextInput
              returnKeyType={"done"}
              style={styles.authorizationInput}
              placeholder="В заданный пробег"
              keyboardType={"numeric"}
              placeholderTextColor="#999999"
              value={odometer}
              onChangeText={(text) => setOdometr(text)}
            />
            <View style={styles.icon}>
              <Metr />
            </View>
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <View
            style={{
              position: "absolute",
              top: 13,
              left: 0,
            }}
          >
            <TouchableOpacity onPress={() => onChangeRegular(0)}>
              {!regular ? <Checked /> : <NotChecked />}
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginLeft: 35 }}>
            <TouchableOpacity onPress={() => setShow(true)}>
              <Text
                style={[
                  styles.authorizationInput,
                  {
                    color: !dateChosen ? "#999" : "#444",
                    lineHeight: 50,
                  },
                ]}
              >
                {dateChosen ? dateFormatted : "В заданную дату"}
              </Text>
              <Calendar style={styles.icon} />
            </TouchableOpacity>
          </View>
        </View>
      </React.Fragment>
    );
  };
  const BottomRight = () => {
    return (
      <React.Fragment>
        <View
          style={{
            position: "relative",
          }}
        >
          <View
            style={{
              position: "absolute",
              top: 13,
              left: 0,
            }}
          >
            <TouchableOpacity onPress={() => onChangeRegular(1)}>
              {regular ? <Checked /> : <NotChecked />}
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginLeft: 35 }}>
            <TextInput
              returnKeyType={"done"}
              style={styles.authorizationInput}
              placeholder="Каждый  пробег"
              keyboardType={"numeric"}
              placeholderTextColor="#999999"
              value={odometer}
              onChangeText={(text) => setOdometr(text)}
            />
            <View style={styles.icon}>
              <Metr />
            </View>
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <View
            style={{
              position: "absolute",
              top: 13,
              left: 0,
            }}
          >
            <TouchableOpacity onPress={() => onChangeRegular(0)}>
              {!regular ? <Checked /> : <NotChecked />}
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginLeft: 35 }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Choice", {
                  title: "Цикл уведомлений",
                  setMethod: setCycleMehtod,
                  requestId: false,
                })
              }
            >
              <Text
                style={[
                  styles.authorizationInput,
                  {
                    color: !cycleName ? "#999" : "#444",
                    lineHeight: 50,
                  },
                ]}
              >
                {cycleName ? cycleName : "В заданный период"}
              </Text>
              <Calendar style={styles.icon} />
            </TouchableOpacity>
          </View>
        </View>
      </React.Fragment>
    );
  };
  const RightBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("Choice", {
                title: "Причина вызова",
                setMethod: chooseType,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !reasonId ? "#999999" : "#444444",
                },
              ]}
            >
              {!reasonId ? "Причина вызова" : reason}
            </Text>
            <ArrowRight style={styles.icon} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  const LeftBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("Choice", {
                title: "Причина визита",
                setMethod: chooseType,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !reasonId ? "#999999" : "#444444",
                },
              ]}
            >
              {!reasonId ? "Причина визита" : reason}
            </Text>
            <ArrowRight style={styles.icon} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          display="default"
          onChange={onChange}
        />
      )}
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Напоминания"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <View
                  style={{
                    flexDirection: "row",
                    paddingBottom: 10,
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setTopScreen(0)}
                    style={[
                      styles.leftBlock,
                      styles.block,
                      !top ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={[
                        !top ? styles.activeText : styles.inactiveText,
                        { fontFamily: global.fonts.regular },
                      ]}
                    >
                      Расход
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => setTopScreen(1)}
                    style={[
                      styles.rightBlock,
                      styles.block,
                      top ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={[
                        top ? styles.activeText : styles.inactiveText,
                        { fontFamily: global.fonts.regular },
                      ]}
                    >
                      Сервис
                    </Text>
                  </TouchableOpacity>
                </View>
                {!top ? LeftBlock() : RightBlock()}
                <View style={{ position: "relative" }}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("ChooseAuto", {
                        method: ChooseAuto,
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !vehicleId ? "#999999" : "#444444",
                        },
                      ]}
                    >
                      {!vehicleId ? "Выберите авто" : number}
                    </Text>
                    <ArrowRight style={styles.icon} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <View
                  style={{
                    flexDirection: "row",
                    paddingBottom: 10,
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setType(0)}
                    style={[
                      styles.leftBlock,
                      styles.block,
                      !type ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={[
                        !type ? styles.activeText : styles.inactiveText,
                        { fontFamily: global.fonts.regular },
                      ]}
                    >
                      Один раз
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => setType(1)}
                    style={[
                      styles.rightBlock,
                      styles.block,
                      type ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={[
                        type ? styles.activeText : styles.inactiveText,
                        { fontFamily: global.fonts.regular },
                      ]}
                    >
                      Регулярно
                    </Text>
                  </TouchableOpacity>
                </View>
                {!type ? BottomLeft() : BottomRight()}
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Примечание ..."
                    placeholderTextColor="#999999"
                    value={note}
                    onChangeText={(text) => setNote(text)}
                  />
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor, marginBottom: 30 },
              ]}
              onPress={() => saveEvent()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Сохранить
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <SuccessModal showModal={showModal} setShowModal={setShowModal} />
      </View>
    </>
  );
}

export default ReminderScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});
