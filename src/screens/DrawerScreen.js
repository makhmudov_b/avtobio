import React, { useState, useContext } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Number from "../../assets/drawer-number.svg";
import Circle from "../../assets/drawer-circle.svg";
import Triangle from "../../assets/triangle.svg";
import Cabinet from "../../assets/cabinet.svg";
import Stats from "../../assets/statistics.svg";
import Event from "../../assets/events.svg";
import Flag from "../../assets/flag.svg";
import Location from "../../assets/location.svg";
import Cart from "../../assets/cart.svg";
import Setting from "../../assets/setting.svg";
import Feedback from "../../assets/feedback.svg";
import SOS from "../../assets/sos.svg";
import Instagram from "../../assets/instagram.svg";
import Facebook from "../../assets/facebook.svg";
import Telegram from "../../assets/telegram.svg";
import Warn from "../../assets/warn.svg";
import Document from "../../assets/document.svg";
import Technic from "../../assets/technic.svg";
import Context from "../components/Context";
import CustomStatusBar from "../components/CustomStatusBar";

function DrawerScreen({ navigation }) {
  const { user, currentCar, autos } = useContext(Context);
  return (
    <>
      <CustomStatusBar type={"blue"} />
      <View style={styles.container}>
        <View
          style={{
            minHeight: 100,
            backgroundColor: global.colors.mainColor,
            position: "relative",
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                fontSize: 18,
                fontFamily: global.fonts.medium,
                color: "white",
              }}
            >
              {user && user.fullName}
            </Text>
          </View>
        </View>
        <ScrollView style={{ flex: 1, paddingHorizontal: 20 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate("Cabinet")}
              style={styles.navigationButton}
            >
              <Cabinet />
              <Text style={styles.navigationButtonText}>Личный кабинет</Text>
              <View
                style={{
                  flex: 1,
                }}
              ></View>
            </TouchableOpacity>
            {autos.length > 0 && (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("Document", { auto: currentCar })
                }
                style={styles.navigationButton}
              >
                <Document />
                <Text style={styles.navigationButtonText}>Мои документы</Text>
                <View
                  style={{
                    flex: 1,
                  }}
                ></View>
              </TouchableOpacity>
            )}
            {autos.length > 0 && (
              <>
                <TouchableOpacity
                  onPress={() => navigation.navigate("Statistic")}
                  style={styles.navigationButton}
                >
                  <Stats />
                  <Text style={styles.navigationButtonText}>Статистика</Text>
                  <View style={{ flex: 1, alignItems: "flex-end" }}></View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate("Technical")}
                  style={styles.navigationButton}
                >
                  <Technic />
                  <Text style={styles.navigationButtonText}>
                    Техобслуживаение
                  </Text>
                  <View style={{ flex: 1, alignItems: "flex-end" }}></View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.navigationButton}
                  onPress={() => navigation.navigate("Booking")}
                >
                  <Flag />
                  <Text style={styles.navigationButtonText}>Мои брони</Text>
                  <View style={{ flex: 1, alignItems: "flex-end" }}></View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.navigationButton}
                  onPress={() => navigation.navigate("Map")}
                >
                  <Location width={24} height={24} />
                  <Text style={styles.navigationButtonText}>Локатор услуг</Text>
                  <View style={{ flex: 1, alignItems: "flex-end" }}></View>
                </TouchableOpacity>
              </>
            )}

            {/* <TouchableOpacity
              style={styles.navigationButton}
              onPress={() => navigation.navigate("Store")}
            >
              <Cart />
              <Text style={styles.navigationButtonText}>Магазин</Text>
              <View style={{ flex: 1, alignItems: "flex-end" }}></View>
            </TouchableOpacity> */}
            <TouchableOpacity
              onPress={() => navigation.navigate("Feedback")}
              style={styles.navigationButton}
            >
              <Feedback />
              <Text style={styles.navigationButtonText}>Оставить отзыв</Text>
              <View style={{ flex: 1, alignItems: "flex-end" }}></View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate("Setting")}
              style={styles.navigationButton}
            >
              <Setting />
              <Text style={styles.navigationButtonText}>Настройки</Text>
              <View style={{ flex: 1, alignItems: "flex-end" }}></View>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 20 }}>
            <TouchableOpacity
              onPress={() => navigation.navigate("ExtremeMap")}
              style={styles.sosButton}
            >
              <SOS />
              <Text style={styles.sosButtonText}>Вызвать помощь</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: "space-between",
              marginTop: 20,
              paddingHorizontal: 40,
              flexDirection: "row",
            }}
          >
            {/* <TouchableOpacity style={styles.socialButton}>
              <Instagram />
            </TouchableOpacity>
            <TouchableOpacity style={styles.socialButton}>
              <Facebook />
            </TouchableOpacity>
            <TouchableOpacity style={styles.socialButton}>
              <Telegram />
            </TouchableOpacity> */}
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default DrawerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  socialButton: {
    width: 40,
    height: 40,
    backgroundColor: global.colors.mainColor,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  navigationButton: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 25,
  },
  sosButton: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: global.colors.danger,
    borderRadius: 50,
    height: 50,
  },
  sosButtonText: {
    color: "white",
    fontSize: 18,
    marginLeft: 10,
    fontFamily: global.fonts.regular,
  },
  navigationButtonText: {
    color: "#444444",
    fontSize: 14,
    marginLeft: 10,
    justifyContent: "center",
    fontFamily: global.fonts.regular,
  },
});
