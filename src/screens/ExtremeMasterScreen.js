import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Modal,
  TouchableWithoutFeedback,
  ScrollView,
  StatusBar,
  Alert,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-red.svg";
import ArrowRight from "../../assets/arrow-right-red.svg";
import Edit from "../../assets/edit-red.svg";
import Success from "../../assets/success-icon.svg";
import Location from "../../assets/location-red.svg";
import Constants from "expo-constants";
import HeaderBackRed from "../components/HeaderBackRed";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function ExtremeMasterScreen({ navigation }) {
  const { token, user, apiService } = useContext(Context);
  const [serviceType, setServiceType] = useState(0);
  const [vehicleId, setVehicle] = useState(0);
  const [number, setNumber] = useState("");
  const [note, setNote] = useState("");
  const [amount, setAmount] = useState("");
  const [phone, setPhone] = useState(user.login);
  const [reason, setReason] = useState("");
  const [reasonId, setReasonId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [serviceId, setServiceId] = useState(null);
  const [serviceName, setServiceName] = useState(null);
  const [lattitude, setLattitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [address, setAddress] = useState(null);
  const [chosenMaster, setChosenMaster] = useState(null);
  const [masterId, setMasterId] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const getPrice = (categoryId) => {
    setLoading(true);
    apiService
      .getResources(
        "/sos/technician/minimal-price?categoryId=" + categoryId,
        token
      )
      .then((value) => {
        setLoading(false);
        const { data } = value;
        setAmount(data.toString());
      });
  };
  const handleReason = (value) => {
    setReason(value.name);
    setReasonId(value.id);
    getPrice(value.id);
    if (chosenMaster) setChosenMaster(null);
    if (masterId) setMasterId(null);
    if (serviceName) setServiceName(null);
  };
  const setPosition = ({ lat, lng }) => {
    setLattitude(lat);
    setLongitude(lng);
    apiService
      .getAddress(lat, lng)
      .then((value) => setAddress(value.results[0].formatted_address));
    if (chosenMaster) setChosenMaster(null);
    if (masterId) setMasterId(null);
    if (serviceName) setServiceName(null);
    if (reason) setReason(null);
    if (reasonId) setReasonId(null);
  };
  const saveEvent = () => {
    if (phone && reasonId && vehicleId && address) {
      const data = {
        coordinate: {
          address: address,
          lat: lattitude,
          lng: longitude,
        },
        note: note,
        phone: phone,
        serviceIds: [reasonId],
        vehicleId: vehicleId,
      };
      setLoading(true);
      apiService
        .postData(token, "/sos/technician/order", data, data.vehicleId)
        .then((value) => {
          setLoading(false);
          if (value.statusCode == 200) navigation.replace("Booking");
          else Alert.alert(value.message);
        });
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const LeftBlock = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ flex: 1 }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("ChooseAuto", {
                  method: ChooseAuto,
                })
              }
            >
              <Text
                style={[
                  styles.authorizationInput,
                  { lineHeight: 50, color: !vehicleId ? "#999999" : "#444444" },
                ]}
              >
                {!vehicleId ? "Выберите авто" : number}
              </Text>
              <ArrowRight style={styles.icon} />
            </TouchableOpacity>
            <View style={{ position: "relative" }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddressMap", {
                    title: "Адрес вызова",
                    type: "light",
                    setPosition: setPosition,
                    showAddress: false,
                  })
                }
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    { lineHeight: 50, color: !address ? "#999999" : "#333333" },
                  ]}
                >
                  {!address ? "Место происшествия" : address}
                </Text>
              </TouchableOpacity>
              <View style={styles.icon}>
                <Location />
              </View>
            </View>
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("TypeChoice", {
                title: "Причина вызова",
                setMethod: handleReason,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !reason ? "#999999" : "#333",
                },
              ]}
            >
              {!reason ? "Тип мастера" : reason}
            </Text>
          </TouchableOpacity>
          <View style={styles.icon}>
            <ArrowRight />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Телефон"
            placeholderTextColor="#999999"
            keyboardType={`phone-pad`}
            value={phone}
            onChangeText={(text) => setPhone(text)}
          />
          <Edit style={styles.icon} />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={note}
            onChangeText={(text) => setNote(text)}
          />
        </View>
      </View>
    );
  };
  const callMaster = () => {
    setShowModal(true);
    setTimeout(() => {
      navigation.navigate("ExtremeOrder", { type: 2 }), setShowModal(false);
    }, 2000);
    return 1;
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <HeaderBackRed
        type="white"
        button={true}
        title="Мастер"
        navigation={navigation}
      />
      <View style={styles.container}>
        <Modal animationType="fade" transparent={true} visible={showModal}>
          <TouchableWithoutFeedback
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
          >
            <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
          </TouchableWithoutFeedback>
          <View
            style={{
              flexDirection: "row",
              position: "absolute",
              marginTop: 60,
              alignSelf: "center",
            }}
          >
            <View
              style={[
                styles.authorizationBlock,
                {
                  borderRadius: 20,
                  marginHorizontal: 20,
                  backgroundColor: "white",
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 24,
                  textAlign: "center",
                  marginBottom: 15,
                  fontFamily: global.fonts.medium,
                  color: "#444444",
                }}
              >
                Заявка принята
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: global.fonts.regular,
                  textAlign: "center",
                  color: "#999999",
                }}
              >
                Поставшик услуг может вам позвонить по указанному номеру для
                подтверждения заявки
              </Text>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 20,
                }}
              >
                <Success />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 14,
                    textAlign: "center",
                    color: "#444444",
                  }}
                >
                  Мин. стоимость:
                  <Text style={{ fontWeight: "bold" }}>{amount} UZS</Text>
                </Text>
              </View>
            </View>
          </View>
        </Modal>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ height: 20 }} />
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>{LeftBlock()}</View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={[styles.authorizationInput]}
                    placeholder="Сумма расхода"
                    keyboardType={"numeric"}
                    placeholderTextColor="#999999"
                    value={amount}
                    editable={false}
                  />
                  <View style={[styles.icon, { width: "auto" }]}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#999999",
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      UZS
                    </Text>
                  </View>
                </View>
                <Text
                  style={{
                    color: global.colors.danger,
                    fontSize: 11,
                    fontFamily: global.fonts.regular,
                    textAlign: "center",
                  }}
                >
                  Указанная сумма расхода НЕ включает в себя расходники такие
                  как запчасти, масла и т.д.
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => saveEvent()}
              style={[styles.button, { backgroundColor: global.colors.danger }]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Вызвать мастера
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default ExtremeMasterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    right: 20,
    top: 13,
  },
});
