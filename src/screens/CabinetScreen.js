import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Alert,
  Modal,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Image,
} from "react-native";
import global from "../../resources/global";
import CustomIcon from "../../assets/users.svg";
import Arrow from "../../assets/arrow.svg";
import Address from "../../assets/address.svg";
import Edit from "../../assets/editable.svg";
import { ScrollView } from "react-native-gesture-handler";
import CalendarPhoto from "../../assets/calendar.svg";
import DateTimePicker from "@react-native-community/datetimepicker";
import Context from "../components/Context";
import CustomStatusBar from "../components/CustomStatusBar";

function PasswordScreen({ navigation }) {
  const { apiService, token, user, setUser } = useContext(Context);
  const [name, setName] = useState(user.fullName);
  const [email, setMail] = useState(user.email);
  const [phoneNumber, setPhone] = useState(user.login);
  const [gender, setGender] = useState(user.sex);
  const [date, setDate] = useState(new Date(user.birthDate));
  const [editable, setEditable] = useState(false);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);
  const [modal, setModal] = useState(false);
  const getGender = gender === "MALE" ? "Мужчина" : "Женщина";
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const chosenDate = `${year}-${month < 10 ? "0" + month : month}-${
    day < 10 ? "0" + day : day
  }`;
  const dateFormatted = `${day < 10 ? "0" + day : day}-${
    month < 10 ? "0" + month : month
  }-${year}`;
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
    }
  };
  const ignoreChoice = () => {
    setEditable(false);
    setName(user.fullName);
    setMail(user.email);
    setPhone(user.login);
    setGender(user.sex);
    setDate(new Date(user.birthDate));
  };
  const getUpdatedUser = () => {
    apiService
      .getReferences("/account", token)
      .then((value) => {
        setUser(value);
        setPhone(value.login);
      })
      .catch((e) => {});
  };
  const accept = () => {
    if (name && email && gender && chosenDate) {
      setLoading(true);
      apiService
        .register(token, {
          firstName: name,
          birthDate: chosenDate,
          email: email,
          sex: gender,
        })
        .then((value) => {
          if (value.statusCode === 200) {
            getUpdatedUser();
            setEditable(false);
            setLoading(false);
          } else {
            Alert.alert(value.message);
            setLoading(false);
          }
        });
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };

  const EditButtons = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          marginTop: 10,
          justifyContent: "space-between",
        }}
      >
        <TouchableOpacity
          onPress={() => ignoreChoice()}
          style={{
            flex: 1,
            marginRight: 10,
            borderRadius: 50,
            backgroundColor: global.colors.danger,
            alignItems: "center",
            justifyContent: "center",
            height: 50,
          }}
        >
          <Text
            style={{
              fontSize: 18,
              color: "white",
              fontFamily: global.fonts.regular,
            }}
          >
            Отмена
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => accept()}
          style={{
            flex: 1,
            borderRadius: 50,
            backgroundColor: global.colors.darkGreen,
            alignItems: "center",
            justifyContent: "center",
            height: 50,
          }}
        >
          <Text
            style={{
              fontSize: 18,
              color: "white",
              fontFamily: global.fonts.regular,
            }}
          >
            Сохранить
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <>
      <CustomStatusBar />
      <Modal animationType="fade" transparent={true} visible={modal}>
        <TouchableWithoutFeedback
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
          onPress={() => setModal(false)}
        >
          <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
        </TouchableWithoutFeedback>
        <View
          style={{
            flexDirection: "row",
            position: "absolute",
            marginTop: 60,
            alignSelf: "center",
            justifyContent: "center",
            width: "100%",
          }}
        >
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <TouchableOpacity
                onPress={() => {
                  setGender("MALE");
                  setModal(false);
                }}
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color: "#444",
                      textAlign: "center",
                    },
                  ]}
                >
                  Мужчина
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setGender("FEMALE");
                  setModal(false);
                }}
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      color: "#444",
                      lineHeight: 50,
                      textAlign: "center",
                    },
                  ]}
                >
                  Женщина
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <View>
        <View
          style={{
            height: 100,
            backgroundColor: global.colors.mainColor,
            position: "relative",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              zIndex: 20,
              paddingTop: 20,
              paddingHorizontal: 20,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity
              style={{ width: 40, height: 40 }}
              onPress={() => navigation.goBack()}
            >
              <Arrow />
            </TouchableOpacity>
            <Text style={styles.title}>Личный кабинет</Text>
            <View style={{ opacity: 0 }}>
              <Arrow />
            </View>
          </View>
          <View style={[styles.absoluteBackground]}>
            <CustomIcon width={280} height={125} />
          </View>
        </View>
      </View>
      {show ? (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          display="default"
          onChange={onChange}
        />
      ) : null}
      <ScrollView>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            paddingBottom: 20,
            paddingTop: 20,
            backgroundColor: "#F6F6F6",
          }}
        >
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <TextInput
                returnKeyType={"done"}
                style={[
                  styles.authorizationInput,
                  { color: !editable ? "#999999" : "#333333" },
                ]}
                placeholder="Ф.И.О."
                placeholderTextColor="#999999"
                editable={editable}
                value={name}
                onChangeText={(text) => setName(text)}
              />
              <TextInput
                returnKeyType={"done"}
                style={[
                  styles.authorizationInput,
                  { color: !editable ? "#999999" : "#333333" },
                ]}
                placeholder="Телефон"
                placeholderTextColor="#999999"
                keyboardType={"phone-pad"}
                editable={false}
                value={phoneNumber}
                onChangeText={(text) => setPhone(text)}
              />
              <TextInput
                returnKeyType={"done"}
                style={[
                  styles.authorizationInput,
                  { color: !editable ? "#999999" : "#333333" },
                ]}
                placeholder="Почтовый ящик"
                keyboardType={"email-address"}
                placeholderTextColor="#999999"
                editable={false}
                value={email}
                onChangeText={(text) => setMail(text)}
              />
              <View style={{ position: "relative" }}>
                <TouchableOpacity
                  onPress={() => {
                    editable ? setShow(true) : setShow(false);
                  }}
                  disabled={!editable}
                >
                  <Text
                    style={[
                      [
                        styles.authorizationInput,
                        { color: !editable ? "#999999" : "#333333" },
                        {
                          lineHeight: 50,
                        },
                      ],
                    ]}
                  >
                    {dateFormatted}
                  </Text>
                  <CalendarPhoto style={styles.icon} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                disabled={!editable}
                onPress={() => setModal(true)}
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color: !editable ? "#999999" : "#333",
                    },
                  ]}
                >
                  {!gender ? "Пол" : getGender}
                </Text>
              </TouchableOpacity>
              {!editable ? (
                loading ? (
                  <View>
                    <Image
                      source={global.images.loader}
                      style={{ height: 200, width: 200, alignSelf: "center" }}
                    />
                  </View>
                ) : (
                  <View>
                    <TouchableOpacity
                      onPress={() => setEditable(true)}
                      style={[
                        styles.button,
                        { backgroundColor: global.colors.mainColor },
                      ]}
                    >
                      <View style={{ position: "absolute", top: 10, left: 20 }}>
                        <Edit />
                      </View>
                      <Text
                        style={{
                          color: "white",
                          fontSize: 18,
                          fontFamily: global.fonts.regular,
                        }}
                      >
                        Изменить
                      </Text>
                    </TouchableOpacity>
                  </View>
                )
              ) : (
                <EditButtons />
              )}
            </View>
          </View>
          <View style={styles.authorizationBottom}>
            <View style={styles.authorizationBlock}>
              <TouchableOpacity
                style={[
                  styles.authorizationInput,
                  { color: editable ? !"#999999" : "#333", marginBottom: 0 },
                  333,
                ]}
                onPress={() => navigation.navigate("MyAddress")}
              >
                <View>
                  <Address style={{ position: "absolute", top: 13, left: 0 }} />
                </View>
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 18,
                    lineHeight: 50,
                    fontFamily: global.fonts.regular,
                    color: "#444",
                  }}
                >
                  Мои адреса
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

export default PasswordScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    position: "relative",
  },
  absoluteBackground: {
    paddingTop: 10,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: -25,
    width: `100%`,
    height: `100%`,
    left: 0,
    right: 0,
  },
  title: {
    color: "white",
    flex: 1,
    textAlign: "center",
    fontFamily: global.fonts.medium,
    fontSize: 24,
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginBottom: 20,
  },
  authorizationBottom: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  authorizationTop: {
    alignItems: "center",
    justifyContent: "center",
    height: 76,
    borderBottomWidth: 1,
    borderBottomColor: "#E9E9E9",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 20,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 10,
    borderColor: global.colors.mainColor,
    borderWidth: 2,
  },
  input: {
    textAlign: "center",
    fontSize: 36,
    fontFamily: global.fonts.medium,
    letterSpacing: 20,
  },
  bottomLine: {
    height: 2,
    width: 160,
    marginTop: 8,
    marginBottom: 20,
    backgroundColor: "#E9E9E9",
  },
  icon: {
    position: "absolute",
    right: 15,
    top: 13,
  },
});
