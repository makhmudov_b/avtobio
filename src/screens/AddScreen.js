import React, { useState } from "react";
import {
  Text,
  View,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Service from "../../assets/service.svg";
import Road from "../../assets/road.svg";
import Tax from "../../assets/tax.svg";
import Notify from "../../assets/notify.svg";
import Fuel from "../../assets/fuel.svg";
import Dollar from "../../assets/dollar.svg";
import HeaderBack from "../components/HeaderBack";

function AddScreen({ navigation }) {
  const [events, changeEvents] = useState([
    {
      screen: "Fuel",
      name: "Заправка",
      type: "fuel",
      color: global.colors.violet,
    },
    {
      screen: "Service",
      name: "Сервис",
      type: "service",
      color: global.colors.warn,
    },
    {
      screen: "Road",
      name: "Поездка",
      type: "road",
      color: global.colors.preDanger,
    },
    {
      screen: "Reminder",
      name: "Напоминание",
      type: "notify",
      color: global.colors.lightGreen,
    },
  ]);
  return (
    <>
      <HeaderBack
        type="white"
        button={true}
        title="Добавить событие"
        navigation={navigation}
      />
      <ScrollView style={styles.container}>
        <View style={{ height: 20 }} />
        <View style={{ flex: 1 }}>
          <View style={[styles.whiteBox, { marginBottom: 20 }]}>
            {events.map((event, index) => {
              return (
                <TouchableOpacity
                  onPress={() => navigation.navigate(event.screen)}
                  key={index}
                  style={[
                    {
                      borderRadius: 20,
                      alignItems: "center",
                      justifyContent: "center",
                      width: "45%",
                      backgroundColor: "white",
                      shadowColor: "rgba(0,0,0,.15)",
                      shadowOffset: {
                        width: 0,
                        height: 2,
                      },
                      shadowOpacity: 0.9,
                      shadowRadius: 3.84,
                      elevation: 5,
                    },
                    index === 1 || index === 0 ? { marginBottom: 20 } : {},
                    (index + 1) % 2 ? styles.left : styles.right,
                  ]}
                >
                  <View style={[styles.innerBox]}>
                    <View
                      style={{ justifyContent: "center", alignItems: "center" }}
                    >
                      <View
                        style={[styles.box, { backgroundColor: event.color }]}
                      >
                        {event.type === "service" ? <Service /> : null}
                        {event.type === "road" ? <Road /> : null}
                        {event.type === "tax" ? <Tax /> : null}
                        {event.type === "notify" ? <Notify /> : null}
                        {event.type === "fuel" ? <Fuel /> : null}
                      </View>
                      <Text style={styles.innerText}>{event.name}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
          <View style={[styles.whiteBox, { marginBottom: 20 }]}>
            <TouchableOpacity
              onPress={() => navigation.navigate("Income")}
              style={[
                {
                  borderRadius: 20,
                  backgroundColor: "white",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "45%",
                  shadowColor: "rgba(0,0,0,.15)",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.9,
                  shadowRadius: 3.84,
                  elevation: 5,
                },
                styles.left,
              ]}
            >
              <View style={[styles.innerBox]}>
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <View
                    style={[
                      styles.box,
                      { backgroundColor: global.colors.darkGreen },
                    ]}
                  >
                    <Dollar />
                  </View>
                  <Text style={styles.innerText}>Заработок</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate("Expense")}
              style={[
                {
                  borderRadius: 20,
                  backgroundColor: "white",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "45%",
                  shadowColor: "rgba(0,0,0,.15)",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.9,
                  shadowRadius: 3.84,
                  elevation: 5,
                },
                styles.right,
              ]}
            >
              <View style={[styles.innerBox]}>
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <View
                    style={[
                      styles.box,
                      { backgroundColor: global.colors.danger },
                    ]}
                  >
                    <Dollar />
                  </View>
                  <Text style={styles.innerText}>Расход</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#F6F6F6" },
  left: {
    marginRight: 10,
  },
  right: {
    marginLeft: 10,
  },
  whiteBox: {
    backgroundColor: "white",
    marginRight: 20,
    marginLeft: 20,
    padding: 20,
    justifyContent: "space-between",
    flexDirection: "row",
    flexWrap: "wrap",
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 8,
  },
  innerBox: {
    height: 137,
    width: 137,
    alignItems: "center",
    justifyContent: "center",
  },
  innerText: {
    color: "#444444",
    fontSize: 14,
    fontFamily: global.fonts.medium,
    marginTop: 10,
  },
  box: {
    height: global.strings.width / 6,
    width: global.strings.width / 6,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
});
export default AddScreen;
