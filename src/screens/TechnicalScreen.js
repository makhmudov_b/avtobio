import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Metr from "../../assets/kilometr.svg";
import Calendar from "../../assets/calendar-time.svg";
import Editable from "../../assets/edit.svg";
import Location from "../../assets/location.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import Success from "../../assets/success-icon.svg";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import {formatDate, parseDate} from "../utils/date";

function TechnicalScreen({ navigation }) {
  const { token, user, apiService } = useContext(Context);
  const [serviceType, setServiceType] = useState(0);
  const [showModal, setShowModal] = useState(0);
  const [vehicleId, setVehicle] = useState(0);
  const [number, setNumber] = useState("");
  const [note, setNote] = useState("");
  const [amount, setAmount] = useState(null);
  const [phone, setPhone] = useState(user.login);
  const [reason, setReason] = useState("");
  const [reasonId, setReasonId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const [serviceId, setServiceId] = useState(null);
  const [serviceName, setServiceName] = useState(null);
  const [lattitude, setLattitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [address, setAddress] = useState(null);
  const [chosenMaster, setChosenMaster] = useState(null);
  const [masterId, setMasterId] = useState(null);
  const onChangeDate = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setShowTime(true);
    }
  };
  const onChangeTime = (event, selectedTime) => {
    setShowTime(false);
    if (selectedTime) {
      setDateChosen(true);
      setDate(selectedTime);
    }
  };
  const setService = (name, id) => {
    setServiceName(name);
    setServiceId(id);
  };
const chosenDateTime = formatDate(date);
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const handleReason = (value) => {
    setReason(value.name);
    setReasonId(value.id);
    if (chosenMaster) setChosenMaster(null);
    if (masterId) setMasterId(null);
    if (serviceName) setServiceName(null);
    if (amount) setAmount(null);
  };
  const setPosition = ({ lat, lng }) => {
    setLattitude(lat);
    setLongitude(lng);
    apiService
      .getAddress(lat, lng)
      .then((value) => setAddress(value.results[0].formatted_address));
    if (chosenMaster) setChosenMaster(null);
    if (masterId) setMasterId(null);
    if (serviceName) setServiceName(null);
    if (reason) setReason(null);
    if (reasonId) setReasonId(null);
  };
  const setMaster = (value) => {
    setChosenMaster(value.firstName + " " + value.lastName);
    setMasterId(value.id);
    setAmount(value.price.toString());
    setServiceName(value.providerName);
  };

  const saveEvent = () => {
    if (dateChosen && phone && reasonId && masterId && vehicleId) {
      const data = {
        coordinate: {
          address: address,
          lat: lattitude,
          lng: longitude,
        },
        note: note,
        orderDate: parseDate(date),
        phone: phone,
        serviceIds: [reasonId],
        technicianId: masterId,
        type: !serviceType ? "VISIT" : "CALL",
        vehicleId: vehicleId,
      };
      setLoading(true);
      apiService
        .postData(token, "/booking/order", data, data.vehicleId)
        .then((value) => {
          setLoading(false);
          if (value.statusCode == 200) navigation.replace("Booking");
          else {
            Alert.alert(value.message);
          }
        })
        .catch((e) => {
          setLoading(false);
          Alert.alert("Что-то пошло не так!");
        });
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const RightBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("AddressMap", {
                showAddress: false,
                title: "Адрес вызова",
                type: "light",
                setPosition: setPosition,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  color: !address ? "#999" : "#333",
                  paddingRight: 50,
                  lineHeight: 50,
                },
              ]}
            >
              {!address ? "Укажите адрес" : address}
            </Text>
            <Location style={styles.icon} />
          </TouchableOpacity>
        </View>
        {address && (
          <React.Fragment>
            <View style={{ position: "relative" }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("TypeChoice", {
                    title: "Причина вызова",
                    setMethod: handleReason,
                  })
                }
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color: !reason ? "#999999" : "#333",
                    },
                  ]}
                >
                  {!reason ? "Причина вызова" : reason}
                </Text>
                <ArrowRight style={styles.icon} />
              </TouchableOpacity>
            </View>
            {reasonId ? (
              <View style={{ position: "relative" }}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate("Master", {
                      category: reasonId,
                      method: setMaster,
                    })
                  }
                >
                  <Text
                    style={[
                      styles.authorizationInput,
                      {
                        lineHeight: 50,
                        color: !chosenMaster ? "#999999" : "#333",
                      },
                    ]}
                  >
                    {!chosenMaster ? "Имя мастера" : chosenMaster}
                  </Text>
                  <ArrowRight style={styles.icon} />
                </TouchableOpacity>
              </View>
            ) : null}
          </React.Fragment>
        )}
      </View>
    );
  };
  const LeftBlock = () => {
    return (
      <View>
        {serviceName ? (
          <View style={{ position: "relative" }}>
            <TouchableOpacity
              disabled={true}
              onPress={() =>
                navigation.navigate("AddressChoice", {
                  title: "Выберити сервис",
                  method: setService,
                  type: "service",
                })
              }
            >
              <Text
                style={[
                  styles.authorizationInput,
                  { lineHeight: 50, color: !serviceName ? "#999" : "#444" },
                ]}
              >
                {!serviceName ? "Адрес мастерской" : serviceName}
              </Text>
              <Location style={styles.icon} />
            </TouchableOpacity>
          </View>
        ) : null}
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("TypeChoice", {
                title: "Причина визита",
                setMethod: handleReason,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !reason ? "#999999" : "#333",
                },
              ]}
            >
              {!reason ? "Причина визита" : reason}
            </Text>
            <ArrowRight style={styles.icon} />
          </TouchableOpacity>
        </View>
        {reasonId ? (
          <View style={{ position: "relative" }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Master", {
                  category: reasonId,
                  method: setMaster,
                })
              }
            >
              <Text
                style={[
                  styles.authorizationInput,
                  { lineHeight: 50, color: !chosenMaster ? "#999999" : "#333" },
                ]}
              >
                {!chosenMaster ? "Имя мастера" : chosenMaster}
              </Text>
              <ArrowRight style={styles.icon} />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      {show && (
        <DateTimePicker
          value={date}
          timeZoneOffsetInMinutes={300}
          mode={`date`}
          display="default"
          onChange={onChangeDate}
          minimumDate={new Date()}
        />
      )}
      {showTime && (
        <DateTimePicker
          value={date}
          timeZoneOffsetInMinutes={300}
          mode={`time`}
          is24Hour={true}
          display="default"
          onChange={onChangeTime}
        />
      )}
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Техобслуживание"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <View
                  style={{
                    flexDirection: "row",
                    paddingBottom: 10,
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      serviceType !== 0 ? setServiceType(0) : null;
                    }}
                    style={[
                      styles.leftBlock,
                      styles.block,
                      !serviceType ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={[
                        !serviceType ? styles.activeText : styles.inactiveText,
                        { fontFamily: global.fonts.regular },
                      ]}
                    >
                      Визит
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      serviceType !== 1 ? setServiceType(1) : null;
                    }}
                    style={[
                      styles.rightBlock,
                      styles.block,
                      serviceType ? styles.activeBlock : styles.inactiveBlock,
                    ]}
                  >
                    <Text
                      style={[
                        serviceType ? styles.activeText : styles.inactiveText,
                        { fontFamily: global.fonts.regular },
                      ]}
                    >
                      Вызов
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("ChooseAuto", {
                        method: ChooseAuto,
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !vehicleId ? "#999999" : "#444444",
                        },
                      ]}
                    >
                      {!vehicleId ? "Выберите авто" : number}
                    </Text>
                    <ArrowRight style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity onPress={() => setShow(true)}>
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !dateChosen ? "#999" : "#444",
                        },
                      ]}
                    >
                      {dateChosen ? chosenDateTime : "Дата и время"}
                    </Text>
                    <Calendar style={styles.icon} />
                  </TouchableOpacity>
                </View>
                {!serviceType ? LeftBlock() : RightBlock()}
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={[styles.authorizationInput]}
                    keyboardType={"phone-pad"}
                    placeholder="Номер телефона"
                    placeholderTextColor="#999999"
                    value={phone}
                    onChangeText={(text) => setPhone(text)}
                  />
                  <View style={styles.icon}>
                    <Editable />
                  </View>
                </View>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={[styles.authorizationInput, { marginBottom: 0 }]}
                    placeholder="Примечание ..."
                    placeholderTextColor="#999999"
                    value={note}
                    onChangeText={(text) => setNote(text)}
                  />
                </View>
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={[styles.authorizationInput]}
                    placeholder="Сумма расхода"
                    keyboardType={"numeric"}
                    placeholderTextColor="#999999"
                    value={amount}
                    editable={false}
                  />
                  <View style={[styles.icon, { width: "auto" }]}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#999999",
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      UZS
                    </Text>
                  </View>
                </View>
                <Text
                  style={{
                    color: global.colors.danger,
                    fontSize: 11,
                    fontFamily: global.fonts.regular,
                    textAlign: "center",
                  }}
                >
                  Указанная сумма расхода НЕ включает в себя расходники такие
                  как запчасти, масла и т.д.
                </Text>
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor, marginBottom: 30 },
              ]}
              onPress={() => saveEvent()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                {serviceType ? "Вызвать" : "Записаться"}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Modal animationType="fade" transparent={true} visible={showModal}>
          <TouchableWithoutFeedback
            onPress={() => setShowModal(false)}
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
          >
            <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
          </TouchableWithoutFeedback>
          <View
            style={{
              flexDirection: "row",
              position: "absolute",
              marginTop: 60,
              alignSelf: "center",
            }}
          >
            <View
              style={[
                styles.authorizationBlock,
                {
                  borderRadius: 20,
                  marginHorizontal: 20,
                  backgroundColor: "white",
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 24,
                  textAlign: "center",
                  marginBottom: 15,
                  fontFamily: global.fonts.medium,
                  color: "#444444",
                }}
              >
                Заявка принята
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: global.fonts.medium,
                  textAlign: "center",
                  color: "#444444",
                }}
              >
                {chosenMaster}
              </Text>
              <Text
                style={{ fontSize: 9, textAlign: "center", color: "#999999" }}
              >
                Поставшик услуг может вам позвонить по указанному номеру для
                подтверждения заявки
              </Text>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 20,
                }}
              >
                <Success />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 14,
                    textAlign: "center",
                    color: "#444444",
                  }}
                >
                  Мин. стоимость:
                  <Text style={{ fontWeight: "bold" }}>{amount} UZS</Text>
                </Text>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </>
  );
}

export default TechnicalScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
    backgroundColor: "white",
  },
});
