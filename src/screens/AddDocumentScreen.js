import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
  TextInput,
  Platform,
} from "react-native";
import global from "../../resources/global";
import Plus from "../../assets/plus.svg";
import Edit from "../../assets/edit-white.svg";
import Trash from "../../assets/trash.svg";
import Document from "../../assets/documents.svg";
import Calendar from "../../assets/calendar.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import Context from "../components/Context";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import * as ImagePicker from "expo-image-picker";
import RequestLoadingScreen from "./RequestLoadingScreen";

function AddDocumentScreen({ navigation, route }) {
  const { apiService, token, phone } = useContext(Context);
  const { params } = route;
  const { code, name, id, vehicleId, updateMethod } = params;
  const [loading, setLoading] = useState(false);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [docName, setDocName] = useState("");
  const [dateChosen, setDateChosen] = useState(false);
  const [document, setDocument] = useState(null);
  const [frontImage, setFront] = useState(null);
  const [backImage, setBack] = useState(null);

  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDateChosen(true);
      setDate(selectedDate);
    }
  };
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const chosenDate = `${year}-${month < 10 ? "0" + month : month}-${
    day < 10 ? "0" + day : day
  }`;
  const dateFormatted = `${day < 10 ? "0" + day : day}-${
    month < 10 ? "0" + month : month
  }-${year}`;

  useEffect(() => {
    getPermissionAsync();
    if (id) {
      if (!loading) setLoading(true);
      apiService
        .getResources("/document/" + id.id, token, vehicleId)
        .then((value) => {
          if (!loading) setLoading(false);
          setDocument(value.data);
          setDocName(value.data.name);
          setDate(new Date(value.data.expirationDate));
          setDateChosen(true);
          if (value.data.frontImageUrl) {
            setFront(value.data.frontImageUrl);
          }
          if (value.data.backImageUrl) {
            setBack(value.data.backImageUrl);
          }
        })
        .catch((e) => {
          Alert.alert("Что-то пошло не так!");
        });
    }
  }, []);
  const saveDocument = () => {
    if (
      !dateChosen ||
      !frontImage ||
      !backImage ||
      (typeof name != "undefined" ? !name.length : !docName.length)
    ) {
      Alert.alert("Нужна заполнить поля");
      return;
    }
    const body = {
      frontImage: frontImage,
      backImage: backImage,
      name: name ? name : docName,
      expirationDate: chosenDate,
      type: code ? code : null,
    };
    if (!loading) setLoading(true);
    apiService
      .postDocument(token, "/document", body, vehicleId)
      .then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          updateMethod();
          navigation.goBack();
        } else {
          Alert.alert("Что-то пошло не так!");
        }
      })
      .catch((e) => {
        setLoading(false);
        Alert.alert("Что-то пошло не так!");
      });
  };
  const updateDocument = () => {
    if (
      !dateChosen ||
      !frontImage ||
      !backImage ||
      (typeof name != "undefined" ? !name.length : !docName.length)
    ) {
      Alert.alert("Нужна заполнить поля");
      return;
    }
    const checkImage = (image) => {
      const checkUrl = image.slice(0, 5);
      if (checkUrl == "https") return null;
      return image;
    };
    const body = {
      frontImage: checkImage(frontImage),
      backImage: checkImage(backImage),
      name: name ? name : docName,
      expirationDate: chosenDate,
      type: code ? code : null,
    };
    if (!loading) setLoading(true);
    apiService
      .editDocument(token, "/document/" + document.id, body, vehicleId)
      .then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          updateMethod();
          navigation.goBack();
        } else {
          Alert.alert("Что-то пошло не так!");
        }
      })
      .catch((e) => {
        setLoading(false);
        Alert.alert("Что-то пошло не так!");
      });
  };

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        Alert.alert("Нужно дать доступ для того чтобы это работало");
      }
    }
  };
  const _pickImage = async (imageType) => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsMultipleSelection: false,
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        if (imageType === 0) setFront(result.uri);
        else setBack(result.uri);
      }
    } catch (E) {
      Alert.alert("Что-то пошло не так");
    }
  };
  const viewImage = (type) => {
    if (type == 0) navigation.navigate("ShowImage", { file: frontImage });
    else navigation.navigate("ShowImage", { file: backImage });
  };
  const deleteImage = (type) => {
    if (type == 0) setFront(null);
    else setBack(null);
  };
  const deleteDocument = () => {
    apiService
      .deleteData("/document/" + document.id, token, vehicleId)
      .then((value) => {
        if (value.statusCode == 200) {
          updateMethod();
          navigation.goBack();
        } else {
          Alert.alert("Что-то пошло не так");
        }
      });
  };
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      {show && (
        <View
          style={
            Platform.OS == "ios"
              ? {
                  position: "absolute",
                  zIndex: 10,
                  bottom: 0,
                  width: "100%",
                  height: "100%",
                  justifyContent: "flex-end",
                }
              : {}
          }
        >
          {/* {Platform.OS == "ios" && (
            <View>
              <TouchableOpacity>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          )} */}
          <DateTimePicker
            timeZoneOffsetInMinutes={0}
            value={date}
            mode={`date`}
            is24Hour={true}
            style={Platform.OS == "ios" && { backgroundColor: "white" }}
            display="default"
            onChange={onChange}
          />
        </View>
      )}
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title={name ? name : "Документ"}
          // title={type ? type.name : ""}
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              paddingHorizontal: 20,
              backgroundColor: "#F6F6F6",
            }}
          >
            <View style={{ height: 20 }} />
            <View>
              {/* {type.code !== 'DRIVER_LICENSE'} */}
              {code != "DRIVER_LICENSE" && code != "TECH_PASSPORT" && (
                <View>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    value={docName}
                    placeholder={`Название документа`}
                    onChangeText={(text) => setDocName(text)}
                  />
                </View>
              )}
              <View style={{ marginTop: 20 }}>
                <View
                  style={[
                    {
                      width: "100%",
                      backgroundColor: "white",
                      height: 200,
                      alignItems: "center",
                      justifyContent: "center",
                      marginBottom: 15,
                      borderRadius: 20,
                      alignItems: "center",
                    },
                    styles.shadow,
                  ]}
                >
                  {!frontImage ? (
                    <TouchableOpacity
                      style={{
                        backgroundColor: global.colors.mainColor,
                        borderRadius: 15,
                        padding: 10,
                      }}
                      onPress={() => _pickImage(0)}
                    >
                      <Plus />
                    </TouchableOpacity>
                  ) : (
                    <View style={{ position: "relative", width: "100%" }}>
                      <Image
                        source={{ uri: frontImage }}
                        style={{
                          width: "100%",
                          height: 200,
                          resizeMode: "cover",
                          borderRadius: 10,
                        }}
                      />
                      <View
                        style={{
                          position: "absolute",
                          bottom: 30,
                          left: 0,
                          right: 0,
                          height: 20,
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            backgroundColor: global.colors.mainColor,
                            borderRadius: 15,
                            padding: 10,
                            marginRight: 10,
                          }}
                          onPress={() => viewImage(0)}
                        >
                          <Document width={30} height={30} />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            backgroundColor: global.colors.success,
                            borderRadius: 15,
                            padding: 10,
                          }}
                          onPress={() => _pickImage(0)}
                        >
                          <Edit width={30} height={30} />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            backgroundColor: global.colors.danger,
                            borderRadius: 15,
                            padding: 10,
                            marginLeft: 10,
                          }}
                          onPress={() => deleteImage(0)}
                        >
                          <Trash width={30} height={30} />
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                </View>
                <View
                  style={[
                    {
                      width: "100%",
                      backgroundColor: "white",
                      height: 200,
                      alignItems: "center",
                      justifyContent: "center",
                      marginBottom: 15,
                      borderRadius: 20,
                      alignItems: "center",
                    },
                    styles.shadow,
                  ]}
                >
                  {!backImage ? (
                    <TouchableOpacity
                      style={{
                        backgroundColor: global.colors.mainColor,
                        borderRadius: 15,
                        padding: 10,
                      }}
                      onPress={() => _pickImage(1)}
                    >
                      <Plus />
                    </TouchableOpacity>
                  ) : (
                    <View style={{ position: "relative", width: "100%" }}>
                      <Image
                        source={{ uri: backImage }}
                        style={{
                          width: "100%",
                          height: 200,
                          resizeMode: "cover",
                          borderRadius: 10,
                        }}
                      />
                      <View
                        style={{
                          position: "absolute",
                          bottom: 30,
                          left: 0,
                          right: 0,
                          height: 20,
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            backgroundColor: global.colors.mainColor,
                            borderRadius: 15,
                            padding: 10,
                            marginRight: 10,
                          }}
                          onPress={() => viewImage(1)}
                        >
                          <Document width={30} height={30} />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            backgroundColor: global.colors.success,
                            borderRadius: 15,
                            padding: 10,
                          }}
                          onPress={() => _pickImage(1)}
                        >
                          <Edit width={30} height={30} />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            backgroundColor: global.colors.danger,
                            borderRadius: 15,
                            padding: 10,
                            marginLeft: 10,
                          }}
                          onPress={() => deleteImage(1)}
                        >
                          <Trash width={30} height={30} />
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setShow(true)}
                    style={[
                      { width: "100%", borderRadius: 25, overflow: "hidden" },
                    ]}
                  >
                    <Text
                      style={[styles.authorizationInput, { lineHeight: 50 }]}
                    >
                      {dateChosen ? chosenDate : "Срок истечения"}
                    </Text>
                    <Calendar style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() =>
                      document ? updateDocument() : saveDocument()
                    }
                    style={[
                      styles.button,
                      { backgroundColor: global.colors.mainColor },
                    ]}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontFamily: global.fonts.regular,
                        fontSize: 18,
                      }}
                    >
                      Сохранить
                    </Text>
                  </TouchableOpacity>
                  {document && (
                    <TouchableOpacity
                      onPress={() => deleteDocument()}
                      style={[
                        styles.button,
                        { backgroundColor: global.colors.mainColor },
                      ]}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: global.fonts.regular,
                          fontSize: 18,
                        }}
                      >
                        Удалить
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default AddDocumentScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  carNumber: {
    borderWidth: 2,
    borderColor: global.colors.mainColor,
    borderRadius: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
    marginTop: 10,
    height: 60,
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 20,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
  },
  shadow: {
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    width: "100%",
  },
  icon: {
    position: "absolute",
    right: 15,
    top: 13,
  },
});
