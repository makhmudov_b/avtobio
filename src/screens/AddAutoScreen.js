import React, { useState, useContext, useRef } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Calendar from "../../assets/calendar.svg";
import Barcode from "../../assets/barcode.svg";
import Uzbekistan from "../../assets/uzbekistan.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import Metr from "../../assets/kilometr.svg";
import { TextInputMask } from "react-native-masked-text";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function AddAutoScreen({ navigation }) {
  // let passwordRef = useRef(null);
  const { apiService, token, getGarage } = useContext(Context);
  const [language, onChangeLang] = useState(0);
  const [loading, setLoading] = useState(false);
  const [leftPlate, setLeftPlate] = useState("");
  const [rightPlate, setRightPlate] = useState("");
  const [current, setCurrent] = useState("");
  const [passport, setPassport] = useState("");
  const [manufacturer, setManufacturer] = useState(0);
  const [model, setModel] = useState(0);
  const [modify, setModify] = useState(0);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const secondInput = useRef(null);
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDateChosen(true);
      setDate(selectedDate);
    }
  };
  function changeManufacturer(value) {
    if (model != 0) setModel(0);
    if (modify != 0) setModify(0);
    setManufacturer(value);
  }
  function changeModel(value) {
    if (modify != 0) setModify(0);
    setModel(value);
  }

  const changeLang = (langId) => {
    onChangeLang(langId);
    setRightPlate("");
  };
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const dateFormatted = `${day < 10 ? "0" + day : day}-${
    month < 10 ? "0" + month : month
  }-${year}`;

  // const ownership = language === 0 ? "OWNER" : "NOT OWNER";
  const plateNumber = leftPlate + rightPlate.replace(/\s/g, "");
  const SendForm = () => {
    if (
      current.length &&
      model &&
      dateChosen &&
      passport.length > 5 &&
      plateNumber.length > 7
    ) {
      setLoading(true);
      apiService
        .addAuto(token, {
          currentMileage: current,
          makeId: manufacturer.id,
          manufacturedMonth: month,
          manufacturedYear: year,
          modelId: model.id,
          modificationId: modify ? modify.id : null,
          ownership: "OWNER",
          plateNumber: plateNumber,
          vehicleTypeId: 27, // 27 - sedan
        })
        .then((value) => {
          setLoading(false);
          if (value.statusCode == 200) {
            getGarage(token);
            navigation.goBack();
          } else Alert.alert(value.message);
        });
    } else Alert.alert("Нужно заполнить все поля!");
  };

  const MoveInput = ({ title, choosen, method, requestId }) => {
    return (
      <View style={{ position: "relative" }}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Choice", {
              title: title,
              setMethod: method,
              requestId: !requestId ? false : requestId,
            })
          }
        >
          <Text
            style={[
              styles.authorizationInput,
              {
                lineHeight: 50,
                color: choosen == title ? "#999999" : "#444",
              },
            ]}
          >
            {choosen ?? title}
          </Text>
          <ArrowRight style={styles.icon} />
        </TouchableOpacity>
      </View>
    );
  };
  const LeftBlock = () => {
    return (
      <View>
        <MoveInput
          title="Производитель"
          choosen={manufacturer ? manufacturer.name : "Производитель"}
          method={changeManufacturer}
        />
        {manufacturer !== 0 && (
          <MoveInput
            title="Модель"
            choosen={model ? model.name : "Модель"}
            requestId={manufacturer.id}
            method={changeModel}
          />
        )}
        {model !== 0 && (
          <MoveInput
            title="Модификация"
            choosen={modify ? modify.name : "Модификация"}
            requestId={model.id}
            method={setModify}
          />
        )}
        <View style={{ position: "relative" }}>
          <TouchableOpacity onPress={() => setShow(true)}>
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !dateChosen ? "#999" : "#444",
                  lineHeight: 50,
                },
              ]}
            >
              {dateChosen ? dateFormatted : "Год выпуска"}
            </Text>
            <Calendar style={styles.icon} />
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Текущий пробег"
            keyboardType={`numeric`}
            placeholderTextColor="#999999"
            value={current}
            onChangeText={(text) => setCurrent(text)}
          />
          <Metr style={styles.icon} />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Номер тех паспорта"
            placeholderTextColor="#999999"
            value={passport}
            onChangeText={(text) => setPassport(text)}
          />
        </View>
        {/* <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={{ flex: 1, height: 50 }}>
            <TextInput returnKeyType={"done"}
              style={[styles.authorizationInput]}
              placeholder="Номер OBD платформы"
              placeholderTextColor="#999999"
              value={confirmation}
            />
          </View>
          <View>
            <TouchableOpacity style={styles.barcode}>
              <Barcode />
            </TouchableOpacity>
          </View>
        </View> */}
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <View style={styles.container}>
      <HeaderBack
        type="white"
        button={true}
        title="Добавление авто"
        navigation={navigation}
      />
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          is24Hour={true}
          onChange={onChange}
        />
      )}
      <ScrollView style={{ flex: 1, backgroundColor: "#F6F6F6" }}>
        <View style={{ height: 20 }} />
        <View style={{ alignItems: "center" }}>
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <View
                style={{
                  flexDirection: "row",
                  paddingBottom: 10,
                  justifyContent: "center",
                }}
              >
                <TouchableOpacity
                  onPress={() => changeLang(0)}
                  style={[
                    styles.leftBlock,
                    styles.block,
                    !language ? styles.activeBlock : styles.inactiveBlock,
                  ]}
                >
                  <Text
                    style={[
                      !language ? styles.activeText : styles.inactiveText,
                      { fontFamily: global.fonts.regular },
                    ]}
                  >
                    Физ. номер
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => changeLang(1)}
                  style={[
                    styles.rightBlock,
                    styles.block,
                    language ? styles.activeBlock : styles.inactiveBlock,
                  ]}
                >
                  <Text
                    style={[
                      language ? styles.activeText : styles.inactiveText,
                      { fontFamily: global.fonts.regular },
                    ]}
                  >
                    Гос. номер
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.carNumber}>
                <View
                  style={{
                    flex: 2,
                    alignItems: "center",
                    height: "100%",
                    justifyContent: "center",
                    borderRightColor: global.colors.mainColor,
                    borderRightWidth: 2,
                  }}
                >
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.carNumberInput}
                    placeholder="--"
                    placeholderTextColor="#444444"
                    value={leftPlate}
                    returnKeyType={"next"}
                    keyboardType="numeric"
                    onChangeText={(text) => {
                      if (text.length < 3) {
                        setLeftPlate(text);
                      }
                      if (text.length == 2) {
                        secondInput.current.focus();
                      }
                    }}
                    // onSubmitEditing={() => passwordRef.focus()}
                  />
                </View>
                <View
                  style={{
                    flex: 6,
                    alignItems: "center",
                    height: "100%",
                    justifyContent: "center",
                  }}
                >
                  <TextInputMask
                    returnKeyType={"done"}
                    style={styles.carNumberInput}
                    // ref={passwordRef}
                    type={`custom`}
                    refInput={(input) => {
                      secondInput.current = input;
                    }}
                    options={{
                      mask: !language ? "A 999 AA" : "999 AAA",
                    }}
                    placeholder={!language ? "- --- --" : "--- ---"}
                    placeholderTextColor="#444444"
                    keyboardType={`default`}
                    value={rightPlate}
                    onChangeText={(text) =>
                      text.length < 10 && setRightPlate(text.toUpperCase())
                    }
                  />
                </View>
                <View
                  style={{
                    flex: 2,
                    paddingTop: 5,
                    height: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Uzbekistan />
                  <Text
                    style={{
                      fontSize: 13,
                      fontFamily: global.fonts.medium,
                      textAlign: "center",
                    }}
                  >
                    UZ
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={[styles.authorization, { marginTop: 20 }]}>
            <View style={styles.authorizationBlock}>{LeftBlock()}</View>
          </View>
          <View style={{ width: "100%", paddingHorizontal: 20 }}>
            <TouchableOpacity
              onPress={() => SendForm()}
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor },
              ]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Добавить авто
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

export default AddAutoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  carNumber: {
    borderWidth: 2,
    borderColor: global.colors.mainColor,
    borderRadius: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 10,
    height: 60,
  },
  carNumberInput: {
    fontSize: 36,
    fontFamily: global.fonts.medium,
    color: "#444444",
    textAlign: "center",
    alignItems: "center",
    width: "100%",
    justifyContent: "center",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  gazShadow: {
    backgroundColor: "white",
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.05)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  inactiveGaz: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "white",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    lineHeight: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "100%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
  barcode: {
    height: 50,
    width: 50,
    marginLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    borderWidth: 1,
    borderRadius: 25,
  },
});
