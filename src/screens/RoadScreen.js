import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Metr from "../../assets/kilometr.svg";
import Calendar from "../../assets/calendar-time.svg";
import Points from "../../assets/points.svg";
import Location from "../../assets/location.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import Context from "../components/Context";
import SuccessModal from "../components/SuccessModal";
import RequestLoadingScreen from "./RequestLoadingScreen";
import {formatDate, parseDate} from "../utils/date";

function RoadScreen({ navigation, route }) {
  const { apiService, token, phone } = useContext(Context);
  const { data, auto } = route.params
    ? route.params
    : { data: null, auto: null };
  const [vehicleId, setVehicle] = useState(0);
  const [number, setNumber] = useState("");
  const [note, setNote] = useState("");
  const [origin, setOrigin] = useState("");
  const [destination, setDestination] = useState("");
  const [pointA, setPointA] = useState({ lat: 0, lng: 0 });
  const [pointB, setPointB] = useState({ lat: 0, lng: 0 });
  const [odometer, setOdometr] = useState("");
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [distanceLoading, setDistanceLoading] = useState(false);
  const [distance, setDistance] = useState(0);
  useEffect(() => {
    if (data) {
      setLoading(true);
      apiService
        .getResources("/events/trip/" + data.id, token)
        .then((value) => {
          callDistance(value.data.startPoint, value.data.endPoint);
          ChooseAuto(auto.id, auto.plateNumber);
          setNote(value.data.note);
          setDate(new Date(value.data.tripEndDate));
          setDateChosen(true);
          setPointA(value.data.startPoint);
          setPointB(value.data.endPoint);
          setOdometr(value.data.endOdometerMileage.toString());
          setLoading(false);
        });
    }
  }, []);
  const onChangeDate = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setShowTime(true);
    }
  };
  const onChangeTime = (event, selectedTime) => {
    setShowTime(false);
    if (selectedTime) {
      setDateChosen(true);
      setDate(selectedTime);
    }
  };
  const setPositionA = (value) => {
    setPointA(value);
    if (pointB.lat != 0) callDistance(value, pointB);
  };
  const setPositionB = (value) => {
    setPointB(value);
    if (pointA.lat != 0) callDistance(pointA, value);
  };
  const callDistance = (orig, dist) => {
    const origin = orig.lat + "," + orig.lng;
    const destin = dist.lat + "," + dist.lng;
    setDistanceLoading(true);
    apiService
      .getDistance(origin, destin)
      .then((res) => {
        setDistanceLoading(false);
        if (res.status == "OK") {
          setDestination(res.destination_addresses[0]);
          setOrigin(res.origin_addresses[0]);
          const data = res.rows[0].elements[0].distance.value;
          setDistance(Math.ceil(parseInt(data) / 1000, -1));
        }
      })
      .catch((e) => {
        setDistanceLoading(false);
      });
  };
  const chosenDateTime = formatDate(date);
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const saveEvent = () => {
    if (
      odometer &&
      parseInt(distance) !== 0 &&
      pointA.long !== 0 &&
      pointB.long !== 0 &&
      dateChosen
    ) {
      setLoading(true);
      const body = {
        // odometer: parseInt(odometer),
        startOdometerMileage: parseInt(odometer),
        endOdometerMileage: parseInt(odometer) + parseInt(distance),
        startPoint: pointA,
        endPoint: pointB,
        tripDate: parseDate(date),
        tripEndDate: parseDate(date),
        vehicleId: parseInt(vehicleId),
        note: note,
      };
      if (data) {
        body.id = data.id;
        apiService
          .updateForm("/events/trip", token, body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
            }
          })
          .catch((e) => {
            setLoading(false);
          });
      } else {
        apiService
          .addEvent(token, "trip", body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
            }
          })
          .catch((e) => {
            setLoading(false);
          });
      }
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };

  const LeftBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("ChooseAuto", {
                method: ChooseAuto,
              })
            }
          >
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !vehicleId ? "#999999" : "#444444",
                },
              ]}
            >
              {!vehicleId ? "Выберите авто" : number}
            </Text>
            <ArrowRight pointerEvents={`none`} style={styles.icon} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ marginRight: 15 }}>
            <Points height={84} width={24} />
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ position: "relative" }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddressMap", {
                    title: "Пункт А",
                    setPosition: (value) => setPositionA(value),
                    showAddress: false,
                  })
                }
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color:
                        pointA.lng === 0 && origin.length == 0
                          ? "#999999"
                          : global.colors.success,
                      paddingRight: 40,
                    },
                  ]}
                >
                  {pointA.lat === 0
                    ? "Пункт А"
                    : origin
                    ? `${origin.slice(0, 20)}${origin.length > 20 && "..."}`
                    : "Выберите Пункт B"}
                </Text>
              </TouchableOpacity>
              <View pointerEvents={`none`} style={styles.icon}>
                <Location />
              </View>
            </View>
            <View style={{ position: "relative" }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddressMap", {
                    title: "Пункт B",
                    setPosition: (value) => setPositionB(value),
                    showAddress: false,
                  })
                }
                // disabled={pointA.lat != 0}
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color:
                        pointB.lng === 0 ? "#999999" : global.colors.success,
                      paddingRight: 40,
                    },
                  ]}
                >
                  {pointB.lat === 0
                    ? "Пункт B"
                    : destination
                    ? `${destination.slice(0, 20)}${
                        destination.length > 20 && "..."
                      }`
                    : "Выберите Пункт А"}
                </Text>
              </TouchableOpacity>
              <View pointerEvents={`none`} style={styles.icon}>
                <Location />
              </View>
            </View>
          </View>
        </View>
        <View style={{ position: "relative", flexDirection: "row" }}>
          <View>
            <View
              style={[
                styles.authorizationInput,
                styles.activeBlock,
                {
                  padding: 0,
                  width: "auto",
                  marginRight: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  height: 50,
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 18,
                  color: "white",
                  fontFamily: global.fonts.regular,
                }}
              >
                {distance + "KM"}
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  color: "white",
                  fontFamily: global.fonts.regular,
                }}
              >
                расстояние
              </Text>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <TextInput
              returnKeyType={"done"}
              style={styles.authorizationInput}
              placeholder="Пробег"
              placeholderTextColor="#999999"
              keyboardType={"numeric"}
              value={odometer}
              onChangeText={(text) => setOdometr(text)}
            />
            <View pointerEvents={`none`} style={styles.icon}>
              <Metr />
            </View>
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity onPress={() => setShow(true)}>
            <Text
              style={[
                styles.authorizationInput,
                {
                  lineHeight: 50,
                  color: !dateChosen ? "#999" : "#444",
                },
              ]}
            >
              {dateChosen ? chosenDateTime : "Дата и время"}
            </Text>
            <Calendar pointerEvents={`none`} style={styles.icon} />
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={note}
            onChangeText={(text) => setNote(text)}
          />
        </View>
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <SuccessModal showModal={showModal} setShowModal={setShowModal} />
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Поездка"
          navigation={navigation}
        />
        {show && (
          <DateTimePicker
            timeZoneOffsetInMinutes={0}
            value={date}
            mode={`date`}
            is24Hour={true}
            display="default"
            onChange={onChangeDate}
          />
        )}
        {showTime && (
          <DateTimePicker
            timeZoneOffsetInMinutes={0}
            value={date}
            mode={`time`}
            is24Hour={true}
            display="default"
            onChange={onChangeTime}
          />
        )}
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>{LeftBlock()}</View>
            </View>
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor },
              ]}
              onPress={() => saveEvent()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Сохранить
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default RoadScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
    backgroundColor: "white",
  },
});
