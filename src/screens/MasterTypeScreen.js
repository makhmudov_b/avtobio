import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-red.svg";
import ArrowRight from "../../assets/arrow-right-red.svg";
import Edit from "../../assets/edit-red.svg";
import Calendar from "../../assets/calendar-time-red.svg";
import { SafeAreaProvider } from "react-native-safe-area-context";
import SafeAreaView from "react-native-safe-area-view";
import Constants from "expo-constants";
import HeaderBackRed from "../components/HeaderBackRed";

function MasterTypeScreen({ navigation }) {
  const [confirmation, onChangeConfirmation] = useState("");
  const LeftBlock = () => {
    return (
      <View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              Электрик
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              Моторист
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              Ходовик
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#444444" },
              ]}
            >
              Кузовщик
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBackRed
          type="white"
          button={true}
          title="Тип мастера"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <LeftBlock />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default MasterTypeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
    marginBottom: 10,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    textAlign: "center",
    lineHeight: 50,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 18,
  },
});
