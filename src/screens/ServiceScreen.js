import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import ArrowRight from "../../assets/arrow-right.svg";
import Calendar from "../../assets/calendar-time.svg";
import Location from "../../assets/location.svg";
import HeaderBack from "../components/HeaderBack";
import DateTimePicker from "@react-native-community/datetimepicker";
import Close from "../../assets/close-blue.svg";
import Metr from "../../assets/kilometr.svg";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import SuccessModal from "../components/SuccessModal";
import {formatDate, parseDate} from "../utils/date";

function ServiceScreen({ navigation, route }) {
  const { apiService, token, notification } = useContext(Context);
  const { data, auto } = route.params
    ? route.params
    : { data: null, auto: null };
  const [serviceId, setServiceId] = useState(null);
  const [serviceName, setServiceName] = useState(null);
  const [serviceList, setServiceList] = useState([]);
  const [vehicleId, setVehicle] = useState(0);
  const [number, setNumber] = useState("");
  const [note, setNote] = useState("");
  const [odometer, setOdometr] = useState("");
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [dateChosen, setDateChosen] = useState(false);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (data) {
      setLoading(true);
      apiService
        .getResources("/events/service/" + data.id, token)
        .then((value) => {
          ChooseAuto(auto.id, auto.plateNumber);
          setOdometr(value.data.odometer.toString());
          setNote(value.data.note);
          setDate(new Date(value.data.serviceDate));
          setDateChosen(true);
          setService(value.data.autoServiceName, value.data.autoServiceId);
          setServiceList(value.data.serviceItems);
          setLoading(false);
        });
    }
  }, []);
  const onChangeDate = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setShowTime(true);
    }
  };
  const setService = (name, id) => {
    setServiceName(name);
    setServiceId(id);
  };
  const onChangeTime = (event, selectedTime) => {
    setShowTime(false);
    if (selectedTime) {
      setDateChosen(true);
      setDate(selectedTime);
    }
  };
  const chosenDateTime = formatDate(date);
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const saveEvent = () => {
    let priceChecker = true;
    if (serviceList.length) {
      serviceList.forEach((serv) => {
        if (!serv.price) {
          priceChecker = false;
          return false;
        }
      });
    }

    if (
      odometer.length &&
      priceChecker &&
      serviceList.length &&
      serviceId &&
      dateChosen
    ) {
      setLoading(true);
      let total = 0;
      serviceList.forEach((e) => (total += parseInt(e.price)));
      const body = {
        note: note,
        serviceDate: parseDate(date),
        vehicleId: parseInt(vehicleId),
        autoServiceId: parseInt(serviceId),
        serviceItems: serviceList,
        total: total,
        odometer: odometer,
      };
      if (data) {
        body.id = data.id;
        apiService
          .updateForm("/events/service", token, body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
              setLoading(false);
            }
          });
      } else {
        apiService
          .addEvent(token, "service", body, body.vehicleId)
          .then((value) => {
            setLoading(false);
            if (value.statusCode === 200) {
              setShowModal(true);
              setTimeout(() => navigation.replace("Home"), 1000);
            } else {
              Alert.alert(value.message);
              setLoading(false);
            }
          });
      }
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const changeServicePrice = (value, key) => {
    let innerService = [...serviceList];
    innerService[key] = { ...innerService[key], price: value };
    setServiceList(innerService);
  };
  const removeServiceItem = (id) => {
    let removedServices = serviceList.filter((ser) => ser.id !== id);
    setServiceList(removedServices);
  };
  const serviceItem = (serv, key) => {
    return (
      <View key={key} style={[styles.authorization, { marginTop: 20 }]}>
        <View style={styles.authorizationBlock}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: 10,
            }}
          >
            <Text
              style={{
                color: "#444",
                fontSize: 18,
                fontFamily: global.fonts.regular,
              }}
            >
              {serv.serviceName}
            </Text>
            <TouchableOpacity onPress={() => removeServiceItem(serv.id)}>
              <Close />
            </TouchableOpacity>
          </View>
          <View style={{ position: "relative" }}>
            <TextInput
              returnKeyType={"done"}
              style={[styles.authorizationInput]}
              placeholder="Сумма расхода"
              keyboardType={"numeric"}
              placeholderTextColor="#999999"
              value={serv.price ? serv.price.toString() : serv.price}
              onChangeText={(text) => changeServicePrice(text, key)}
            />
            <View style={[styles.icon, { width: "auto" }]}>
              <Text
                style={{
                  fontSize: 18,
                  color: "#999999",
                  fontFamily: global.fonts.regular,
                }}
              >
                UZS
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`date`}
          is24Hour={true}
          display="default"
          onChange={onChangeDate}
        />
      )}
      {showTime && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={`time`}
          is24Hour={true}
          display="default"
          onChange={onChangeTime}
        />
      )}
      <SuccessModal showModal={showModal} setShowModal={setShowModal} />
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Сервис"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("ChooseAuto", {
                        method: ChooseAuto,
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !vehicleId ? "#999999" : "#444444",
                        },
                      ]}
                    >
                      {!vehicleId ? "Выберите авто" : number}
                    </Text>
                    <ArrowRight style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={{ position: "relative" }}>
                  <TouchableOpacity onPress={() => setShow(true)}>
                    <Text
                      style={[
                        styles.authorizationInput,
                        {
                          lineHeight: 50,
                          color: !dateChosen ? "#999" : "#444",
                        },
                      ]}
                    >
                      {dateChosen ? chosenDateTime : "Дата и время"}
                    </Text>
                    <Calendar style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View>
                  <View style={{ position: "relative" }}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate("AddressChoice", {
                          title: "Выберити сервис",
                          method: setService,
                          type: "service",
                        })
                      }
                    >
                      <Text
                        style={[
                          styles.authorizationInput,
                          {
                            lineHeight: 50,
                            color: !serviceName ? "#999" : "#444",
                          },
                        ]}
                      >
                        {!serviceName ? "Адрес мастерской" : serviceName}
                      </Text>
                      <Location style={styles.icon} />
                    </TouchableOpacity>
                  </View>
                  <View style={{ position: "relative" }}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate("Reason", {
                          title: "Причина визита",
                          method: setServiceList,
                        })
                      }
                    >
                      <Text
                        style={[
                          styles.authorizationInput,
                          {
                            lineHeight: 50,
                            color: !serviceList.length
                              ? "#999999"
                              : global.colors.success,
                          },
                        ]}
                      >
                        {serviceList.length
                          ? "Выбрали из списка"
                          : "Список услуг"}
                      </Text>
                      <ArrowRight style={styles.icon} />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Пробег"
                    placeholderTextColor="#999999"
                    keyboardType={"numeric"}
                    value={odometer}
                    onChangeText={(text) => setOdometr(text)}
                  />
                  <View style={styles.icon}>
                    <Metr />
                  </View>
                </View>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Примечание ..."
                    placeholderTextColor="#999999"
                    value={note}
                    onChangeText={(text) => setNote(text)}
                  />
                </View>
              </View>
            </View>
            {serviceList.map((serv, key) => {
              return serviceItem(serv, key);
            })}
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor, marginBottom: 30 },
              ]}
              onPress={() => saveEvent()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Сохранить
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default ServiceScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});
