import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import ShowMarker from "../../assets/show-marker.svg";
import ChosenMarker from "../../assets/chosen-marker.svg";
import Service from "../../assets/service.svg";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Search from "../../assets/search.svg";
import Heart from "../../assets/heart.svg";
import Distance from "../../assets/distance.svg";
import Share from "../../assets/share.svg";
import Overlay from "../../assets/overlay.svg";
import Context from "../components/Context";
import Oil from "../../assets/oil.svg";
import EmptyResult from "../../assets/no-search.svg";
import Fuel from "../../assets/fuel.svg";
import CarWash from "../../assets/car-wash.svg";
import Evakuator from "../../assets/evakuator.svg";
import CustomStatusBar from "../components/CustomStatusBar";
import RequestLoadingScreen from "./RequestLoadingScreen";
import Close from "../../assets/close.svg";
import XBlue from "../../assets/x-blue.svg";
import Constants from "expo-constants";

export default class MapScreen extends Component {
  static contextType = Context;
  constructor() {
    super();
    this.state = {
      region: {
        latitude: 41.26465,
        longitude: 69.21627,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05,
      },
      markers: [],
      chosen: -1,
      show: false,
      service: 0,
      search: "",
      openSearch: false,
      searchWaiting: true,
      mainTypes: [],
      loading: true,
    };
  }
  componentDidMount() {
    this.context.getLocation();
    this.getList();
  }
  onRegionChange(region) {
    this.setState({
      region: {
        latitude: region.latitude,
        longitude: region.longitude,
      },
    });
  }
  handleSearch(search) {
    this.setState({ search: search });
  }
  getList() {
    this.context.apiService
      .getReferences(
        "/backend/v1/reference/choose?parentCode=_SERVICE_PROVIDER_CATEGORIES&",
        this.context.token
      )
      .then((value) => {
        const data = typeof value.data != "undefined" ? value.data : [];
        if (data) {
          this.setState({ mainTypes: data, loading: false });
        } else {
          this.setState({ loading: false, mainTypes: [] });
        }
      })
      .catch((e) => this.props.navigation.goBack());
  }
  onEditEnd() {
    const location = this.context.location;
    const body = {
      searchKey: this.state.search,
      ...(!!location && {
        lat: location.coords.latitude,
        lng: location.coords.longitude,
      }),
    };
    if (this.state.search.length) {
      if (!this.state.loading) this.setState({ loading: true });
      this.context.apiService
        .getResources(
          "/locator?" + new URLSearchParams(body),
          this.context.token,
          this.context.vehicleId
        )
        .then((value) => {
          const data = typeof value.data != "undefined" ? value.data : [];
          if (!data.length) Alert.alert("Ничего не найдено!");
          this.setState({
            loading: false,
            markers: data,
            show: false,
            chosen: -1,
          });
        })
        .catch((e) => {
          Alert.alert("Что-то пошло не так!");
          this.setState({
            loading: false,
            markers: [],
            show: false,
            chosen: -1,
          });
        });
    }
  }
  setSearch(boolValue) {
    this.setState({ openSearch: boolValue });
  }
  setCamerView(markerPosition, key) {
    this.setState({
      region: {
        latitude: markerPosition.latitude,
        longitude: markerPosition.longitude,
        latitudeDelta: this.state.region.latitudeDelta,
        longitudeDelta: this.state.region.longitudeDelta,
      },
      chosen: key,
      show: true,
    });
  }
  getListById(type) {
    const { id } = type;
    this.setState({ loading: true, service: id });
    const location = this.context.location;
    const body = {
      categoryCode: type.code,
      ...(!!location && {
        lat: location.coords.latitude,
        lng: location.coords.longitude,
      }),
    };
    this.context.apiService
      .getResources(
        "/locator?" + new URLSearchParams(body),
        this.context.token,
        this.context.currentCar.id
      )
      .then((value) => {
        const data = typeof value.data != "undefined" ? value.data : [];
        this.setState({
          loading: false,
          markers: data,
          show: false,
          chosen: -1,
        });
      })
      .catch((e) => {
        this.setState({ loading: false, markers: [], show: false, chosen: -1 });
      });
  }
  findServiceType = (code) => {
    if (code == "CAR_WASH")
      return (
        <CarWash
          height={20}
          style={{
            resizeMode: "contain",
          }}
        />
      );
    if (code == "AUTO_SERVICE")
      return (
        <Service
          height={20}
          style={{
            resizeMode: "contain",
          }}
        />
      );
    if (code == "GAS_STATION")
      return (
        <Fuel
          height={20}
          style={{
            resizeMode: "contain",
          }}
        />
      );
    if (code == "OIL_STATION")
      return (
        <Oil
          height={20}
          style={{
            resizeMode: "contain",
          }}
        />
      );
    if (code == "EVAСUATOR")
      return (
        <Evakuator
          height={20}
          style={{
            resizeMode: "contain",
          }}
        />
      );
    if (
      code != "CAR_WASH" &&
      code != "AUTO_SERVICE" &&
      code != "GAS_STATION" &&
      code != "OIL_STATION" &&
      code != "EVAСUATOR"
    )
      return (
        <Heart
          height={20}
          style={{
            resizeMode: "contain",
          }}
        />
      );
  };
  render() {
    const { navigation } = this.props;
    const now = new Date();
    const weekDay = now.getDay();
    if (this.state.loading) return <RequestLoadingScreen />;
    const currentDay = () => {
      if (this.state.chosen == -1 && !this.state.markers.length) return false;
      if (typeof this.state.markers[this.state.chosen] === "undefined")
        return false;
      return this.state.markers[this.state.chosen].timeslots[weekDay];
    };
    const isOpen = (item = null) => {
      const getCurrent = item ? item.timeslots : currentDay();
      if (!getCurrent) return "Закрыто";
      if (
        now.getHours() > parseInt(getCurrent.startTime.slice(0, 2)) &&
        now.getHours() < parseInt(getCurrent.endTime.slice(0, 2))
      ) {
        return "Открыто";
      } else {
        return "Закрыто";
      }
    };
    return (
      <>
        <CustomStatusBar type={`white`} />
        <KeyboardAvoidingView behavior={"padding"} style={styles.container}>
          <View
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              zIndex: 10,
            }}
          >
            <View
              style={{
                paddingHorizontal: 20,
                paddingTop: 20,
                // paddingBottom: 40,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Arrow />
                </TouchableOpacity>
                <View style={{ flex: 1, position: "relative" }}>
                  {this.state.openSearch && (
                    <TextInput
                      returnKeyType={"search"}
                      style={{
                        alignSelf: "center",
                        width: "80%",
                        backgroundColor: "white",
                        borderRadius: 50,
                        paddingHorizontal: 20,
                        height: 40,
                        shadowColor: "rgba(0,0,0,.15)",
                        shadowOffset: {
                          width: 0,
                          height: 2,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 3.84,
                        elevation: 5,
                        fontSize: 18,
                        fontFamily: global.fonts.regular,
                        color: "#444",
                        position: "absolute",
                        top: -5,
                        zIndex: 20,
                      }}
                      value={this.state.search}
                      onChangeText={(text) => this.handleSearch(text)}
                      onSubmitEditing={() => this.onEditEnd()}
                    />
                  )}
                  <Text
                    style={{
                      fontSize: 24,
                      textAlign: "center",
                      fontFamily: global.fonts.medium,
                      color: "#444444",
                    }}
                  >
                    Локатор услуг
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.setSearch(!this.state.openSearch)}
                >
                  {this.state.openSearch ? (
                    <XBlue height={30} style={{ resizeMode: "contain" }} />
                  ) : (
                    <Search height={30} style={{ resizeMode: "contain" }} />
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {this.state.openSearch == 1 &&
            this.state.search.length > 0 &&
            this.state.show == 0 &&
            this.state.markers.length > 0 && (
              <View style={styles.searchBlock}>
                <View
                  style={[
                    {
                      backgroundColor: "white",
                      paddingVertical: 10,
                      borderRadius: 20,
                      flex: 1,
                    },
                    styles.shadow,
                  ]}
                >
                  <ScrollView
                    contentContainerStyle={[
                      {
                        backgroundColor: "white",
                        borderRadius: 20,
                      },
                    ]}
                  >
                    {this.state.markers.map((item, key) => {
                      return (
                        <TouchableOpacity
                          key={key}
                          onPress={() =>
                            item.categoryCode != "AUTO_SERVICE"
                              ? navigation.navigate("ShowLocator", {
                                  service: item,
                                })
                              : navigation.navigate("ServiceProfile", {
                                  service: item,
                                  type: item.categoryCode,
                                })
                          }
                          style={{
                            width: "94%",
                            alignSelf: "center",
                            backgroundColor: "white",
                            height: 60,
                            borderRadius: 25,
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            paddingHorizontal: 20,
                            shadowColor: "rgba(0,0,0,.15)",
                            shadowOffset: {
                              width: 0,
                              height: 0,
                            },
                            shadowOpacity: 0.6,
                            shadowRadius: 3.84,
                            elevation: 2,
                            marginLeft: 5,
                            marginRight: 5,
                            marginTop: 10,
                          }}
                        >
                          <View style={styles.icon}>
                            {this.findServiceType(item.categoryCode)}
                          </View>
                          <View style={{ flex: 1, paddingLeft: 10 }}>
                            <Text
                              style={{
                                color: "#444444",
                                marginBottom: 5,
                                fontSize: 12,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              {item.name}
                            </Text>
                            <Text
                              style={{
                                color: "#999999",
                                fontSize: 9,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              {typeof item.coordinate !== "undefined"
                                ? item.coordinate.address
                                : null}
                            </Text>
                          </View>
                          <View>
                            <Text
                              style={{
                                color: global.colors.success,
                                marginBottom: 5,
                                fontSize: 12,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              {isOpen(item)}
                            </Text>
                            <Text
                              style={{
                                color: "#999999",
                                fontSize: 9,
                                fontFamily: global.fonts.regular,
                              }}
                            >
                              {Math.ceil(item.distance / 100) / 10} KM
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                    <View height={20} />
                  </ScrollView>
                </View>
              </View>
            )}
          {this.state.show ? (
            <View style={[styles.whiteBlock, { zIndex: 999, paddingTop: 10 }]}>
              <TouchableOpacity
                style={[
                  styles.roundBtn,
                  {
                    position: "absolute",
                    backgroundColor: global.colors.danger,
                    zIndex: 999,
                    right: 20,
                    top: -10,
                  },
                ]}
                onPress={() =>
                  this.setState({ show: false, chosen: -1, service: 1 })
                }
              >
                <Close width={15} resizeMode={"contain"} />
              </TouchableOpacity>
              <View
                style={[
                  {
                    backgroundColor: "white",
                    padding: 20,
                    borderRadius: 20,
                    flex: 1,
                  },
                  styles.shadow,
                ]}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <View style={styles.icon}>
                    {this.findServiceType(
                      this.state.markers[this.state.chosen].categoryCode
                    )}
                  </View>
                  <View style={{ flex: 1, marginLeft: 15 }}>
                    <Text
                      style={{
                        color: "#444444",
                        marginBottom: 5,
                        fontSize: 14,
                        fontFamily: global.fonts.medium,
                      }}
                    >
                      {this.state.markers[this.state.chosen].name}
                    </Text>
                    <Text
                      style={{ color: "#999999", marginBottom: 5, fontSize: 9 }}
                    >
                      {!!this.state.markers[this.state.chosen].coordinate
                        ? this.state.markers[this.state.chosen].coordinate
                            .address
                        : null}
                    </Text>
                    <Text style={{ color: "#999999", fontSize: 9 }}>
                      {!!currentDay()
                        ? `${currentDay().startTime} - ${currentDay().endTime}`
                        : "00:00 - 00:00"}
                    </Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={{
                        textAlign: "right",
                        color: "#444444",
                        marginBottom: 5,
                        fontSize: 14,
                        fontFamily: global.fonts.medium,
                      }}
                    >
                      {Math.ceil(
                        this.state.markers[this.state.chosen].distance / 100
                      ) / 10}{" "}
                      KM
                    </Text>
                    <Text
                      style={{
                        textAlign: "right",
                        color: global.colors.success,
                        marginBottom: 5,
                        fontSize: 9,
                      }}
                    >
                      {isOpen()}
                    </Text>
                    {/* <Text
                      style={{
                        textAlign: "right",
                        color: "#999999",
                        fontSize: 9,
                      }}
                    >
                      Без выходных
                    </Text> */}
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 15,
                    alignItems: "center",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 15,
                      alignItems: "center",
                      justifyContent: "space-evenly",
                      marginBottom: 10,
                    }}
                  >
                    <TouchableOpacity
                      style={[
                        styles.bigButton,
                        {
                          backgroundColor: "white",
                          shadowColor: "rgba(0,0,0,.15)",
                          shadowOffset: {
                            width: 0,
                            height: 2,
                          },
                          shadowOpacity: 0.1,
                          shadowRadius: 3.84,
                          elevation: 2,
                        },
                      ]}
                      onPress={() =>
                        this.state.markers[this.state.chosen].categoryCode !=
                        "AUTO_SERVICE"
                          ? navigation.navigate("ShowLocator", {
                              service: this.state.markers[this.state.chosen],
                            })
                          : navigation.navigate("ServiceProfile", {
                              service: this.state.markers[this.state.chosen],
                              type: this.state.markers[this.state.chosen]
                                .categoryCode,
                            })
                      }
                    >
                      <Text
                        style={{
                          color: "#444",
                          fontSize: 14,
                          fontFamily: global.fonts.regular,
                        }}
                      >
                        Подробнее
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.whiteBlock}>
              <View
                style={[
                  {
                    backgroundColor: "white",
                    padding: 20,
                    borderRadius: 20,
                    flex: 1,
                  },
                  styles.shadow,
                ]}
              >
                <View
                  style={{
                    flexDirection: "row",
                    flexWrap: "wrap",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  {this.state.mainTypes.map((type, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() => this.getListById(type)}
                        style={styles.serviceButton}
                      >
                        <View style={styles.button}>
                          {type.code == "CAR_WASH" && <CarWash width={24} />}
                          {type.code == "AUTO_SERVICE" && (
                            <Service width={24} />
                          )}
                          {type.code == "GAS_STATION" && <Fuel width={24} />}
                          {type.code == "OIL_STATION" && <Oil width={24} />}
                          {type.code == "EVAСUATOR" && <Evakuator width={24} />}
                          {type.code != "CAR_WASH" &&
                            type.code != "AUTO_SERVICE" &&
                            type.code != "GAS_STATION" &&
                            type.code != "OIL_STATION" &&
                            type.code != "EVAСUATOR" && <Heart width={24} />}
                        </View>
                        <Text
                          style={{
                            textAlign: "center",
                            fontSize: 10,
                            fontFamily: global.fonts.medium,
                            marginTop: 10,
                          }}
                        >
                          {type.name}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            </View>
          )}
          <MapView
            initialRegion={{
              latitude: 41.26465,
              longitude: 69.21627,
              latitudeDelta: 0.4,
              longitudeDelta: 0.4,
            }}
            // onRegionChangeComplete={(region) => this.onRegionChange(region)}
            // provider={MapView.PROVIDER_GOOGLE}
            style={styles.mapStyle}
            showsUserLocation={true}
          >
            {this.state.service > 0
              ? this.state.markers.map((marker, key) => {
                  return (
                    <Marker
                      coordinate={{
                        latitude: Number(marker.coordinate.lat),
                        longitude: Number(marker.coordinate.lng),
                      }}
                      key={key}
                      onPress={() => this.setCamerView(marker, key)}
                    >
                      <View
                        style={
                          key === this.state.chosen ? styles.none : styles.show
                        }
                      >
                        <View style={{ position: "relative" }}>
                          <View
                            style={{
                              position: "absolute",
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 0,
                              alignItems: "center",
                              justifyContent: "center",
                              zIndex: 10,
                            }}
                          >
                            {this.findServiceType(marker.categoryCode)}
                          </View>
                          <ChosenMarker />
                        </View>
                      </View>
                      <View
                        style={
                          key !== this.state.chosen ? styles.none : styles.show
                        }
                      >
                        <View style={{ position: "relative" }}>
                          <View
                            style={{
                              position: "absolute",
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 10,
                              alignItems: "center",
                              justifyContent: "center",
                              zIndex: 10,
                            }}
                          >
                            {this.findServiceType(marker.categoryCode)}
                          </View>
                          <ShowMarker />
                        </View>
                      </View>
                    </Marker>
                  );
                })
              : null}
          </MapView>
          <View pointerEvents={"none"} style={styles.overlay}>
            <Overlay
              width={global.strings.width}
              height={global.strings.height}
              style={{ transform: [{ scale: 1.3 }] }}
            />
          </View>
        </KeyboardAvoidingView>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // width: global.strings.width,
    // height: global.strings.height,
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    fontSize: 18,
    color: "#444444",
    backgroundColor: "white",
    borderRadius: 50,
    width: 225,
    height: 40,
    paddingLeft: 10,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  whiteBlock: {
    zIndex: 20,
    position: "absolute",
    bottom: 20,
    width: global.strings.width,
    paddingHorizontal: 20,
  },
  searchBlock: {
    zIndex: 90,
    position: "absolute",
    top: Constants.statusBarHeight + 50,
    width: global.strings.width,
    bottom: 20,
    paddingHorizontal: 20,
  },
  serviceButton: {
    alignItems: "center",
    justifyContent: "center",
    width: 90,
    height: 90,
    backgroundColor: "white",
    borderRadius: 20,
    marginBottom: 10,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  mapStyle: {
    width: global.strings.width,
    height: global.strings.height,
    // position: "relative",
    position: "absolute",
    // top:0,
    // left:0,
    // right:0,
    // bottom:0
  },
  shadow: {
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.6,
    shadowRadius: 3.84,
    elevation: 2,
  },
  overlay: {
    position: "absolute",
    top: 0,
  },
  //   circle: {
  //     backgroundColor: 'black',
  //     borderRadius: 100,
  //     alignSelf: 'stretch',
  //     width:400,
  //     height:300,
  //   },
  none: {
    display: "none",
    overflow: "hidden",
    width: 0,
    height: 0,
  },
  show: {
    width: 90,
    height: 90,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.mainColor,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  roundBtn: {
    width: 40,
    height: 40,
    backgroundColor: "white",
    borderRadius: 40,
    shadowColor: "rgba(0,0,0,.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.65,
    shadowRadius: 0.1,
    elevation: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  bigButton: {
    // width: 140,
    flex: 1,
    height: 50,
    backgroundColor: global.colors.mainColor,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
  },
});
