import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Image,
} from "react-native";
import global from "../../resources/global";
import CustomIcon from "../../assets/background.svg";
import Preview from "../../assets/preview.svg";
import CustomStatusBar from "../components/CustomStatusBar";

const properties = global.strings.width - 40;
let scrollViewRef = null;
function ReviewScreen({ navigation }) {
  const [screen, changeScreen] = useState(0);

  const setScreen = (value) => {
    if (scrollViewRef) {
      changeScreen(value);
      scrollViewRef.scrollTo({ x: value * properties, y: 0, animated: true });
    }
  };

  const getText = () => {
    if (screen === 0) return "Контроль нескольких авто одновременно";
    if (screen === 1)
      return "Экономия времени при вызове мастера в любую точку города";
    if (screen === 2) return "Своевременный уход за тех статус авто";
    if (screen === 3)
      return "Контроль всех видов оплат касающиеся авто включая штрафы ГУБДД";
    if (screen === 4)
      return "Контроль истечения срока документов авто без головной боли, таких как ОСАГО, доверенность и т.д";
  };
  const getAnimated = () => {
    if (screen == 0) {
      return (
        <Image
          source={global.images.screen1}
          style={{ height: properties, width: properties }}
        />
      );
    }
    if (screen == 1) {
      return (
        <Image
          source={global.images.screen2}
          style={{ height: properties, width: properties }}
        />
      );
    }
    if (screen == 2) {
      return (
        <Image
          source={global.images.screen3}
          style={{ height: properties, width: properties }}
        />
      );
    }
    if (screen == 3) {
      return (
        <Image
          source={global.images.screen4}
          style={{ height: properties, width: properties }}
        />
      );
    }
    if (screen == 4) {
      return (
        <Image
          source={global.images.screen5}
          style={{ height: properties, width: properties }}
        />
      );
    }
  };
  return (
    <>
      <CustomStatusBar type={global.colors.mainColor} />
      <View style={styles.container}>
        <View style={styles.topInfo}>
          <View style={{ paddingTop: 10 }}>
            <TouchableOpacity
              style={{ padding: 10 }}
              onPress={() => navigation.replace("Login")}
            >
              <Text style={styles.topInfoText}>Пропустить</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={[styles.absoluteBackground]}>
          <CustomIcon width={`90%`} height={`100%`} />
        </View>
        <View
          style={{
            flex: 5,
            alignItems: "center",
            paddingHorizontal: 20,
            justifyContent: "center",
          }}
        >
          <View
            style={{
              width: properties,
              height: properties,
              overflow: "hidden",
              backgroundColor: "white",
              borderRadius: 79,
              position: "relative",
            }}
          >
            <View
              style={{
                height: properties,
                width: properties,
                position: "absolute",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                zIndex: 20,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {getAnimated()}
            </View>
            <ScrollView
              scrollEnabled={false}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              ref={(item) => {
                scrollViewRef = item;
              }}
            >
              <Preview />
            </ScrollView>
          </View>
        </View>
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Text
            style={{
              textAlign: "center",
              color: "white",
              paddingHorizontal: 10,
              fontSize: 18,
              fontFamily: global.fonts.medium,
            }}
          >
            {getText()}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            alignItems: "flex-end",
            padding: 20,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View>
            {screen > 0 ? (
              <TouchableOpacity
                onPress={() => setScreen(screen - 1)}
                style={styles.button}
              >
                <Text style={styles.buttonText}>Назад</Text>
              </TouchableOpacity>
            ) : null}
          </View>
          <View>
            {screen < 4 ? (
              <TouchableOpacity onPress={() => setScreen(screen + 1)}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Далее</Text>
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={() => navigation.replace("Login")}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Начать</Text>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </>
  );
}

export default ReviewScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.mainColor,
    position: "relative",
  },
  slide: {
    alignItems: "center",
    justifyContent: "center",
  },
  topInfo: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  topInfoText: {
    color: "#FFFFFF",
    fontSize: 18,
    fontFamily: global.fonts.medium,
  },
  absoluteBackground: {
    paddingTop: 10,
    alignItems: "center",
    position: "absolute",
    bottom: `-5%`,
    width: `100%`,
    height: `100%`,
    left: 0,
    right: 0,
  },
  button: {
    backgroundColor: "white",
    borderRadius: 20,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    width: 75,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  buttonText: {
    color: global.colors.mainColor,
    fontSize: 14,
    fontFamily: global.fonts.regular,
  },
});
