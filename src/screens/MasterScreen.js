import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Constants from "expo-constants";
import Search from "../../assets/search.svg";
import MasterItem from "../components/MasterItem";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";

function MasterScreen({ navigation, route }) {
  const { token, apiService, currentCar } = useContext(Context);
  const { params } = route;
  const { category, method } = params;
  const [masters, setMasters] = useState([]);
  const [loading, setLoading] = useState(true);
  const getMasters = () => {
    apiService
      .getResources(
        `/booking/technician/search?categoryId=${category}&lat=69.4111&lng=46.546&sortType=DESC`,
        token,
        currentCar.id
      )
      .then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          if (value.data) {
            setMasters(value.data);
          }
        } else {
          Alert.alert(value.message);
        }
      });
  };
  useEffect(() => {
    getMasters();
  }, []);
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title="Выбрать мастера"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              {!loading ? (
                masters.map((master, key) => {
                  return (
                    <MasterItem master={master} method={method} key={key} />
                  );
                })
              ) : (
                <Image
                  source={global.images.loader}
                  style={{ height: 200, width: 200, alignSelf: "center" }}
                />
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default MasterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
});
