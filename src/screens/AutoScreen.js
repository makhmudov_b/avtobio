import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Document from "../../assets/documents.svg";
import Stats from "../../assets/car-stats.svg";
import AutoItem from "../components/AutoItem";
import Carousel from "react-native-snap-carousel";
import Context from "../components/Context";
import CarNumber from "../components/CarNumber";
import CustomStatusBar from "../components/CustomStatusBar";
import RequestLoadingScreen from "./RequestLoadingScreen";

function AutoScreen({ navigation, route }) {
  const { apiService, token, setAutos, autos } = useContext(Context);
  const { params } = route;
  const { vehicleId } = params;
  const [auto, setAuto] = useState(null);
  const [stats, setStats] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getAuto();
  }, []);
  const getAuto = () => {
    apiService
      .getResources("/vehicle/item/" + vehicleId, token)
      .then((value) => {
        if (value.statusCode === 200) {
          setAuto(value.data);
        } else {
          Alert.alert("Что-то пошло не так!");
        }
      })
      .then((val) => {
        getStats();
      });
  };
  const getStats = () => {
    apiService
      .getResources("/statistics/document-stats", token, vehicleId)
      .then((value) => {
        if (value.statusCode === 200) {
          setStats(value.data);
          setLoading(false);
        } else {
          Alert.alert("Что-то пошло не так!");
        }
      });
  };
  const deleteConfirm = () => {
    setLoading(true);
    apiService
      .deleteData("/vehicle/item/" + auto.id, token, auto.id)
      .then((value) => {
        if (value.statusCode == 200) {
          setAutos(autos.filter((item) => item.id != auto.id));
          navigation.goBack();
        } else {
          setLoading(false);
          Alert.alert("Что-то пошло не так!");
        }
      });
  };
  const deleteAuto = () => {
    Alert.alert("Вы уверены ? ", "", [
      {
        text: "Подтвердить",
        onPress: () => deleteConfirm(),
      },
      {
        text: "Отмена",
        style: "cancel",
      },
    ]);
  };
  const currentMileage = () => {
    const arr = auto ? auto.currentMileage.toString().split("") : [];
    const milageView = Array(9 - arr.length)
      .fill(0)
      .concat(arr);
    return milageView;
  };
  const width = global.strings.width;
  const SliderItem = ({ item, index }) => {
    return (
      <View
        style={{
          alignItems: "center",
          maxWidth: 280,
          justifyContent: "center",
          paddingBottom: 20,
          alignSelf: "center",
        }}
      >
        <CarNumber autoInfo={item.plateNumber} />
      </View>
    );
  };
  const generateInfo = (left, right) => {
    return (
      <View
        style={{
          alignItems: "center",
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: 5,
        }}
      >
        <View>
          <Text
            style={{
              fontSize: 10,
              color: "#444",
              fontFamily: global.fonts.regular,
            }}
          >
            {left}
          </Text>
        </View>
        <View>
          <Text
            style={{
              fontSize: 10,
              textAlign: "right",
              color: "#444",
              fontFamily: global.fonts.regular,
            }}
          >
            {right}
          </Text>
        </View>
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <CustomStatusBar type="white" />
      <View style={styles.container}>
        <View
          style={{
            paddingHorizontal: 20,
            backgroundColor: "white",
            width: "100%",
            paddingTop: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Arrow />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 24,
                color: "#444444",
                textAlign: "center",
                fontFamily: global.fonts.medium,
              }}
            >
              Информация
            </Text>
            <TouchableOpacity
              style={{ opacity: 0 }}
              // onPress={() => navigation.goBack()}
            >
              <Stats />
            </TouchableOpacity>
          </View>
          <View style={{ paddingTop: 20, alignItems: "center" }}>
            {auto ? (
              <Carousel
                layout={"stack"}
                data={[auto]}
                // loop={true}
                // loopClonesPerSide={entries.length}
                renderItem={SliderItem}
                sliderWidth={width}
                itemWidth={width}
              />
            ) : (
              <View
                style={{
                  alignItems: "center",
                  maxWidth: 280,
                  justifyContent: "center",
                  paddingBottom: 20,
                  alignSelf: "center",
                }}
              >
                <CarNumber autoInfo={"--------"} />
              </View>
            )}
          </View>
        </View>

        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View
              style={{
                backgroundColor: global.colors.mainColor,
                borderRadius: 10,
                marginTop: 20,
                justifyContent: "center",
                alignItems: "center",
                height: 100,
                width: "90%",
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                  fontFamily: global.fonts.medium,
                  paddingBottom: 10,
                }}
              >
                Общий пробег автомобиля
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  width: "100%",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                {currentMileage().map((item, key) => {
                  return (
                    <View
                      key={"metr" + key}
                      style={[
                        styles.probeg,

                        // (key == 8 || key == 7) && {
                        //   backgroundColor: global.colors.normal,
                        // },
                      ]}
                    >
                      <Text style={styles.probegText}>{item}</Text>
                    </View>
                  );
                })}
                {/* {auto !== null ? (
                currentMileage().map((item, key) => {
                  return (
                    <View
                      key={"metr" + key}
                      style={[
                        styles.probeg,
                        key == 6 ||
                          (key == 7 && {
                            backgroundColor: global.colors.normal,
                          }),
                      ]}
                    >
                      <Text style={styles.probegText}>{item}</Text>
                    </View>
                  );
                })
              ) : (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center",
                  }}
                >
                  <ActivityIndicator size="large" color={"white"} />
                </View>
              )} */}
                {/* <View style={styles.probeg}>
                <Text style={styles.probegText}>0</Text>
              </View> */}
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                {generateInfo("Производитель:", auto.makeName)}
                {generateInfo("Модель:", auto.modelName)}
                {generateInfo("Модификация:", auto.modificationName)}
                {generateInfo("Год выпуска:", auto.manufacturedYear)}
                {/* {generateInfo("Номер тех паспорта:", "AAA4567891")} */}
                {/* {generateInfo("Номер OBD платформы:", "Отсутствует")} */}
                <View height={10} />
                <TouchableOpacity
                  onPress={() => navigation.navigate("Document", { auto })}
                  style={[
                    styles.button,
                    { backgroundColor: global.colors.mainColor },
                  ]}
                >
                  <Document />
                  <Text
                    style={{
                      color: "white",
                      marginLeft: 15,
                      fontSize: 18,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    Мои документы
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => deleteAuto()}
                  style={[styles.button, { backgroundColor: "#C4C4C4" }]}
                >
                  <Text
                    style={{
                      color: "white",
                      marginLeft: 15,
                      fontSize: 18,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    Удалить авто
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                {stats.map((stat, key) => {
                  const getDays = parseInt(stat.days);
                  const days = 100 - (365 / 100) * getDays;
                  const status = () => {
                    if (getDays > 0 && getDays < 350) return "Истекает срок";
                    else if (getDays > 350 && getDays < 365) return "Актуально";
                    else return "Просрочен";
                  };
                  return (
                    <React.Fragment key={key}>
                      <AutoItem
                        title={stat.name}
                        type="passport"
                        info={`${getDays} Д.`}
                        status={status()}
                        progress={days > 100 ? 100 : days}
                      />
                      {key != stats.length - 1 && <View height={20} />}
                    </React.Fragment>
                  );
                })}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default AutoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    marginBottom: 20,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    fontFamily: global.fonts.regular,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    height: 60,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    borderRadius: 50,
    marginTop: 10,
    width: "100%",
  },
  probeg: {
    width: 30,
    height: 40,
    backgroundColor: "white",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  probegText: {
    fontSize: 36,
    color: "#444444",
    fontFamily: global.fonts.regular,
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 18,
  },
  barcode: {
    height: 60,
    width: 60,
    marginLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    borderWidth: 1,
    borderRadius: 25,
  },
});
