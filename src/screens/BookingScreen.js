import React, { useState, useEffect, useContext } from "react";
import {
  ScrollView,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Image,
  Alert,
} from "react-native";
import OrderItem from "../components/OrderItem";
import global from "../../resources/global";
import ArrowBack from "../../assets/arrow-blue.svg";
import Number from "../../assets/one-number.svg";
import Carousel from "react-native-snap-carousel";
import CustomStatusBar from "../components/CustomStatusBar";
import Context from "../components/Context";
import CarNumber from "../components/CarNumber";
import NoCar from "../../assets/no-car.svg";
import NoBooking from "../../assets/no-booking.svg";

function BookingScreen({ navigation }) {
  const {
    apiService,
    token,
    phone,
    setUser,
    currentCar,
    setCar,
    autos,
    notification,
  } = useContext(Context);
  const width = global.strings.width;
  const [loading, setLoading] = useState(false);
  const [events, setEvents] = useState([]);
  const [index, setIndex] = useState(0);
  useEffect(() => {
    if (notification) {
      if (typeof notification == "object") {
        getList(index);
      }
    }
  }, [notification]);
  useEffect(() => {
    getList(index);
  }, []);
  const getList = (key) => {
    setLoading(true);
    apiService
      .getResources("/booking/order", token, autos[key].id)
      .then((value) => {
        setLoading(false);
        if (value.statusCode == 200) {
          const data = value.data;
          if (typeof data !== "undefined") setEvents(value.data);
        } else {
          Alert.alert(value.message);
        }
      });
  };
  const setCurrentCar = (key) => {
    getList(key);
    setIndex(key);
  };
  const SliderItem = ({ item, key }) => {
    return (
      <View
        style={[
          {
            alignItems: "center",
            width: "100%",
            justifyContent: "center",
            paddingBottom: 20,
            alignSelf: "center",
            zIndex: 20,
          },
        ]}
      >
        <CarNumber autoInfo={item.plateNumber} />
      </View>
    );
  };
  const AddAutoButton = () => {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          style={[
            styles.button,
            { backgroundColor: global.colors.mainColor, marginBottom: 50 },
          ]}
          onPress={() => navigation.navigate("AddAuto")}
        >
          <Text
            style={{
              color: "white",
              fontSize: 18,
              fontFamily: global.fonts.regular,
            }}
          >
            Добавить авто
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  const AutoEmpty = () => {
    return (
      <View
        style={{
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <NoCar />
      </View>
    );
  };
  const EventEmpty = () => {
    return (
      <View
        style={{
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
          minHeight: global.strings.height / 2,
        }}
      >
        <NoBooking />
        <Text
          style={{
            color: "#444444",
            textAlign: "left",
            paddingTop: 20,
            fontSize: 18,
            fontFamily: global.fonts.medium,
          }}
        >
          Бронирований нет
        </Text>
      </View>
    );
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#F6F6F6",
      }}
    >
      <CustomStatusBar type={"white"} />
      <View
        style={{
          paddingHorizontal: 20,
          backgroundColor: "white",
          width: "100%",
          paddingTop: 20,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.1,
          shadowRadius: 4.65,
          elevation: 2,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <ArrowBack />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 24,
              color: "#444444",
              textAlign: "center",
              fontFamily: global.fonts.medium,
            }}
          >
            Мои брони
          </Text>
          <View style={{ opacity: 0 }}>
            <ArrowBack />
          </View>
        </View>
        <View style={{ marginTop: 20, marginBottom: 10, alignItems: "center" }}>
          {loading ? (
            <View
            style={{
                position: "absolute",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                backgroundColor: "rgba(246, 246, 246,0.6)",
                zIndex: 999,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 20,
            }}
            />
          ) : null}
          {autos.length ? (
            <Carousel
              layout={"stack"}
              // layout={"default"}
              data={autos}
              loop={true}
              loopClonesPerSide={5}
              initialNumToRender={autos.length + 1}
              renderItem={SliderItem}
              sliderWidth={width}
              itemWidth={width - 100}
              // slideInterpolatedStyle={animatedStyles}
              // scrollInterpolator={scrollInterpolator}
              useScrollView={false}
              onSnapToItem={(index) => setCurrentCar(index)}
            />
          ) : (
            <View
              style={{
                alignItems: "center",
                maxWidth: 280,
                justifyContent: "center",
                paddingBottom: 10,
                alignSelf: "center",
              }}
            >
              <CarNumber autoInfo={"--------"} />
            </View>
          )}
        </View>
      </View>
      <ScrollView style={{ width: "100%" }}>
        {!loading ? (
          autos.length > 0 ? (
            events.length > 0 ? (
              events.map((event, key) => {
                return (
                  <OrderItem
                    key={key}
                    top={key === 0 ? true : false}
                    bottom={key === events.length - 1}
                    event={event}
                    auto={autos[index]}
                    method={() => getList(index)}
                  />
                );
              })
            ) : (
              <EventEmpty />
            )
          ) : (
            <View>
              <AutoEmpty />
              <View style={{ marginHorizontal: 20 }}>
                <AddAutoButton />
              </View>
            </View>
          )
        ) : (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              minHeight: 200,
            }}
          >
            <Image
              source={global.images.loader}
              style={{ height: 200, width: 200, alignSelf: "center" }}
            />
          </View>
        )}
        {/* <MainItem type="service" top={true} />
          <MainItem type="service" />
          <MainItem type="service" bottom={true} /> */}
        <View height={100} />
      </ScrollView>
    </View>
  );
}
export default BookingScreen;
const styles = StyleSheet.create({
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
});
