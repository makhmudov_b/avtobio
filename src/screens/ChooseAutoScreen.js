import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Image,
  Alert,
} from "react-native";
import global from "../../resources/global";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import CarNumber from "../components/CarNumber";

function ChooseAutoScreen({ navigation, route }) {
  const [autos, setAutos] = useState([]);
  const { params } = route;
  const { method } = params;
  const { apiService, token } = useContext(Context);
  const [loading, setLoading] = useState(true);
  const [disabled, setDisabled] = useState(false);
  useEffect(() => {
    apiService
      .getResources("/vehicle/list", token)
      .then((value) => {
        if (value.data) setAutos(value.data);
        setLoading(false);
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
      });
  }, []);
  const chooseAuto = (id, plateNumber) => {
    method(id, plateNumber);
    navigation.goBack();
    setDisabled(true);
    setTimeout(() => setDisabled(true), 800);
  };
  return (
    <>
      <HeaderBack
        type="white"
        button={true}
        title="Выберите авто"
        navigation={navigation}
      />
      <ScrollView style={styles.container}>
        <View style={{ height: 20 }} />
        <View style={{ flex: 1 }}>
          <View style={[styles.whiteBox, { marginBottom: 20 }]}>
            {!loading ? (
              autos.length ? (
                autos.map((auto, key) => {
                  return (
                    <TouchableOpacity
                      key={key}
                      disabled={disabled}
                      onPress={() => chooseAuto(auto.id, auto.plateNumber)}
                      style={{
                        width: "100%",
                        maxWidth: 300,
                        marginBottom: autos.length - 1 === key ? 0 : 10,
                      }}
                    >
                      <CarNumber autoInfo={auto.plateNumber} />
                    </TouchableOpacity>
                  );
                })
              ) : null
            ) : (
              <Image
                source={global.images.loader}
                style={{ height: 200, width: 200, alignSelf: "center" }}
              />
            )}
          </View>
        </View>
      </ScrollView>
    </>
  );
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#F6F6F6" },
  left: {
    marginRight: 10,
  },
  right: {
    marginLeft: 10,
  },
  whiteBox: {
    backgroundColor: "white",
    marginRight: 20,
    marginLeft: 20,
    padding: 20,
    shadowColor: "#000",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,

    elevation: 8,
  },
  innerBox: {
    height: 137,
    width: 137,
    alignItems: "center",
    justifyContent: "center",
  },
  innerText: {
    color: "#444444",
    fontSize: 14,
    fontFamily: global.fonts.medium,
    marginTop: 10,
  },
  box: {
    height: 60,
    width: 60,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
});
export default ChooseAutoScreen;
