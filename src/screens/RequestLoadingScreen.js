import React, { Component } from "react";
import { View, Image } from "react-native";
import global from "../../resources/global";
export default class RequestLoadingScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          backgroundColor: "rgba(255,255,255,.3)",
          justifyContent: "center",
        }}
      >
        <View>
          <Image
            source={global.images.loader}
            style={{ height: 200, width: 200, alignSelf: "center" }}
          />
        </View>
      </View>
    );
  }
}
