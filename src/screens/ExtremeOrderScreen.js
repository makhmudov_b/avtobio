import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-red.svg";
import ArrowRight from "../../assets/arrow-right-red.svg";
import Edit from "../../assets/edit-red.svg";
import Calendar from "../../assets/calendar-time.svg";
import Point from "../../assets/extreme-location.svg";
import Points from "../../assets/points-evacuate.svg";
import Location from "../../assets/location-red.svg";
import FirstStatus from "../../assets/first-status.svg";

import { SafeAreaProvider } from "react-native-safe-area-context";
import SafeAreaView from "react-native-safe-area-view";
import Constants from "expo-constants";
import HeaderBackRed from "../components/HeaderBackRed";

function ExtremeOrderScreen({ route, navigation }) {
  const [confirmation, onChangeConfirmation] = useState("");
  const { params } = route;
  const LeftBlock = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ marginRight: 15 }}>
            {params.type === 1 ? (
              <Points height={84} width={24} />
            ) : (
              <Point height={84} width={24} />
            )}
          </View>
          {params.type === 2 ? (
            <View style={{ flex: 1 }}>
              <View style={{ position: "relative" }}>
                <TouchableOpacity
                  onPress={() => navigation.navigate("ExtremeMap", { type: 2 })}
                >
                  <Text
                    style={[styles.authorizationInput, { color: "#999999" }]}
                  >
                    Адресс
                  </Text>
                </TouchableOpacity>
                <View style={styles.icon}>
                  <Location />
                </View>
              </View>
            </View>
          ) : (
            <View style={{ flex: 1 }}>
              <View style={{ position: "relative" }}>
                <TouchableOpacity>
                  <Text
                    style={[styles.authorizationInput, { color: "#999999" }]}
                  >
                    Пункт А
                  </Text>
                </TouchableOpacity>
                <View style={styles.icon}>
                  <Location />
                </View>
              </View>
              <View style={{ position: "relative" }}>
                <TouchableOpacity>
                  <Text
                    style={[styles.authorizationInput, { color: "#999999" }]}
                  >
                    Пункт B
                  </Text>
                </TouchableOpacity>
                <View style={styles.icon}>
                  <Location />
                </View>
              </View>
            </View>
          )}
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity onPress={() => navigation.navigate("MasterType")}>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#999999" },
              ]}
            >
              Тип мастера
            </Text>
          </TouchableOpacity>
          <View style={styles.icon}>
            <ArrowRight />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <TouchableOpacity onPress={() => navigation.navigate("CallType")}>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#999999" },
              ]}
            >
              Тип вызова
            </Text>
          </TouchableOpacity>
          <View style={styles.icon}>
            <ArrowRight />
          </View>
        </View>
        <View style={{ position: "relative" }}>
          <View>
            <Text
              style={[
                styles.authorizationInput,
                { lineHeight: 50, color: "#999999" },
              ]}
            >
              Сумма расхода
            </Text>
          </View>
        </View>
        <Text
          style={{
            textAlign: "center",
            paddingHorizontal: 20,
            paddingBottom: 10,
            fontSize: 11,
            color: global.colors.danger,
          }}
        >
          Сумма расхода указывается приблизительно
        </Text>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Телефон"
            placeholderTextColor="#999999"
            keyboardType={`phone-pad`}
            value={"+998 99 881 80 60"}
          />
          <Edit style={styles.icon} />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={confirmation}
          />
        </View>
      </View>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBackRed
          type="white"
          button={true}
          title={params.type === 1 ? "Эвакуатор" : "Мастер"}
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={{ height: 20 }} />
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>
                <LeftBlock />
              </View>
            </View>
            <View
              style={[
                styles.authorization,
                { marginTop: 25, marginBottom: 20 },
              ]}
            >
              <View style={styles.authorizationBlock}>
                <Text
                  style={{
                    textAlign: "center",
                    color: "#444444",
                    fontFamily: global.fonts.medium,
                    fontSize: 24,
                  }}
                >
                  Статус заказа
                </Text>
                <View style={{ flexDirection: "row", marginTop: 20 }}>
                  <FirstStatus />
                  <View style={{ marginLeft: 10 }}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#444444",
                        marginBottom: 10,
                      }}
                    >
                      Заявка принята
                    </Text>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#444444",
                        marginBottom: 10,
                      }}
                    >
                      Прибывает
                    </Text>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#999999",
                        marginBottom: 10,
                      }}
                    >
                      В пути
                    </Text>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#999999",
                        marginBottom: 10,
                      }}
                    >
                      Завершен
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TouchableOpacity
              style={[styles.button, { backgroundColor: global.colors.danger }]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Отменить заявку
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default ExtremeOrderScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 20,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});
