import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  TextInput,
} from "react-native";
import global from "../../resources/global";
import CustomIcon from "../../assets/background.svg";
import Arrow from "../../assets/arrow.svg";
import { AnimatedCircularProgress } from "react-native-circular-progress";
// import SmsRetriever from "react-native-sms-retriever";
import Context from "../components/Context";
import CustomStatusBar from "../components/CustomStatusBar";
import RequestLoadingScreen from "./RequestLoadingScreen";

const MAX_POINTS = 60;
class LoginScreen extends React.Component {
  // const { apiService, token, phone } = useContext(Context);
  static contextType = Context;
  constructor(props) {
    super(props);
  }
  state = {
    code: "",
    again: false,
    loading: false,
  };

  setCode = (text) => {
    this.setState({ code: text });
  };

  storeData = async (name, data) => {
    try {
      await AsyncStorage.setItem(name, JSON.stringify(data));
    } catch (e) {}
  };
  getData = async (name) => {
    try {
      const data = await AsyncStorage.getItem(name);
      return data;
    } catch (e) {}
  };
  setLoading = (value) => {
    this.setState({ loading: value });
  };
  sendAgain = async () => {
    const { apiService, phone } = this.context;
    const { navigation } = this.props;
    this.setLoading(true);
    apiService
      .sendCode(phone)
      .then((value) => {
        this.setLoading(false);
        if (value.statusCode === 200) {
          navigation.replace("Confirm");
        } else {
          Alert.alert("Введены неправильные данные. Повторите заново!");
        }
      })
      .catch((e) => {
        this.setLoading(false);
        Alert.alert("Введены неправильные данные. Повторите заново!");
      });
  };
  onConfirm = async () => {
    if (this.state.code.length !== 6) {
      Alert.alert("Введите 6 цифр");
      return 0;
    }
    this.context.setLoading(false);
    fetch("https://api.avtobio.uz:443/api/mobile/v1/member/activatecode", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ code: this.state.code }),
    })
      .then((res) => {
        return res.json(); // data. accessToken , registered : true
      })
      .then((res) => {
        if (res.statusCode === 200) {
          const registered = !!res.data.registered;
          const token = res.data.accessToken;
          this.context.setToken(token);
          this.storeData("token", token);
          this.storeData("registered", registered);
          if (registered === true) {
            this.context.getUserData(true);
            this.props.navigation.replace("Home");
          } else {
            this.props.navigation.replace("Register");
          }
        } else {
          this.context.setLoading(false);
          Alert.alert("Что-то пошло не так");
        }
      });
  };

  render() {
    const { navigation } = this.props;
    if (this.state.loading) return <RequestLoadingScreen />;
    return (
      <>
        <CustomStatusBar type={"blue"} />
        <View style={styles.container}>
          <View style={{ height: 200, position: "relative" }}>
            <View style={{ zIndex: 20, paddingTop: 20, paddingLeft: 20 }}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Arrow />
              </TouchableOpacity>
            </View>
            <View style={[styles.absoluteBackground]}>
              <CustomIcon width={350} height={610} />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={styles.authorization}>
              <View style={styles.authorizationTop}>
                <Text
                  style={{
                    fontFamily: global.fonts.medium,
                    fontSize: 18,
                    color: "#444444",
                  }}
                >
                  СМС код
                </Text>
              </View>
              <View style={styles.authorizationBlock}>
                <View style={{ alignItems: "center", position: "relative" }}>
                  <Text
                    style={{
                      fontSize: 18,
                      paddingBottom: 20,
                      textAlign: "center",
                      lineHeight: 20,
                      color: "#444444",
                    }}
                  >
                    На этот номер отправлен код
                  </Text>
                  <View
                    style={{
                      alignItems: "center",
                      position: "relative",
                      width: "100%",
                    }}
                  >
                    <TextInput
                      returnKeyType={"done"}
                      keyboardType="number-pad"
                      returnKeyType={"done"}
                      // autoFocus={true}
                      textContentType="oneTimeCode"
                      style={[styles.input, { width: 220 }]}
                      value={this.state.code}
                      onChangeText={(text) => this.setCode(text)}
                      maxLength={6}
                    />
                    <View style={{ position: "absolute", right: 0, top: 20 }}>
                      <AnimatedCircularProgress
                        size={30}
                        width={3}
                        fill={100}
                        duration={60000}
                        tintColor={global.colors.mainColor}
                        backgroundColor="#E9E9E9"
                        onAnimationComplete={() =>
                          this.setState({ again: true })
                        }
                      >
                        {(fill) => (
                          <Text style={styles.points}>
                            {Math.round(MAX_POINTS - (MAX_POINTS * fill) / 100)}
                          </Text>
                        )}
                      </AnimatedCircularProgress>
                    </View>
                    <View style={styles.bottomLine}></View>
                  </View>
                </View>
                <TouchableOpacity
                  style={[
                    styles.button,
                    { backgroundColor: global.colors.mainColor },
                  ]}
                  onPress={() => this.onConfirm()}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 18,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    Войти
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={!this.state.again}
                  style={[
                    styles.button,
                    {
                      backgroundColor: this.state.again
                        ? global.colors.mainColor
                        : "#A0DCF5",
                    },
                  ]}
                  onPress={() => this.sendAgain()}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 18,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    Отправить заново
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.mainColor,
    position: "relative",
  },
  absoluteBackground: {
    paddingTop: 10,
    alignItems: "center",
    position: "absolute",
    bottom: `-10%`,
    width: `100%`,
    height: `100%`,
    left: 0,
    right: 0,
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    transform: [{ translateY: -76 }],
  },
  authorizationTop: {
    alignItems: "center",
    justifyContent: "center",
    height: 76,
    borderBottomWidth: 1,
    borderBottomColor: "#E9E9E9",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 20,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 10,
    borderColor: global.colors.mainColor,
    borderWidth: 2,
  },
  input: {
    fontSize: 24,
    fontFamily: global.fonts.medium,
    letterSpacing: 20,
    height: 45,
    textAlign: "center",
  },
  bottomLine: {
    height: 2,
    width: 210,
    marginTop: 8,
    marginBottom: 20,
    backgroundColor: "#E9E9E9",
  },
  points: {
    color: global.colors.mainColor,
    fontSize: 10,
    fontFamily: global.colors.medium,
  },
});
