import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Image,
} from "react-native";
import global from "../../resources/global";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";

function ChoiceScreen({ navigation, route }) {
  const { apiService, token } = useContext(Context);
  const { params } = route;
  const { title, setMethod, requestId } = params;
  const [choices, setChoices] = useState([]);
  const [loading, setLoading] = useState(true);
  const reasonCheck = title === "Причина вызова" || title === "Причина визита";
  const getData = (url) => {
    if (reasonCheck) return apiService.getReferences(url, token);
    return apiService.getResources(url, token);
  };
  const getUrl = () => {
    if (title === "Цикл уведомлений") return "/events/recurring-intervals";
    if (title === "Производитель") return "/vehicle/makers";
    if (title === "Модель") return "/vehicle/models/" + requestId;
    if (title === "Модификация") return "/vehicle/modifications/" + requestId;
    if (reasonCheck)
      return "/backend/v1/reference/choose?parentCode=AUTO_SERVICE";
  };
  const submit = (choice) => {
    setMethod(choice);
    navigation.goBack();
  };
  useEffect(() => {
    getData(getUrl()).then((value) => {
      if (value.data) setChoices(value.data);
      setLoading(false);
    });
  }, []);
  return (
    <>
      <HeaderBack
        type="white"
        button={true}
        title={title}
        navigation={navigation}
      />
      <ScrollView style={styles.container}>
        <View style={{ height: 20 }} />
        <View style={[styles.whiteBox, { marginBottom: 20 }]}>
          {!loading ? (
            choices.length ? (
              choices.map((choice, key) => {
                return (
                  <TouchableOpacity key={key} onPress={() => submit(choice)}>
                    <Text style={styles.authorizationInput}>{choice.name}</Text>
                  </TouchableOpacity>
                );
              })
            ) : null
          ) : (
            <Image
              source={global.images.loader}
              style={{ height: 200, width: 200, alignSelf: "center" }}
            />
          )}
        </View>
      </ScrollView>
    </>
  );
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#F6F6F6" },
  left: {
    marginRight: 10,
  },
  right: {
    marginLeft: 10,
  },
  whiteBox: {
    backgroundColor: "white",
    marginRight: 20,
    marginLeft: 20,
    padding: 20,
    shadowColor: "#000",
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 2,
  },
  box: {
    height: 60,
    width: 60,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    lineHeight: 50,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    textAlign: "center",
  },
});
export default ChoiceScreen;
