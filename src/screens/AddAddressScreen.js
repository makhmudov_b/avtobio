import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Work from "../../assets/work.svg";
import House from "../../assets/house.svg";
import Service from "../../assets/service.svg";
import Fuel from "../../assets/fuel.svg";
import Location from "../../assets/location.svg";
import LocationWhite from "../../assets/location-white.svg";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function AddAddressScreen({ navigation, route }) {
  const { token, apiService } = useContext(Context);
  const { params } = route;
  const { update, data } = params;
  const [note, setNote] = useState("");
  const [name, setName] = useState("");
  const [lattitude, setLattitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [address, setAddress] = useState(null);
  const [mark, setMark] = useState(0);
  const [loading, setLoading] = useState(0);
  const [emotions] = useState([
    { text: <Work /> },
    { text: <House /> },
    { text: <Service width={30} height={30} /> },
    { text: <Fuel /> },
    { text: <LocationWhite /> },
  ]);
  useEffect(() => {
    if (data) {
      setAddress(data.address);
      setMark(Number(data.icon));
      setLongitude(data.lng);
      setLattitude(data.lat);
      setName(data.name);
      if (data.note) setNote(data.note);
    }
  }, []);
  const setPosition = ({ lat, lng }) => {
    setLattitude(lat);
    setLongitude(lng);
    apiService
      .getAddress(lat, lng)
      .then((value) => setAddress(value.results[0].formatted_address));
  };
  const deleteForm = () => {
    setLoading(true);
    apiService.deleteData("/address/item/" + data.id, token).then((value) => {
      setLoading(false);
      if (value.statusCode === 200) {
        update();
        navigation.goBack();
      } else {
        Alert.alert(value.message);
      }
    });
  };
  const saveForm = () => {
    if (lattitude && longitude && name) {
      const body = {
        address: address,
        icon: mark,
        lat: lattitude,
        lng: longitude,
        name: name,
        note: note,
      };
      setLoading(true);
      if (data) {
        apiService.updateForm("/address/item", token, body).then((value) => {
          setLoading(false);
          if (value.statusCode === 200) {
            update();
            navigation.goBack();
          } else {
            Alert.alert(value.message);
          }
        });
      } else {
        apiService.postData(token, "/address/item", body).then((value) => {
          setLoading(false);
          if (value.statusCode === 200) {
            update();
            navigation.goBack();
          } else {
            Alert.alert(value.message);
          }
        });
      }
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          type="white"
          button={true}
          title={data ? "Адрес" : "Новый адрес"}
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ height: 20 }} />
          <View style={{ flex: 1, justifyContent: "space-between" }}>
            <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
              <View style={styles.authorization}>
                <View style={styles.authorizationBlock}>
                  <Text
                    style={{
                      fontSize: 18,
                      paddingBottom: 25,
                      fontFamily: global.fonts.medium,
                      textAlign: "center",
                    }}
                  >
                    Иконка адреса
                  </Text>
                  <View
                    style={{
                      paddingBottom: 20,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    {emotions.map((emotion, key) => {
                      return (
                        <TouchableOpacity
                          onPress={() => setMark(key)}
                          style={[styles.emojiButton]}
                          key={key}
                        >
                          <View
                            style={[
                              {
                                width: 50,
                                height: 50,
                                borderRadius: 10,
                                alignItems: "center",
                                justifyContent: "center",
                                backgroundColor: global.colors.mainColor,
                              },
                              mark === key ? { opacity: 1 } : { opacity: 0.5 },
                            ]}
                          >
                            <Text style={[{ fontSize: 28 }]}>
                              {emotion.text}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Название адреса"
                    placeholderTextColor="#999999"
                    value={name}
                    onChangeText={(text) => setName(text)}
                  />
                  <View style={{ position: "relative" }}>
                    <TouchableOpacity
                      style={{ position: "relative" }}
                      onPress={() =>
                        navigation.push("AddressMap", {
                          title: "Новый адрес ",
                          type: "light",
                          setPosition: setPosition,
                          showAddress: false,
                        })
                      }
                    >
                      <Text
                        style={[
                          styles.authorizationInput,
                          {
                            lineHeight: 50,
                            color: !address ? "#999" : "#333",
                            paddingRight: 50,
                          },
                        ]}
                      >
                        {!address ? "Укажите адрес" : address}
                      </Text>
                      <Location style={styles.icon} />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    returnKeyType={"done"}
                    style={styles.authorizationInput}
                    placeholder="Примечание ..."
                    placeholderTextColor="#999999"
                    value={note}
                    onChangeText={(text) => setNote(text)}
                  />
                </View>
              </View>
              <View style={{ width: "90%" }}>
                <TouchableOpacity
                  style={[
                    styles.button,
                    { backgroundColor: global.colors.mainColor },
                  ]}
                  onPress={() => (data ? deleteForm() : saveForm())}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 18,
                      fontFamily: global.fonts.regular,
                    }}
                  >
                    {data ? "Удалить" : "Сохранить"}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default AddAddressScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-around",
  },
  emojiButton: {
    height: 50,
    width: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    overflow: "hidden",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  noPhoto: {
    height: 150,
    borderWidth: 1,
    borderColor: "rgba(0,0,0,.15)",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
  },
  icon: {
    position: "absolute",
    right: 20,
    top: 13,
  },
});
