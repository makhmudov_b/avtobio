import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  Modal,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-red.svg";
import ArrowRight from "../../assets/arrow-right-red.svg";
import Edit from "../../assets/edit-red.svg";
import Calendar from "../../assets/calendar-time.svg";
import Points from "../../assets/points-evacuate.svg";
import Location from "../../assets/location-red.svg";
import Constants from "expo-constants";
import Success from "../../assets/success-icon.svg";
import HeaderBackRed from "../components/HeaderBackRed";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function EvacuatorScreen({ navigation }) {
  const { apiService, token, phone } = useContext(Context);
  const [vehicleId, setVehicle] = useState(0);
  const [number, setNumber] = useState("");
  const [note, setNote] = useState("");
  const [origin, setOrigin] = useState("");
  const [destination, setDestination] = useState("");
  const [pointA, setPointA] = useState({ lat: 0, lng: 0 });
  const [pointB, setPointB] = useState({ lat: 0, lng: 0 });
  const [showModal, setShowModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [distance, setDistance] = useState(0);
  const [chosenPhone, setPhone] = useState(phone);
  const [basisRange, setBasisRange] = useState(null);
  const [priceRange, setPriceRange] = useState(null);
  useEffect(() => {
    getPrice();
  }, []);
  const getPrice = () => {
    apiService
      .getResources("/sos/evacuator/minimal-price", token)
      .then((value) => {
        const { data } = value;
        setBasisRange([data.minPrice, data.minPrice]);
        setPriceRange([data.minPricePerKm, data.maxPricePerKm]);
        setLoading(false);
      });
  };
  const call = () => {
    setShowModal(true);
    setTimeout(() => {
      navigation.navigate("ExtremeOrder", { type: 2 }), setShowModal(false);
    }, 2000);
    return 1;
  };
  const setPositionA = (value) => {
    setPointA(value);
    if (pointB.lat != 0) callDistance(value, pointB);
  };
  const setPositionB = (value) => {
    setPointB(value);
    if (pointA.lat != 0) callDistance(pointA, value);
  };
  const callDistance = (orig, dist) => {
    const origin = orig.lat + "," + orig.lng;
    const destin = dist.lat + "," + dist.lng;
    apiService.getDistance(origin, destin).then((res) => {
      if (res.status == "OK") {
        setDestination(res.destination_addresses[0]);
        setOrigin(res.origin_addresses[0]);
        const data = res.rows[0].elements[0].distance.value;
        const value = Math.ceil(parseInt(data) / 1000, -1);
        setDistance(value);
      }
    });
  };
  const ChooseAuto = (id, plateNumber) => {
    setNumber(plateNumber);
    setVehicle(id);
  };
  const save = () => {
    if (
      distance > 0 &&
      // price.length > 0 &&
      parseInt(distance) !== 0 &&
      pointA.long !== 0 &&
      pointB.long !== 0
    ) {
      const body = {
        coordinate: pointA,
        distance: parseInt(distance),
        evacuationCoordinate: pointB,
        note: note,
        phone: phone,
        vehicleId: parseInt(vehicleId),
      };
      setLoading(true);
      apiService
        .postData(token, "/sos/evacuator/order", body, body.vehicleId)
        .then((value) => {
          setLoading(false);
          if (value.statusCode === 200) {
            setShowModal(true);
            setTimeout(() => navigation.replace("Booking"), 1500);
          } else {
            Alert.alert(value.message);
          }
        })
        .catch((e) => {
          Alert.alert("Что-то пошло не-так!");
          setLoading(false);
        });
    } else {
      Alert.alert("Нужно заполнить все поля!");
    }
  };
  const LeftBlock = () => {
    return (
      <View>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("ChooseAuto", {
              method: ChooseAuto,
            })
          }
        >
          <Text
            style={[
              styles.authorizationInput,
              {
                lineHeight: 50,
                color: !vehicleId ? "#999999" : "#444444",
              },
            ]}
          >
            {!vehicleId ? "Выберите авто" : number}
          </Text>
          <ArrowRight style={styles.icon} />
        </TouchableOpacity>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <View style={{ flex: 1 }}>
            <View style={{ position: "relative" }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddressMap", {
                    title: "Место происшествие",
                    setPosition: (value) => setPositionA(value),
                    showAddress: false,
                  })
                }
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color:
                        pointA.lng === 0 && origin.length == 0
                          ? "#999999"
                          : global.colors.success,
                      paddingRight: 40,
                    },
                  ]}
                >
                  {pointA.lat === 0
                    ? "Место происшествие"
                    : origin
                    ? origin
                    : "Выберите Место эвакуации"}
                </Text>
                <View style={styles.icon}>
                  <Location />
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ position: "relative" }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("AddressMap", {
                    title: "Место эвакуации",
                    setPosition: (value) => setPositionB(value),
                    showAddress: false,
                  })
                }
              >
                <Text
                  style={[
                    styles.authorizationInput,
                    {
                      lineHeight: 50,
                      color:
                        pointB.lng === 0 ? "#999999" : global.colors.success,
                      paddingRight: 40,
                    },
                  ]}
                >
                  {pointB.lat === 0
                    ? "Место эвакуации"
                    : destination
                    ? destination
                    : "Выберите Выберите Место происшествия"}
                </Text>
                <View style={styles.icon}>
                  <Location />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={{
            alignSelf: "center",
            paddingVertical: 20,
            justifyContent: "center",
            position: "relative",
          }}
        >
          <View
            style={{
              position: "absolute",
              justifyContent: "center",
              alignItems: "center",
              left: 0,
              right: 0,
              zIndex: 40,
            }}
          >
            <View
              style={[
                {
                  paddingHorizontal: 20,
                  height: 50,
                  backgroundColor: global.colors.danger,
                  borderRadius: 25,
                  justifyContent: "center",
                  alignItems: "center",
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 18,
                  color: "white",
                  fontFamily: global.fonts.regular,
                }}
              >
                {distance + "KM"}
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  color: "white",
                  fontFamily: global.fonts.regular,
                }}
              >
                расстояние
              </Text>
            </View>
          </View>

          <Points
            width={global.strings.width - 40}
            style={{
              resizeMode: "contain",
            }}
          />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Телефон"
            placeholderTextColor="#999999"
            keyboardType={`phone-pad`}
            value={chosenPhone}
            onChangeText={(text) => setPhone(text)}
          />
          <Edit style={styles.icon} />
        </View>
        <View style={{ position: "relative" }}>
          <TextInput
            returnKeyType={"done"}
            style={styles.authorizationInput}
            placeholder="Примечание ..."
            placeholderTextColor="#999999"
            value={note}
            onChangeText={(text) => setNote(text)}
          />
        </View>
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <Modal animationType="fade" transparent={true} visible={showModal}>
        <View
          style={{ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }}
        >
          <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
        </View>
        <View
          style={{
            flexDirection: "row",
            position: "absolute",
            marginTop: 60,
            // top: 0,
            // left: 0,
            // right: 0,
            // height: global.strings.height,
            // justifyContent: "center",
            // alignItems: "center",
            alignSelf: "center",
          }}
        >
          <View
            style={[
              styles.authorizationBlock,
              {
                borderRadius: 20,
                marginHorizontal: 20,
                backgroundColor: "white",
              },
            ]}
          >
            <Text
              style={{
                fontSize: 24,
                textAlign: "center",
                marginBottom: 10,
                fontFamily: global.fonts.medium,
                color: "#444444",
              }}
            >
              Заявка принята
            </Text>
            <Text
              style={{
                fontFamily: global.fonts.regular,
                fontSize: 10,
                textAlign: "center",
                color: "#999999",
              }}
            >
              Указанная сумма расхода может меняться в зависимость от
              обстоятельств и дистанции
            </Text>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: 20,
              }}
            >
              <Success />
              <Text
                style={{
                  marginTop: 20,
                  fontSize: 14,
                  textAlign: "center",
                  color: "#444444",
                  fontFamily: global.fonts.regular,
                }}
              >
                Мин. стоимость:{" "}
                <Text style={{ fontWeight: "bold" }}>
                  {basisRange[0] + distance * priceRange[0]} UZS
                </Text>
              </Text>
              <Text
                style={{
                  marginTop: 10,
                  fontSize: 14,
                  textAlign: "center",
                  color: "#444444",
                  fontFamily: global.fonts.regular,
                }}
              >
                Макс. стоимость:{" "}
                <Text style={{ fontWeight: "bold" }}>
                  {basisRange[1] + distance * priceRange[1]} UZS
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </Modal>
      <View style={styles.container}>
        <HeaderBackRed
          type="white"
          button={true}
          title="Эвакуатор"
          navigation={navigation}
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ height: 20 }} />
          <View
            style={{
              flex: 1,
              backgroundColor: "#F6F6F6",
              alignItems: "center",
            }}
          >
            <View style={styles.authorization}>
              <View style={styles.authorizationBlock}>{LeftBlock()}</View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <Text
                  style={{
                    fontFamily: global.fonts.regular,
                    color: "#333",
                    fontSize: 18,
                    marginBottom: 8,
                  }}
                >
                  Сумма за выезд
                </Text>
                <Text
                  style={{
                    fontFamily: global.fonts.regular,
                    color: "#999",
                    fontSize: 11,
                    marginBottom: 10,
                  }}
                >
                  {basisRange && `${basisRange[0]} - ${basisRange[1]} UZS`}
                </Text>
                <Text
                  style={{
                    fontFamily: global.fonts.regular,
                    color: "#333",
                    fontSize: 18,
                    marginBottom: 8,
                  }}
                >
                  Сумма за километр
                </Text>
                <Text
                  style={{
                    fontFamily: global.fonts.regular,
                    color: "#999",
                    fontSize: 11,
                  }}
                >
                  {priceRange && `${priceRange[0]} - ${priceRange[1]} UZS`}
                </Text>
              </View>
            </View>
            <View style={[styles.authorization, { marginTop: 20 }]}>
              <View style={styles.authorizationBlock}>
                <View style={{ position: "relative" }}>
                  <TextInput
                    returnKeyType={"done"}
                    style={[styles.authorizationInput]}
                    placeholder="Общая сумма"
                    keyboardType={"numeric"}
                    placeholderTextColor="#999999"
                    value={
                      distance != 0
                        ? `${basisRange[0] + distance * priceRange[0]} - ${
                            basisRange[1] + distance * priceRange[1]
                          }`
                        : ""
                    }
                    editable={false}
                  />
                  <View style={[styles.icon, { width: "auto" }]}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: "#999999",
                        fontFamily: global.fonts.regular,
                      }}
                    >
                      UZS
                    </Text>
                  </View>
                </View>
                <Text
                  style={{
                    color: global.colors.danger,
                    fontSize: 11,
                    fontFamily: global.fonts.regular,
                    textAlign: "center",
                  }}
                >
                  Указанная сумма расхода может меняться в зависимость от
                  обстоятельств и дистанции
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => save()}
              style={[styles.button, { backgroundColor: global.colors.danger }]}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Вызвать эвакуатор
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default EvacuatorScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 20,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});
