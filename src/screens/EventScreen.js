import React, { useContext, useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Service from "../../assets/service.svg";
import Road from "../../assets/road.svg";
import Fuel from "../../assets/fuel.svg";
import Dollar from "../../assets/dollar.svg";
// import MasterItem from "../components/MasterItem";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";
import {formatDate} from "../utils/date";

function EventScreen({ navigation, route }) {
  const { apiService, token } = useContext(Context);
  const [loading, setLoading] = useState(false);
  const { params } = route;
  const { event, auto, reload } = params;
  const { type , eventDate } = event;
  const chosenDateTime = formatDate(eventDate);
  const deleteEvent = () => {
    setLoading(true);
    apiService
      .deleteData(
        "/events?" + new URLSearchParams({ type: type, eventId: event.id }),
        token,
        auto.id
      )
      .then((value) => {
        if (value.statusCode == 200) {
          reload();
          navigation.goBack();
        }
        setLoading(false);
      });
  };
  const TextBlock = ({ textLeft, textRight, colorLeft, colorRight }) => {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingTop: 5,
        }}
      >
        <Text
          style={{
            fontFamily: global.fonts.regular,
            fontSize: 9,
            color: "#999999",
          }}
        >
          {textLeft}
        </Text>
        <Text
          style={{
            fontFamily: global.fonts.regular,
            fontSize: 9,
            color: "#999999",
          }}
        >
          {textRight}
        </Text>
      </View>
    );
  };
  if (loading) return <RequestLoadingScreen />;
  const moveScreen = () => {
    if (type === "REFUEL") return "Fuel";
    if (type === "SERVICE") return "Service";
    if (type === "TRIP") return "Road";
    if (type === "EARN") return "Income";
    if (type === "EXPENSE") return "Expense";
    return "Home";
  };
  const getName = () => {
    if (
      event.name &&
      type != "CALL" &&
      type != "VISIT" &&
      type != "VISIT" &&
      type != "ORDER"
    ) {
      return event.name;
    } else {
      if (type == "CALL") {
        return "Вызов мастера";
      } else if (type == "VISIT") return "Вызов мастера";
      else if (type == "ORDER") return "Вызов мастера";
      else return "Визит к мастеру";
    }
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          navigation={navigation}
          title={`${getName().slice(0, 14)}`}
          button={true}
          type="white"
        />
        <View style={{ flex: 1 }}>
          <View style={{ height: 20 }} />
          <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
            <View style={[styles.authorization]}>
              <View
                style={{
                  backgroundColor: "white",
                  padding: 20,
                  borderRadius: 20,
                  shadowColor: "rgba(0,0,0,.15)",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.9,
                  shadowRadius: 3.84,
                  elevation: 5,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 20,
                  }}
                >
                  <View
                    style={[
                      styles.icon,
                      type === "SERVICE" || type === "ORDER" || type === "CALL"
                        ? styles.warn
                        : styles.noStyle,
                      type === "TRIP" ? styles.danger : styles.noStyle,
                      type === "EXPENSE" ? styles.tax : styles.noStyle,
                      type === "EARN"
                        ? { backgroundColor: "#6AD97B" }
                        : styles.noStyle,
                      type === "REFUEL"
                        ? { backgroundColor: "#B84BFF" }
                        : styles.noStyle,
                    ]}
                  >
                    {type === "SERVICE" ||
                    type === "ORDER" ||
                    type === "CALL" ? (
                      <Service />
                    ) : null}
                    {type === "TRIP" ? <Road /> : null}
                    {type === "EXPENSE" ? <Dollar /> : null}
                    {type === "REFUEL" ? <Fuel /> : null}
                    {type === "EARN" ? <Dollar /> : null}
                  </View>
                  <View
                    style={{
                      flex: 1,
                    }}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "flex-start",
                          justifyContent: "space-between",
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 14,
                            fontFamily: global.fonts.medium,
                            color: "#444444",
                            maxWidth: 150,
                          }}
                        >
                          {getName().slice(0, 14)}
                          {getName().length > 14 && "..."}
                        </Text>
                        <Text
                          style={[
                            styles.infoText,
                            type === "SERVICE" ||
                            type === "ORDER" ||
                            type === "CALL"
                              ? styles.warnColor
                              : styles.noStyle,
                            type === "TRIP"
                              ? styles.dangerColor
                              : styles.noStyle,
                            type === "EXPENSE"
                              ? styles.taxColor
                              : styles.noStyle,
                            type === "REFUEL"
                              ? { color: "#B84BFF" }
                              : styles.noStyle,
                            type === "EARN"
                              ? { color: "#6AD97B" }
                              : styles.noStyle,
                          ]}
                        >
                          {type === "REFUEL" ? "Заправка" : null}
                          {type === "SERVICE" ? "Сервис" : null}
                          {type === "TRIP" ? "Поездка" : null}
                          {type === "EARN" ? "Заработок" : null}
                          {type === "EXPENSE" ? "Расход" : null}
                          {type === "ORDER" || type === "CALL"
                            ? event.statusName
                            : null}
                        </Text>
                      </View>

                      {auto && (
                        <TextBlock
                          textLeft={"Номер авто"}
                          textRight={auto.plateNumber}
                        />
                      )}
                      {(type == "ORDER" || type == "CALL") && (
                        <TextBlock
                          textLeft={"Имя мастера"}
                          textRight={event.name}
                        />
                      )}
                      <TextBlock
                        textLeft="Дата и время:"
                        textRight={chosenDateTime}
                      />
                      <TextBlock
                        textLeft="Цена"
                        textRight={
                          type == "ORDER" || type == "CALL"
                            ? "not given api"
                            : event.price + " UZS"
                        }
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 12,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: global.fonts.medium,
                      fontSize: 14,
                      color: "#444444",
                    }}
                  >
                    {event.note ? event.note : "Нет комментария"}
                  </Text>
                </View>
                {type !== "ORDER" && type !== "CALL" && (
                  <>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate(moveScreen(), {
                          data: event,
                          auto: auto,
                        })
                      }
                      style={{
                        width: "100%",
                        height: 50,
                        alignItems: "center",
                        justifyContent: "center",
                        backgroundColor: global.colors.mainColor,
                        borderRadius: 50,
                        marginTop: 20,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 18,
                          color: "white",
                        }}
                      >
                        Изменить
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: "100%",
                        height: 50,
                        alignItems: "center",
                        justifyContent: "center",
                        backgroundColor: global.colors.danger,
                        borderRadius: 50,
                        marginTop: 10,
                      }}
                      onPress={() => deleteEvent()}
                    >
                      <Text
                        style={{
                          fontFamily: global.fonts.regular,
                          fontSize: 18,
                          color: "white",
                        }}
                      >
                        Удалить
                      </Text>
                    </TouchableOpacity>
                  </>
                )}
              </View>
            </View>
          </View>
        </View>
      </View>
    </>
  );
}

export default EventScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
    width: "100%",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.warn,
    borderRadius: 50,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 3,
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: global.colors.mainColor,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  infoText: {
    fontFamily: global.fonts.medium,
    fontSize: 14,
  },
  noStyle: {},
  warn: {
    backgroundColor: global.colors.warn,
  },
  danger: {
    backgroundColor: global.colors.danger,
  },
  tax: {
    backgroundColor: global.colors.preDanger,
  },
  warnColor: {
    color: global.colors.warn,
  },
  dangerColor: {
    color: global.colors.danger,
  },
  taxColor: {
    color: global.colors.preDanger,
  },
  itemIcon: {
    height: 46,
    width: 46,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});
