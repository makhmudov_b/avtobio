import React, { useState, useContext } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import CustomIcon from "../../assets/settings.svg";
import Arrow from "../../assets/arrow.svg";
import CustomStatusBar from "../components/CustomStatusBar";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function SettingScrenn({ navigation }) {
  const { logout, apiService } = useContext(Context);
  const [loading, setLoading] = useState(false);
  const deleteAccount = () => {
    setLoading(true);
    apiService.deleteReference("/account").then((value) => {
      setLoading(false);
      if (value.statusCode == 200) {
        logout();
      }
    });
  };
  if (loading) return <RequestLoadingScreen />;
  return (
    <>
      <CustomStatusBar />
      <View style={{ height: 95 }}>
        <View
          style={{
            height: 100,
            backgroundColor: global.colors.mainColor,
            position: "relative",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.1,
            shadowRadius: 1.65,
            elevation: 2,
          }}
        >
          <View
            style={{
              zIndex: 20,
              paddingTop: 20,
              paddingHorizontal: 20,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity
              style={{ width: 40, height: 40 }}
              onPress={() => navigation.goBack()}
            >
              <Arrow />
            </TouchableOpacity>
            <Text style={styles.title}>Настройки</Text>
            <View style={{ opacity: 0 }}>
              <Arrow />
            </View>
          </View>
          <View style={[styles.absoluteBackground]}>
            <CustomIcon width={500} height={193} />
          </View>
        </View>
      </View>
      <View style={styles.container}>
        <View
          style={{ flex: 1, backgroundColor: "#F6F6F6", alignItems: "center" }}
        >
          {/* <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <TouchableOpacity
                  onPress={() => setLanguage(0)}
                  style={[
                    styles.leftBlock,
                    styles.block,
                    !language ? styles.activeBlock : styles.inactiveBlock,
                  ]}
                >
                  <Text
                    style={!language ? styles.activeText : styles.inactiveText}
                  >
                    Русский
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => setLanguage(1)}
                  style={[
                    styles.rightBlock,
                    styles.block,
                    language ? styles.activeBlock : styles.inactiveBlock,
                  ]}
                >
                  <Text
                    style={language ? styles.activeText : styles.inactiveText}
                  >
                    Узбекский
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View> */}
          <View style={styles.authorization}>
            <View style={styles.authorizationBlock}>
              <TouchableOpacity
                style={[
                  styles.button,
                  { backgroundColor: global.colors.danger },
                ]}
                onPress={() => logout()}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Выйти из аккаунта
                </Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                onPress={() => logout()}
                style={[
                  styles.button,
                  { backgroundColor: "#C4C4C4", marginTop: 20 },
                ]}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Удалить аккаунт
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </View>
    </>
  );
}

export default SettingScrenn;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.mainColor,
    position: "relative",
  },
  title: {
    color: "white",
    flex: 1,
    textAlign: "center",
    fontFamily: global.fonts.medium,
    fontSize: 24,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  absoluteBackground: {
    paddingTop: 10,
    alignItems: "center",
    position: "absolute",
    bottom: `-20%`,
    width: `100%`,
    height: `100%`,
    left: 0,
    right: 0,
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    marginTop: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  authorizationTop: {
    alignItems: "center",
    justifyContent: "center",
    height: 76,
    borderBottomWidth: 1,
    borderBottomColor: "#E9E9E9",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 20,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
  },
  input: {
    textAlign: "center",
    fontSize: 36,
    fontFamily: global.fonts.medium,
    letterSpacing: 20,
  },
  bottomLine: {
    height: 2,
    width: 160,
    marginTop: 8,
    marginBottom: 20,
    backgroundColor: "#E9E9E9",
  },
});
