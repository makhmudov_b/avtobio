import React, { useState, useContext, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Search from "../../assets/search.svg";
import Locations from "../../assets/locations.svg";
import MasterItem from "../components/MasterItem";
import HeaderBack from "../components/HeaderBack";
import Context from "../components/Context";
import RequestLoadingScreen from "./RequestLoadingScreen";

function MasterProfileScreen({ navigation, route }) {
  const { token, apiService, currentCar } = useContext(Context);
  const { params } = route;
  const { master, method } = params;
  const [service, setService] = useState([]);
  const [loading, setLoading] = useState(true);
  const getMaster = () => {
    apiService
      .getResources(`/booking/technician/${master.id}`, token, currentCar.id)
      .then((value) => {
        setLoading(false);
        if (value.data) {
          if (value.data.services) setService(value.data.services);
        }
      });
  };
  const chooseMaster = () => {
    method(master);
    navigation.pop(2);
  };
  useEffect(() => {
    getMaster();
  }, []);
  if (loading) {
    return <RequestLoadingScreen />;
  }
  return (
    <>
      <View style={styles.container}>
        <HeaderBack
          navigation={navigation}
          title="Профиль мастера"
          button={true}
          type="white"
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={{ backgroundColor: "#F6F6F6", alignItems: "center" }}>
            <View style={{ height: 20 }} />
            <View style={styles.authorization} pointerEvents={"none"}>
              <MasterItem master={master} />
            </View>
            <View style={[styles.authorization]}>
              <View
                style={{
                  backgroundColor: "white",
                  padding: 20,
                  borderRadius: 20,
                }}
              >
                <View>
                  <Text
                    style={{
                      fontSize: 18,
                      marginBottom: 10,
                      fontFamily: global.fonts.medium,
                      textAlign: "center",
                      color: "#444444",
                    }}
                  >
                    Перечень услуг
                  </Text>
                </View>
                <View>
                  {service.map((serv, key) => {
                    return (
                      <View
                        key={key}
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          marginTop: 6,
                          alignItems: "center",
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 14,
                            fontFamily: global.fonts.regular,
                            color: "#444444",
                            flex: 1,
                          }}
                        >
                          {serv.name}
                        </Text>
                        <Text
                          style={{
                            fontSize: 14,
                            fontFamily: global.fonts.medium,
                            color: "#444444",
                          }}
                        >
                          {serv.price} UZS
                        </Text>
                      </View>
                    );
                  })}
                </View>
              </View>
            </View>
            <View
              style={[
                styles.authorization,
                { marginTop: 20, marginBottom: 20 },
              ]}
            >
              <View
                style={[
                  {
                    flexDirection: "row",
                    paddingHorizontal: 20,
                    height: 40,
                    justifyContent: "space-between",
                    alignItems: "center",
                    backgroundColor: "white",
                    borderTopRightRadius: 20,
                    borderTopLeftRadius: 20,
                  },
                  styles.shadow,
                ]}
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: global.fonts.medium,
                    color: "#444444",
                  }}
                >
                  Кол-во отзывов:
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: global.fonts.medium,
                    color: "#444444",
                  }}
                >
                  {master.rateCount}
                </Text>
              </View>
              <TouchableOpacity
                style={[
                  {
                    borderBottomRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    backgroundColor: "#A0DCF5",
                    alignItems: "center",
                    justifyContent: "center",
                    height: 40,
                  },
                  styles.shadow,
                ]}
                onPress={() =>
                  master.rateCount > 0 &&
                  navigation.navigate("AddFeedback", { master })
                }
              >
                <Text
                  style={{
                    fontFamily: global.fonts.regular,
                    fontSize: 18,
                    color: "white",
                  }}
                >
                  Посмотреть отзывы
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: "100%",
                paddingHorizontal: 20,
              }}
            >
              {method && (
                <TouchableOpacity
                  style={{
                    width: "100%",
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: global.colors.mainColor,
                    borderRadius: 50,
                    marginBottom: 10,
                  }}
                  onPress={() => chooseMaster()}
                >
                  <Text
                    style={{
                      fontFamily: global.fonts.regular,
                      fontSize: 18,
                      color: "white",
                    }}
                  >
                    Выбрать
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default MasterProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    paddingHorizontal: 20,
    width: "100%",
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 3,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 30,
    marginBottom: 10,
    width: "85%",
  },
});
