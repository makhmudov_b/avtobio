import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  StatusBar,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import MyLocation from "../../assets/my-location.svg";
import Target from "../../assets/target.svg";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-blue.svg";
import Work from "../../assets/work.svg";
import House from "../../assets/house.svg";
import Service from "../../assets/service.svg";
import Fuel from "../../assets/fuel.svg";
import LocationWhite from "../../assets/location-white.svg";
import Location from "../../assets/location.svg";
import PlusWhite from "../../assets/plus.svg";
import Overlay from "../../assets/overlay.svg";
import Context from "../components/Context";
import CustomStatusBar from "../components/CustomStatusBar";
import RequestLoadingScreen from "./RequestLoadingScreen";

export default class AddressMapScreen extends Component {
  static contextType = Context;

  constructor() {
    super();
  }
  state = {
    search: "",
    region: {
      latitude: 41.26465,
      longitude: 69.21627,
      latitudeDelta: 0.04,
      longitudeDelta: 0.05,
    },
    openModal: false,
    openSearch: false,
    zoom: 14,
    address: [],
    loading: true,
  };
  componentDidMount() {
    this.getList();
  }
  getList() {
    const { apiService, token } = this.context;
    apiService
      .getResources("/address/list", token)
      .then((value) => {
        const data = value.data;
        if (typeof data !== "undefined") {
          this.setState({ address: data });
        }
        this.setState({ loading: false });
      })
      .catch((e) => Alert.alert("Что-то пошло не так"));
  }
  setLocation() {
    this.props.route.params.setPosition({
      lat: this.state.region.latitude,
      lng: this.state.region.longitude,
    });
    this.props.navigation.goBack();
  }
  onSearch(search) {
    this.setState({ search: search });
  }
  setSearch(boolValue) {
    this.setState({ openSearch: boolValue });
  }
  onRegionChange(region) {
    // this.map.setCamera();
    this.setState({
      region: {
        latitude: region.latitude,
        longitude: region.longitude,
        latitudeDelta: region.latitudeDelta,
        longitudeDelta: region.longitudeDelta,
      },
    });
  }
  goToUserLocation() {
    const { coords } = this.context.location;
    const { latitude, longitude, altitude, heading } = coords;
    const temp_cordinate = { latitude: latitude, longitude: longitude };
    this.map.animateCamera({
      center: temp_cordinate,
      pitch: 2,
      heading: heading,
      altitude: altitude,
      zoom: 14,
    });
  }
  goToAddressLocation(lat, long) {
    this.setState({ openModal: false });
    const { coords } = this.context.location;
    const { altitude, heading } = coords;
    const temp_cordinate = { latitude: lat, longitude: long };
    this.map.animateCamera({
      center: temp_cordinate,
      pitch: 2,
      heading: heading,
      altitude: altitude,
      zoom: 14,
    });
  }
  handleUpdateAddress = () => {
    this.getList();
    this.setState({ openModal: true });
  };
  addAddress() {
    const { navigation } = this.props;
    this.setState({ openModal: false });
    navigation.navigate("AddAddress", {
      update: this.handleUpdateAddress,
    });
  }
  render() {
    const { navigation, route } = this.props;
    const { params } = route;
    const { title, showAddress } = params;
    const showIcon = (value) => {
      if (value == 0) return <Work />;
      if (value == 1) return <House />;
      if (value == 2) return <Service width={30} height={30} />;
      if (value == 3) return <Fuel />;
      if (value == 4) return <LocationWhite />;
    };
    if (this.state.loading) {
      return <RequestLoadingScreen />;
    }
    return (
      <>
        <CustomStatusBar type={`white`} />
        {true ? (
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.openModal}
          >
            <TouchableWithoutFeedback
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
              }}
              onPress={() => this.setState({ openModal: false })}
            >
              <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
            </TouchableWithoutFeedback>
            <View
              style={{
                flexDirection: "row",
                position: "absolute",
                bottom: 20,
                justifyContent: "center",
                alignItems: "center",
                marginHorizontal: 20,
              }}
            >
              <View
                style={[
                  styles.authorizationBlock,
                  {
                    width: "100%",
                    borderRadius: 20,
                    backgroundColor: "white",
                  },
                ]}
              >
                <Text
                  style={{
                    fontSize: 24,
                    textAlign: "center",
                    marginBottom: 15,
                    fontFamily: global.fonts.medium,
                    color: "#444444",
                  }}
                >
                  {this.state.address.length
                    ? "Мои адреса"
                    : "У вас нет адресов"}
                </Text>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  contentContainerStyle={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <View width={10} />
                  {this.state.address.map((address, key) => {
                    return (
                      <TouchableOpacity
                        key={key}
                        onPress={() =>
                          this.goToAddressLocation(address.lat, address.lng)
                        }
                        style={styles.block}
                      >
                        <View style={[styles.blockInner, { marginBottom: 10 }]}>
                          {showIcon(address.icon)}
                        </View>
                        <Text
                          style={{
                            fontSize: 14,
                            fontFamily: global.fonts.medium,
                            textAlign: "center",
                            color: "#444444",
                          }}
                        >
                          {address.name}
                        </Text>
                        <Text
                          style={{
                            fontSize: 9,
                            textAlign: "center",
                            color: "#999999",
                            fontFamily: global.fonts.regular,
                          }}
                        >
                          {address.address}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                  <TouchableOpacity
                    onPress={() => this.addAddress()}
                    style={styles.block}
                  >
                    <View style={[styles.blockInner, { marginBottom: 10 }]}>
                      <PlusWhite />
                    </View>
                  </TouchableOpacity>
                  <View width={10} />
                </ScrollView>
              </View>
            </View>
          </Modal>
        ) : null}
        <KeyboardAvoidingView behavior={"padding"} style={styles.container}>
          <View
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              zIndex: 10,
            }}
          >
            <View
              style={{
                paddingHorizontal: 20,
                paddingTop: 20,
                // paddingBottom: 40,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Arrow />
                </TouchableOpacity>
                <View style={{ flex: 1, position: "relative" }}>
                  <Text
                    style={{
                      fontSize: 24,
                      textAlign: "center",
                      fontFamily: global.fonts.medium,
                      color: "#444444",
                    }}
                  >
                    {title}
                  </Text>
                </View>
                <View style={{ opacity: 0 }}>
                  <Arrow />
                </View>
              </View>
            </View>
          </View>
          <View style={[styles.whiteBlock, { alignItems: "center" }]}>
            {!showAddress ? (
              <TouchableOpacity
                onPress={() => this.setState({ openModal: true })}
                style={[styles.button, { backgroundColor: "white" }]}
              >
                <View style={{ position: "absolute", left: 20, top: 13 }}>
                  <Location />
                </View>
                <Text
                  style={{
                    color: "#444444",
                    fontSize: 18,
                    fontFamily: global.fonts.regular,
                  }}
                >
                  Мои адреса
                </Text>
              </TouchableOpacity>
            ) : null}
            <TouchableOpacity
              style={[
                styles.button,
                { backgroundColor: global.colors.mainColor },
              ]}
              onPress={() => this.setLocation()}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: global.fonts.regular,
                }}
              >
                Указать место
              </Text>
            </TouchableOpacity>
          </View>
          <MapView
            initialRegion={this.state.region}
            ref={(map) => {
              this.map = map;
            }}
            onRegionChangeComplete={(region) => this.onRegionChange(region)}
            provider={PROVIDER_GOOGLE}
            style={styles.mapStyle}
            loadingEnabled={true}
            showsUserLocation={true}
          />
          <View pointerEvents={"none"} style={styles.overlay}>
            <Overlay
              width={global.strings.width}
              height={global.strings.height}
              style={{ position: "absolute", transform: [{ scale: 1.3 }] }}
            />
            <MyLocation />
          </View>
          <View style={{ position: "absolute", right: 20 }}>
            <TouchableOpacity
              onPress={() => this.goToUserLocation()}
              style={[styles.mapButton, { borderRadius: 50, marginTop: 20 }]}
            >
              <Target width={24} />
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // width: global.strings.width,
    // height: global.strings.height,
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  authorizationBlock: {
    paddingVertical: 20,
  },
  block: {
    marginBottom: 10,
    marginTop: 5,
    marginHorizontal: 10,
    width: global.strings.width * 0.34,
    height: global.strings.width * 0.34,
    justifyContent: "center",
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  blockInner: {
    padding: 10,
    alignItems: "center",
    justifyContent: "center",
    width: 60,
    height: 60,
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
  },
  input: {
    fontSize: 18,
    color: "#444444",
    backgroundColor: "white",
    borderRadius: 50,
    width: 225,
    height: 40,
    paddingLeft: 10,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  whiteBlock: {
    zIndex: 20,
    position: "absolute",
    bottom: 20,
    width: global.strings.width,
    paddingHorizontal: 20,
  },
  mapStyle: {
    width: global.strings.width,
    height: global.strings.height,
    position: "relative",
    position: "absolute",
    // top:0,
    // left:0,
    // right:0,
    // bottom:0
  },
  overlay: {
    position: "absolute",
    flex: 1,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  //   circle: {
  //     backgroundColor: 'black',
  //     borderRadius: 100,
  //     alignSelf: 'stretch',
  //     width:400,
  //     height:300,
  //   },
  none: {
    display: "none",
    overflow: "hidden",
    width: 0,
    height: 0,
  },
  show: {
    width: 90,
    height: 90,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    width: 46,
    height: 46,
    backgroundColor: global.colors.warn,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    borderRadius: 50,
    marginTop: 20,
    width: "85%",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  mapButton: {
    backgroundColor: "white",
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  borderBottom: {
    borderBottomColor: "#E9E9E9",
    borderBottomWidth: 1,
  },
});
