import moment from "moment";

export const pad = (number) => {
  if (number < 10) {
    return "0" + number;
  }
  return number;
};

export const parseDate = (dateVal) => {
  // return moment(dateVal);
  return (
    dateVal.getFullYear() +
    "-" +
    pad(dateVal.getMonth() + 1) +
    "-" +
    pad(dateVal.getDate()) +
    "T" +
    pad(dateVal.getHours()) +
    ":" +
    pad(dateVal.getMinutes()) +
    ":" +
    pad(dateVal.getSeconds()) +
    "." +
    (dateVal.getMilliseconds() / 1000).toFixed(3).slice(2, 5) +
    "Z"
  );
};

export const formatDate = (date) => {
  return moment(date).format("DD-MM-YYYY HH:mm");
};
