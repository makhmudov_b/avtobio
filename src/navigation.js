import React, { useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./screens/HomeScreen";
import AddScreen from "./screens/AddScreen";
import ReviewScreen from "./screens/ReviewScreen";
import LoginScreen from "./screens/LoginScreen";
import ConfirmScreen from "./screens/ConfirmScreen";
import PasswordScreen from "./screens/PasswordScreen";
import ChangePasswordScreen from "./screens/ChangePasswordScreen";
import RegisterScreen from "./screens/RegisterScreen";
import CabinetScreen from "./screens/CabinetScreen";
import SettingScreen from "./screens/SettingScreen";
import FeedbackScreen from "./screens/FeedbackScreen";
import ServiceScreen from "./screens/ServiceScreen";
import RoadScreen from "./screens/RoadScreen";
import FuelScreen from "./screens/FuelScreen";
import IncomeScreen from "./screens/IncomeScreen";
import ExpenseScreen from "./screens/ExpenseScreen";
import StatisticScreen from "./screens/StatisticScreen";
import DrawerScreen from "./screens/DrawerScreen";
import AddAutoScreen from "./screens/AddAutoScreen";
import AutoScreen from "./screens/AutoScreen";
import DocumentScreen from "./screens/DocumentScreen";
import AddDocumentScreen from "./screens/AddDocumentScreen";
import MapScreen from "./screens/MapScreen";
import StoreScreen from "./screens/StoreScreen";
import CheckoutScreen from "./screens/CheckoutScreen";
import AddressMapScreen from "./screens/AddressMapScreen";
import AddAddressScreen from "./screens/AddAddressScreen";
import MasterScreen from "./screens/MasterScreen";
import MasterProfileScreen from "./screens/MasterProfileScreen";
import AddFeedbackScreen from "./screens/AddFeedbackScreen";
import ExtremeMapScreen from "./screens/ExtremeMapScreen";
import EvacuatorScreen from "./screens/EvacuatorScreen";
import CallTypeScreen from "./screens/CallTypeScreen";
import MasterTypeScreen from "./screens/MasterTypeScreen";
import ExtremeMasterScreen from "./screens/ExtremeMasterScreen";
import ExtremeOrderScreen from "./screens/ExtremeOrderScreen";
import ServiceProfileScreen from "./screens/ServiceProfileScreen";
import EventScreen from "./screens/EventScreen";
import BookingScreen from "./screens/BookingScreen";
import MyAddressScreen from "./screens/MyAddressScreen";
import ChooseAutoScreen from "./screens/ChooseAutoScreen";
import ReasonScreen from "./screens/ReasonScreen";
import ReminderScreen from "./screens/ReminderScreen";
import ChoiceScreen from "./screens/ChoiceScreen";
import AddressChoiceScreen from "./screens/AddressChoiceScreen";
import LoadingScreen from "./screens/LoadingScreen";
import TechnicalScreen from "./screens/TechnicalScreen";
import Context from "./components/Context";
import ShowImageScreen from "./screens/ShowImageScreen";
import ShowLocatorScreen from "./screens/ShowLocatorScreen";
import ServiceEventScreen from "./screens/ServiceEventScreen";
import TypeChoiceScreen from "./screens/TypeChoiceScreen";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const AppContainer = () => {
  const { authorized, loading } = useContext(Context);

  if (loading) {
    return <LoadingScreen />;
  }
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={authorized ? "Home" : "Preview"}>
        <>
          <Stack.Screen
            name="Preview"
            component={ReviewScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Confirm"
            component={ConfirmScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Register"
            component={RegisterScreen}
            options={{ headerShown: false }}
          />
        </>
        <Stack.Screen name="Home" options={{ headerShown: false }}>
          {() => {
            return (
              <Drawer.Navigator
                initialRouteName="Main"
                drawerType={"front"}
                drawerContent={(props) => <DrawerScreen {...props} />}
              >
                <Drawer.Screen name="Main" component={HomeScreen} />
              </Drawer.Navigator>
            );
          }}
        </Stack.Screen>
        <Stack.Screen
          name="Choice"
          component={ChoiceScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddressChoice"
          component={AddressChoiceScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Reminder"
          component={ReminderScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Reason"
          component={ReasonScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ChooseAuto"
          component={ChooseAutoScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Cabinet"
          component={CabinetScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddEvent"
          component={AddScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Password"
          component={PasswordScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ChangePassword"
          component={ChangePasswordScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Setting"
          component={SettingScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Feedback"
          component={FeedbackScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Service"
          component={ServiceScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Road"
          component={RoadScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Fuel"
          component={FuelScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Income"
          component={IncomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Expense"
          component={ExpenseScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Statistic"
          component={StatisticScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddAuto"
          component={AddAutoScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Auto"
          component={AutoScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Document"
          component={DocumentScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddDocument"
          component={AddDocumentScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Map"
          component={MapScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Store"
          component={StoreScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddressMap"
          component={AddressMapScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Checkout"
          component={CheckoutScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddAddress"
          component={AddAddressScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Master"
          component={MasterScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MasterProfile"
          component={MasterProfileScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AddFeedback"
          component={AddFeedbackScreen}
          options={{ headerShown: false }}
        />
        {/* sos */}
        <Stack.Screen
          name="ExtremeMap"
          component={ExtremeMapScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Evacuator"
          component={EvacuatorScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ExtremeMaster"
          component={ExtremeMasterScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="CallType"
          component={CallTypeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MasterType"
          component={MasterTypeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ExtremeOrder"
          component={ExtremeOrderScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ServiceProfile"
          component={ServiceProfileScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ShowLocator"
          component={ShowLocatorScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Event"
          component={EventScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Booking"
          component={BookingScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MyAddress"
          component={MyAddressScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Technical"
          component={TechnicalScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ShowImage"
          component={ShowImageScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ServiceEvent"
          component={ServiceEventScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="TypeChoice"
          component={TypeChoiceScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppContainer;
