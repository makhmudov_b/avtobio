const googleApiKey = "AIzaSyBuIojBnjjA5J2AtWwwy6LBT2lp1d9hU20";
class ApiService {
  _host = "https://api.avtobio.uz:443/api";
  _apiBase = this._host + "/mobile/v1";
  _headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  // Authorization
  sendCode = async (phone) => {
    return fetch(this._apiBase + "/member/registration", {
      method: "POST",
      headers: this._headers,
      body: JSON.stringify({ phone: phone }),
    })
      .then((res) => {
        // if (!res.ok)
        //   throw new Error("Введены неправильные данные. Повторите заново!");
        return res.json();
      })
      .then((res) => {
        return res;
      });
  };

  register = async (token, form) => {
    let headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    return fetch(this._host + "/account", {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => {
      // if (res.status > 200) throw res.json();
      return res.json();
    });
  };
  updateForm = async (url, token = null, form, vehicle_id) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicle_id,
        "Accept-Language": "ru",
      };
    }
    return fetch(this._apiBase + url, {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => {
      // if (res.status > 201) throw "Login";
      return res.json();
    });
  };
  deleteMethod = async (url, token = null, form, vehicle_id) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicle_id,
        "Accept-Language": "ru",
      };
    }
    return fetch(this._apiBase + url, {
      method: "DELETE",
      headers: headers,
      body: JSON.stringify(form),
    })
      .then((res) => {
        if (res.status > 201) throw "Login";
        return res.json();
      })
      .then((value) => {
        return value;
      });
  };
  deleteReference = async (url, token = null, form, vehicle_id) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicle_id,
        "Accept-Language": "ru",
      };
    }
    return fetch(this._host + url, {
      method: "DELETE",
      headers: headers,
      body: JSON.stringify(form),
    })
      .then((res) => {
        if (res.status > 201) throw "Login";
        return res.json();
      })
      .then((value) => {
        return value;
      });
  };

  login = async (code) => {
    return fetch(this._apiBase + "/member/activatecode", {
      method: "PUT",
      headers: this._headers,
      body: JSON.stringify({ code }),
    })
      .then((res) => {
        if (!res.ok) throw new Error(res.message);
        return res.json();
      })
      .then((res) => {
        alert(res.message);
        return res.status;
      });
  };

  addAuto = async (token = null, form) => {
    let headers = {};
    if (token) {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + "/vehicle/item", {
      method: "POST",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => {
      // if (res.status > 200) throw "Login";
      return res.json();
    });
  };

  uploadFiles = async (token = null, vehicleId, form) => {
    const { frontImage, backImage } = form;
    let data = new FormData();
    const random = Math.floor(Math.random() * 10000);
    if (frontImage) {
      const getType = frontImage.indexOf("png");
      data.append("uploadFile", {
        uri: frontImage,
        name: `frontdocument${random}` + (getType > 0 ? ".png" : ".jpg"),
        type: getType > 0 ? "image/png" : "image/jpeg",
      });
    }
    if (backImage) {
      const getType = backImage.indexOf("png");
      data.append("uploadFile", {
        uri: backImage,
        name: `frontdocument${random}` + (getType > 0 ? ".png" : ".jpg"),
        type: getType > 0 ? "image/png" : "image/jpeg",
      });
    }
    let headers = {};
    if (token) {
      headers = {
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._host + "/backend/v1/file/list", {
      method: "POST",
      headers: headers,
      body: data,
    }).then((res) => {
      return res.json();
    });
  };

  addEvent = async (token = null, type, form, vehicle_id) => {
    let headers = {};
    if (token) {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicle_id,
        "Accept-Language": "ru",
      };
    }
    return fetch(this._apiBase + "/events/" + type, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(form),
    })
      .then((res) => {
        // if (res.status > 200) throw "Login";
        return res.json();
      })
      .then((value) => {
        return value;
      });
  };
  postData = async (token = null, url, form, vehicle_id) => {
    let headers = {};
    if (token) {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicle_id,
        "Accept-Language": "ru",
      };
    }
    return fetch(this._apiBase + url, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(form),
    })
      .then((res) => {
        // if (res.status > 200) throw "Login";
        return res.json();
      })
      .then((value) => {
        return value;
      });
  };
  postDocument = (token = null, url, form, vehicle_id) => {
    let headers = {
      Authorization: `Bearer ${token}`,
      VEHICLE_ID: vehicle_id,
    };
    const { frontImage, backImage, name, expirationDate, type } = form;
    const formData = new FormData();
    if (name) formData.append("name", name);
    if (expirationDate) formData.append("expirationDate", expirationDate);
    if (type) formData.append("type", type);
    if (frontImage) {
      const getType = frontImage.indexOf("png");
      formData.append("frontImage", {
        uri: frontImage,
        name: "front" + (getType > 0 ? ".png" : ".jpg"),
        // contentType: "multipart/form-data",
        type: getType > 0 ? "image/png" : "image/jpeg",
      });
    }
    if (backImage) {
      const getType = backImage.indexOf("png");
      formData.append("backImage", {
        uri: backImage,
        name: "back" + (getType > 0 ? ".png" : ".jpg"),
        // contentType: "multipart/form-data",
        type: getType > 0 ? "image/png" : "image/jpeg",
      });
    }
    return fetch(this._apiBase + url, {
      method: "POST",
      headers: headers,
      body: formData,
    })
      .then((res) => {
        // if (res.status > 200) throw "Login";
        return res.json();
      })
      .then((value) => {
        return value;
      });
  };
  editDocument = async (token = null, url, form, vehicle_id) => {
    let headers = {};
    if (token) {
      headers = {
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicle_id,
        "Accept-Language": "ru",
      };
    }
    const { frontImage, backImage, name, expirationDate, type } = form;
    const formData = new FormData();
    if (name) formData.append("name", name);
    if (expirationDate) formData.append("expirationDate", expirationDate);
    if (type) formData.append("type", type);
    if (frontImage) {
      const getType = frontImage.indexOf("png");
      formData.append("frontImage", {
        uri: frontImage,
        name: "front" + (getType > 0 ? ".png" : ".jpg"),
        type: getType > 0 ? "image/png" : "image/jpeg",
      });
    }
    if (backImage) {
      const getType = backImage.indexOf("png");
      formData.append("backImage", {
        uri: backImage,
        name: "back" + (getType > 0 ? ".png" : ".jpg"),
        type: getType > 0 ? "image/png" : "image/jpeg",
      });
    }
    return fetch(this._apiBase + url, {
      method: "PUT",
      headers: headers,
      body: formData,
    })
      .then((res) => {
        // if (res.status > 200) throw "Login";
        return res.json();
      })
      .then((value) => {
        return value;
      });
  };
  getDistance = (origin, destination) => {
    const googleBase =
      "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric?language=ru";
    return fetch(
      `${googleBase}&key=${googleApiKey}&origins=${origin}&destinations=${destination}`
    )
      .then((res) => res.json())
      .then((value) => value);
  };
  getAddress = async (lat, long) => {
    const googleBase = "https://maps.googleapis.com/maps/api/geocode/json?";
    const res = await fetch(
      `${googleBase}latlng=${lat},${long}&language=ru&key=${googleApiKey}`
    );
    return await res.json();
  };
  deleteData = async (url, token = null, vehicleId = null) => {
    let headers = [];

    if (token) {
      headers = {
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicleId,
        "Accept-Language": "ru",
      };
    }

    const res = await fetch(this._apiBase + url, {
      method: "DELETE",
      headers: headers,
    });
    // if (res.status > 200) throw "Login";

    return await res.json();
  };
  getResources = async (url, token = null, vehicleId = null) => {
    let headers = [];

    if (token) {
      headers = {
        Authorization: `Bearer ${token}`,
        VEHICLE_ID: vehicleId,
        "Accept-Language": "ru",
      };
    }

    const res = await fetch(this._apiBase + url, { headers });
    // if (res.status > 200) throw "Login";

    return await res.json();
  };
  getReferences = async (url, token = null) => {
    let headers = [];

    if (token) {
      headers = { Authorization: `Bearer ${token}`, "Accept-Language": "ru" };
    }

    const res = await fetch(this._host + url, { headers });
    if (res.status > 200) throw "Login";

    return await res.json();
  };
}
export default ApiService;
