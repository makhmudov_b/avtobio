export const items = {
    REQUEST: 'FETCH_ITEMS_REQUEST',
    SUCCESS: 'FETCH_ITEMS_SUCCESS',
    FAILURE: 'FETCH_ITEMS_FAILURE',
};

export const searchItems = {
    REQUEST: 'FETCH_SEARCH_ITEMS_REQUEST',
    SUCCESS: 'FETCH_SEARCH_ITEMS_SUCCESS',
    FAILURE: 'FETCH_SEARCH_ITEMS_FAILURE',
};

export const categories = {
    REQUEST: 'FETCH_CATEGORIES_REQUEST',
    SUCCESS: 'FETCH_CATEGORIES_SUCCESS',
    FAILURE: 'FETCH_CATEGORIES_FAILURE',
};

export const settings = {
    THEME_CHANGE: 'THEME_CHANGE',
    LANG_CHANGE: 'LANG_CHANGE',
    FONTSIZE_CHANGE: 'FONTSIZE_CHANGE',
    SEARCH_CHANGE: 'SEARCH_CHANGE',
    ARTICLE_STYLE_CHANGE: 'ARTICLE_STYLE_CHANGE',
};

export const item = {
    REQUEST: 'FETCH_ITEM_REQUEST',
    SUCCESS: 'FETCH_ITEM_SUCCESS',
    FAILURE: 'FETCH_ITEM_FAILURE',
};

export const user = {
    REGISTER_REQUEST: 'REGISTER_REQUEST',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILURE: 'REGISTER_FAILURE',

    PROFILE_REQUEST: 'PROFILE_REQUEST',
    PROFILE_SUCCESS: 'PROFILE_SUCCESS',
    PROFILE_FAILURE: 'PROFILE_FAILURE',

    LOGIN_REQUEST: 'LOGIN_REQUEST',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILURE: 'LOGIN_FAILURE',

    LOGOUT: 'LOGOUT',
};
