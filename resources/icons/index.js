/* eslint-disable */
import React from "react";
import Svg, { Path } from "react-native-svg";
import vector from "./vector";

const Icon = props => <Svg {...props} svgs={vector} />;

export default Icon;
