const colors = {
  mainColor: "#40B8EB",
  warn: "#FFD500",
  danger: "#FF4444",
  preDanger: "#FF6035",
  success: "#00D756",
  violet: "#B84BFF",
  lightGreen: "#2EE6A8",
  darkGreen: "#6AD97B",
  normal: "#EBE449"
};

export default colors;
