const images = {
  drawer: require("../assets/drawer.png"),
  trust: require("../assets/trust.png"),
  technical: require("../assets/technical.png"),
  product: require("../assets/product-item.png"),
  repair: require("../assets/repair.png"),
  splash: require("../assets/splash.png"),
  comment: require("../assets/comment.png"),
  loader: require("../assets/loader.gif"),
  screen1: require("../assets/screen1.gif"),
  screen2: require("../assets/screen2.gif"),
  screen3: require("../assets/screen3.gif"),
  screen4: require("../assets/screen4.gif"),
  screen5: require("../assets/screen5.gif"),
};

export default images;
