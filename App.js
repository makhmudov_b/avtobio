import React, { useState, useEffect, useRef } from "react";
import AppContainer from "./src/navigation";
import * as Font from "expo-font";
import Context from "./src/components/Context";
import ErrorBoundary from "./src/components/ErrorBoundary";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import * as Notifications from "expo-notifications";
import {
  Text,
  TextInput,
  Alert,
  Vibration,
  AsyncStorage,
  Platform,
} from "react-native";

import ApiService from "./src/services/api";
import { AndroidNotificationPriority } from "expo-notifications";

const apiService = new ApiService();
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
    priority: AndroidNotificationPriority.MAX,
  }),
});
export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const [token, setToken] = useState(null);
  const [phone, setPhone] = useState(null);
  const [user, setUser] = useState(null);
  const [location, setLocation] = useState(null);
  const [currentCar, setCar] = useState(0);
  const [reminderCount, setReminderCount] = useState(0);
  const [autos, setAutos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [authorized, setAuth] = useState(false);
  const [expoPushToken, setExpoPushToken] = useState(null);
  const [notification, setNotification] = useState(null);
  const notificationListener = useRef();
  const responseListener = useRef();
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
  TextInput.defaultProps.allowFontScaling = false;
  const getLocation = async () => {
    const { status } = await Location.requestPermissionsAsync();
    const location = await Location.getCurrentPositionAsync({
      enableHighAccuracy: false,
    });
    if (status === "granted") setLocation(location);
  };
  const registerForPushNotificationsAsync = async (userToken, settedToken) => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        return;
      }
      const notificationToken = await Notifications.getExpoPushTokenAsync();
      if (userToken) {
        if (notificationToken != settedToken) {
          const body = {
            token: notificationToken.data,
          };
          setExpoPushToken(notificationToken.data);
          apiService.updateForm("/member/firebase-token", userToken, body);
        }
      }
      if (Platform.OS === "android") {
        Notifications.setNotificationChannelAsync("default", {
          name: "default",
          importance: Notifications.AndroidImportance.MAX,
          vibrationPattern: [0, 250, 250, 250],
          lightColor: "#FF231F7C",
          sound: "default",
          lockscreenVisibility:
            Notifications.AndroidNotificationVisibility.PUBLIC,
          showBadge: true,
        });
      }

      setExpoPushToken(notificationToken);
    }
  };
  useEffect(() => {
    Font.loadAsync({
      "Rubik-Light": require("./assets/fonts/Rubik-Light.ttf"),
      "Rubik-Regular": require("./assets/fonts/Rubik-Regular.ttf"),
      "Rubik-Bold": require("./assets/fonts/Rubik-Bold.ttf"),
      "Rubik-Medium": require("./assets/fonts/Rubik-Medium.ttf"),
    }).then(() => {
      setFontLoaded(true);
    });
    getUserData();
    Location.requestPermissionsAsync();
    notificationListener.current = Notifications.addNotificationReceivedListener(
      (notification) => {
        if (notification) setNotification(notification);
      }
    );
    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);
  const getData = async (name) => {
    try {
      const data = await AsyncStorage.getItem(name);
      return data;
    } catch (e) {}
  };
  const removeData = async (name) => {
    try {
      await AsyncStorage.removeItem(name);
      return true;
    } catch (e) {}
  };
  const storeData = async (name, data) => {
    try {
      await AsyncStorage.setItem(name, JSON.stringify(data));
    } catch (e) {}
  };
  const logout = () => {
    setLoading(true);
    setUser(null);
    setPhone(null);
    setToken(null);
    removeData("token");
    removeData("registered");
    removeData("phone");
    setAuth(false);
    setTimeout(() => setLoading(false), 1000);
  };
  const getUserData = (catcher) => {
    if (!loading) setLoading(true);
    getData("token").then((value) => {
      const getToken = JSON.parse(value);
      if (getToken) {
        apiService
          .getReferences("/account", getToken)
          .then((value) => {
            setToken(getToken);
            setUser(value);
            setAuth(true);
            setReminderCount(value.reminderCount);
            setPhone(value.login);
            if (catcher) setLoading(false);
            registerForPushNotificationsAsync(getToken, value.firebaseToken);
            getGarage(getToken);
          })
          .catch((e) => {
            setAuth(false);
            if (loading) setLoading(false);
          });
      } else {
        setLoading(false);
      }
    });
  };
  const getGarage = (getToken) => {
    // setLoading(true);
    return apiService.getResources("/vehicle/list", getToken).then((value) => {
      if (loading) setLoading(false);
      const data = value.data;
      if (typeof data !== "undefined") {
        if (data.length) setCar(data[0]);
        setAutos(data);
      }
      return true;
    });
  };
  return fontLoaded ? (
    <ErrorBoundary>
      <Context.Provider
        value={{
          apiService,
          token,
          setToken,
          setPhone,
          phone,
          user,
          setUser,
          location,
          getLocation,
          currentCar,
          setCar,
          autos,
          setAutos,
          getGarage,
          getUserData,
          loading,
          setLoading,
          authorized,
          setAuth,
          logout,
          reminderCount,
          setReminderCount,
          notification,
        }}
      >
        <AppContainer />
      </Context.Provider>
    </ErrorBoundary>
  ) : null;
}
