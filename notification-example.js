import React, { useState, useEffect } from "react";
import AppContainer from "./src/navigation";
import * as Font from "expo-font";
import Context from "./src/components/Context";
import ErrorBoundary from "./src/components/ErrorBoundary";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { Notifications } from "expo";
import { Text, TextInput, Alert, Vibration } from "react-native";

import ApiService from "./src/services/api";

const apiService = new ApiService();
export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const [token, setToken] = useState(null);
  const [phone, setPhone] = useState(null);
  const [user, setUser] = useState(null);
  const [registered, setRegistered] = useState(null);
  const [location, setLocation] = useState(null);
  const [currentCar, setCar] = useState(0);
  const [autos, setAutos] = useState([]);
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState({});
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
  TextInput.defaultProps.allowFontScaling = false;
  const getLocation = async () => {
    const location = await Location.getCurrentPositionAsync({
      enableHighAccuracy: false,
      maximumAge: 1000,
    });
    setLocation(location);
  };
  const _handleNotification = (notification) => {
    Vibration.vibrate();
    this.setState({ notification: notification });
  };
  const registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        Alert.alert("Failed to get push token for push notification!");
        return;
      }
      token = await Notifications.getExpoPushTokenAsync();
      setExpoPushToken(token);
    } else {
      Alert.alert("Must use physical device for Push Notifications");
    }

    if (Platform.OS === "android") {
      Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      });
    }
  };
  useEffect(() => {
    Font.loadAsync({
      "Rubik-Light": require("./assets/fonts/Rubik-Light.ttf"),
      "Rubik-Regular": require("./assets/fonts/Rubik-Regular.ttf"),
      "Rubik-Bold": require("./assets/fonts/Rubik-Bold.ttf"),
      "Rubik-Medium": require("./assets/fonts/Rubik-Medium.ttf"),
    }).then(() => {
      setFontLoaded(true);
    });
    getLocation();
  }, []);

  return fontLoaded ? (
    <ErrorBoundary>
      <Context.Provider
        value={{
          apiService,
          token,
          setToken,
          setPhone,
          phone,
          registered,
          setRegistered,
          user,
          setUser,
          location,
          getLocation,
          currentCar,
          setCar,
          autos,
          setAutos,
        }}
      >
        <AppContainer />
      </Context.Provider>
    </ErrorBoundary>
  ) : null;
}
